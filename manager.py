import os
import sys
import multiprocessing as mp

services = {
    "carts-service": "carts-and-orders/carts-src",
    "orders-service": "carts-and-orders/orders-src",
    "customers-service": "customers/src",
    "products-service": "products-and-stock/products-src",
    "stock-service": "products-and-stock/stock-src",
    "shipments-service": "shipments/src",
    "stores-service": "stores/src",
    "backoffice-frontend": "backoffice-frontend/src",
    "ecommerce-frontend": "ecommerce-frontend/src"
}

base_dir = os.path.dirname(os.path.realpath(__file__))


def init_api_gateway():
    command = f"docker run -d --rm --network brewtopia-services-develop --name api-gateway -p 80:80 -v {base_dir}/api-gateway/nginx.conf:/etc/nginx/conf.d/default.conf:ro nginx:1.21.0-alpine"
    print("Starting api-gateway", command)
    os.system(command)


def init_service(service):
    # build
    service_path = services[service]
    command = f"docker build {base_dir}/{service_path} -f {base_dir}/{service_path}/Dockerfile -t registry.gitlab.com/itba-brewtopia/brewtopia/{service}:latest"
    print("Building", service, command)
    os.system(command)
    domain_path = service_path.split("/")[:-1]
    domain_path = "/".join(domain_path)
    compose_file = f"{base_dir}/{domain_path}/docker-compose.yml"

    # run common services
    command = f"ENV_FILE=env-files/.env.develop docker compose -f {compose_file} --env-file {base_dir}/{domain_path}/env-files/.env.develop --profile common-services up -d"
    print("Starting common services", command)
    os.system(command)

    # run
    command = f"ENV_FILE=env-files/.env.develop docker compose -f {compose_file} --env-file {base_dir}/{domain_path}/env-files/.env.develop up {service} -d"
    print("Starting", service, command)
    os.system(command)


def init_all_services():
    processes = []
    p = mp.Process(target=init_api_gateway)
    p.start()
    processes.append(p)
    for service in services.keys():
        p = mp.Process(target=init_service, args=(service,))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()

def no_frontend():
    processes = []
    p = mp.Process(target=init_api_gateway)
    p.start()
    processes.append(p)
    for service in services.keys():
        if service not in ["backoffice-frontend", "ecommerce-frontend"]:
            p = mp.Process(target=init_service, args=(service,))
            p.start()
            processes.append(p)
    for p in processes:
        p.join()


def remove_all_services():
    domains = set()
    for service in services.keys():
        domain = services[service].split("/")[:-1]
        domain = "/".join(domain)
        domains.add(domain)
    processes = []
    for domain in domains:
        compose_file = f"{base_dir}/{domain}/docker-compose.yml"
        command = f"ENV_FILE=env-files/.env.develop docker compose -f {compose_file} --env-file {base_dir}/{domain}/env-files/.env.develop --profile all  down -v --remove-orphans"
        print("Stopping", domain, command)
        p = mp.Process(target=os.system, args=(command,))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()


def run_services_test():
    command = f"docker build {base_dir}/integration-test -f {base_dir}/integration-test/Dockerfile -t integration-tester:latest"
    print("Building {tester}", command)
    os.system(command)
    command = f"docker run --rm --network brewtopia-services-develop -e GATEWAY_PATH='http://api-gateway:80' integration-tester:latest"
    print("Running {tester}", command)
    os.system(command)


if __name__ == "__main__":

    os.system("docker network create --driver bridge brewtopia-services-develop")

    args = sys.argv
    # args[0] = current file
    # args[1] = function name
    # args[2:] = function args : (*unpacked)
    globals()[args[1]](*args[2:])
