from typing import List, Optional

import psycopg2
from exceptions.not_found_error import NotFoundError
import requests
from psycopg2.errorcodes import UNIQUE_VIOLATION

from api.dto.cart_patch_dto import CartPatchDtoAction
from api.dto.cart_full_dto import CartFullDto
from persistence.carts_persistence import CartsPersistence


def _check_customer_id(customer_id: str):
    try:
        response = requests.get(
            f'http://customers-service:8080/customers/{str(customer_id)}', timeout=10)
        if response.status_code != 200:
            if response.status_code == 404:
                raise NotFoundError(
                    "Invalid customer id: " + str(customer_id))
            else:
                raise ValueError("Customer server internal error")
    except ValueError:
        raise
    except NotFoundError:
        raise
    except Exception as e:
        print(str(e))
        raise Exception(
            'Customer server not available')


def _get_product_info(product_id: str):
    try:
        response = requests.get(
            f'http://products-service:8080/products/{str(product_id)}', timeout=10)
        if response.status_code != 200:
            if response.status_code == 404:
                raise NotFoundError(
                    "Invalid product id: " + str(product_id))
            else:
                raise ValueError("Product server internal error")
    except ValueError:
        raise
    except NotFoundError:
        raise
    except Exception as e:
        print(str(e))
        raise Exception(
            'Product server not available')

    return response.json()


class CartsService():
    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return CartsPersistence.get_instance().health_check()

    def get_cart_by_customer_id(self, customer_id: str):
        _check_customer_id(customer_id)
        cart = CartsPersistence.get_instance().get_cart_by_customer_id(customer_id)
        return CartFullDto.from_model(cart)

    def get_cart_size_by_customer_id(self, customer_id: str):
        _check_customer_id(customer_id)
        return CartsPersistence.get_instance().get_cart_size_by_customer_id(customer_id)

    def edit_cart_item(self, customer_id: str, product_id: str,  action: CartPatchDtoAction, quantity: Optional[int] = None,):
        _check_customer_id(customer_id)
        # if it doesn't exist, it will raise an exception
        _get_product_info(product_id)
        if action == CartPatchDtoAction.INCREMENT:
            CartsPersistence.get_instance().increment_cart_item_quantity(
                customer_id, product_id)
        elif action == CartPatchDtoAction.DECREMENT:
            CartsPersistence.get_instance().decrement_cart_item_quantity(
                customer_id, product_id)
        elif action == CartPatchDtoAction.CHANGE and quantity is not None:
            CartsPersistence.get_instance().change_cart_item_quantity(
                customer_id, product_id, quantity)
        elif action == CartPatchDtoAction.REMOVE:
            CartsPersistence.get_instance().remove_cart_item(
                customer_id, product_id)
        else:
            raise ValueError("Invalid action")

        return self.refresh_cart_total_price(customer_id)

    def refresh_cart_total_price(self, customer_id: str):
        _check_customer_id(customer_id)
        cart = CartsPersistence.get_instance().get_cart_by_customer_id(customer_id)
        total_price = 0
        for cart_item in cart.items:
            # if it doesn't exist, it will raise an exception
            product_json = _get_product_info(cart_item.product_id)
            total_price += float(product_json['price']) * cart_item.quantity

        cart = CartsPersistence.get_instance().update_cart_total_price(
            customer_id, total_price)

        return CartFullDto.from_model(cart)

    def create_cart(self, customer_id):
        _check_customer_id(customer_id)
        try:
            cart = CartsPersistence.get_instance().create_cart(customer_id)
        except psycopg2.Error as e:
            if e.pgcode == UNIQUE_VIOLATION:
                raise ValueError(f'Customer {customer_id} already has a cart')
        return CartFullDto.from_model(cart)
