from exceptions.not_found_error import NotFoundError
from fastapi import FastAPI, HTTPException

from services.carts_service import CartsService
from api.dto.cart_patch_dto import CartPatchDtoAction
from api.dto.cart_full_dto import CartFullDto
from api.dto.cart_patch_dto import CartPatchDto

app = FastAPI(openapi_url="/carts/openapi.json",
              docs_url="/carts/docs", redoc_url="/carts/redoc")


@app.get("/carts/health")
async def health_check():
    service_status = CartsService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@app.get("/carts/{customer_id}", response_model=CartFullDto)
async def get_cart_by_customer_id(customer_id: str):
    try:
        cart = CartsService.get_instance().get_cart_by_customer_id(customer_id)
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail="Cart not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return cart


@app.get("/carts/{customer_id}/size", response_model=int)
async def get_cart_size_by_customer_id(customer_id: str):
    try:
        cart_size = CartsService.get_instance().get_cart_size_by_customer_id(customer_id)
        return cart_size
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail="Cart not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.patch("/carts/{customer_id}", response_model=CartFullDto)
async def edit_cart_item(customer_id: str, patch: CartPatchDto):
    try:
        if CartPatchDtoAction.CHANGE == patch.action and patch.quantity is None:
            raise ValueError(
                "quantity is required for action CHANGE")

        cart = CartsService.get_instance().edit_cart_item(
            customer_id, patch.product_id, patch.action, patch.quantity)
        return cart
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post("/carts/{customer_id}", response_model=CartFullDto, status_code=201)
async def create_cart(customer_id: str):
    try:
        cart = CartsService.get_instance().create_cart(customer_id)
        return cart
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail="Customer not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
