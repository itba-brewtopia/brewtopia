from models.cart import CartItem
from pydantic import BaseModel


class CartItemDto(BaseModel):
    product_id: str
    quantity: int

    @staticmethod
    def from_model(cart_item: CartItem):
        return CartItemDto(
            product_id=cart_item.product_id,
            quantity=cart_item.quantity
        )
