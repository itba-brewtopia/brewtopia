from pydantic import BaseModel
from typing import List, Optional

from models.cart import Cart
from api.dto.cart_item_dto import CartItemDto


class CartFullDto(BaseModel):
    customer_id: str
    created_at: str
    modified_at: str
    is_deleted: bool
    total_price: str
    items: List[CartItemDto]

    @staticmethod
    def from_model(cart: Cart):
        return CartFullDto(
            customer_id=str(cart.customer_id),
            created_at=cart.created_at.isoformat() if cart.created_at else '',
            modified_at=cart.modified_at.isoformat() if cart.modified_at else '',
            total_price=str(cart.total_price),
            is_deleted=cart.is_deleted,
            items=[CartItemDto.from_model(item) for item in cart.items]
        )
