from enum import Enum
from typing import Optional
from pydantic import BaseModel


class CartPatchDtoAction(str, Enum):
    INCREMENT = 'increment'
    DECREMENT = 'decrement'
    CHANGE = 'change'
    REMOVE = 'remove'


class CartPatchDto(BaseModel):
    product_id: str
    quantity: Optional[int] = None
    action: CartPatchDtoAction
