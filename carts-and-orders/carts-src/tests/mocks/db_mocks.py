from models.cart import Cart, CartItem

cart_item_getter = lambda customer_id: CartItem(
    product_id='5f4f3f2f1e0d0c0b0a090820',
    quantity=1,
    customer_id=customer_id,
)


mock_db_carts = [
    Cart(customer_id='a'*24,
         total_price=1000,
         is_deleted=False,
         items=[
            cart_item_getter('a'*24),
         ]),
    Cart(customer_id='b'*24,
         total_price=2000,
         is_deleted=False),
    Cart(customer_id='c'*24,
         total_price=3000,
         is_deleted=False),
]
