from models.base import Base
from models.order import Order, OrderItem
from models.cart import Cart, CartItem
from tests.mocks.db_mocks import mock_db_carts
from persistence.carts_persistence import CartsPersistence
import copy


def setup_db():
    engine = CartsPersistence().get_instance().engine
    # create order and order items tables
    Base.metadata.create_all(bind=engine, tables=[
        Order.__table__, OrderItem.__table__])
    session = CartsPersistence().get_instance().session
    session.query(CartItem).delete()
    session.query(Cart).delete()
    session.commit()
    session.close()


def seed_db():
    session = CartsPersistence().get_instance().session
    for cart in mock_db_carts:
        insert_cart = copy.deepcopy(cart)
        session.add(insert_cart)  # adds cart and its items
        session.commit()

    session.commit()
    session.close()


def create_cart(customer_id, items=[]):
    session = CartsPersistence().get_instance().session
    cart = Cart(customer_id=customer_id)
    session.add(cart)
    for item in items:
        session.add(CartItem(customer_id=customer_id, product_id=item.product_id,
                    quantity=item.quantity))  # creates deepcopy
    session.commit()
    session.close()


def drop_db():
    session = CartsPersistence().get_instance().session
    # rollback any pending transaction, like errors, to avoid locking for future tests
    session.rollback()
    session.query(CartItem).delete()
    session.query(Cart).delete()
    session.commit()
    session.close()
