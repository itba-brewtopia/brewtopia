import uuid
import json
from exceptions.not_found_error import NotFoundError
from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient

from api.carts_api import app
from api.dto.cart_patch_dto import CartPatchDto, CartPatchDtoAction
from unittest.mock import MagicMock, patch
from api.dto.cart_full_dto import CartFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_carts

client = TestClient(app)


@patch('services.carts_service.CartsService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/carts/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_by_customer_id(mock_get_instance):
    mock_cart = mock_db_carts[0]
    mock_response = CartFullDto.from_model(mock_cart)
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_by_customer_id.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.get(f'/carts/{str("a"*24)}')

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_value_error(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_by_customer_id.side_effect = ValueError(
        "Invalid parameters: ")

    response = client.get(f'/carts/{str("a"*24)}')

    assert response.status_code == 422
    assert "Invalid parameters: " in response.json()["detail"]


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_not_found(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_by_customer_id.side_effect = NotFoundError("asd")

    response = client.get(f'/carts/{str("a"*24)}')

    assert response.status_code == 404
    assert response.json() == {"detail": "Cart not found"}


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_exception(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_by_customer_id.side_effect = Exception("")

    response = client.get(f'/carts/{str("a"*24)}')

    assert response.status_code == 500
    assert response.json() == {"detail": ""}


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_size_by_customer_id(mock_get_instance):
    expected_response = 24
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_size_by_customer_id.return_value = expected_response

    response = client.get(f'/carts/{str("a"*24)}/size')

    assert response.status_code == 200
    assert response.json() == expected_response


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_size_value_error(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_size_by_customer_id.side_effect = ValueError(
        "Invalid parameters: ")

    response = client.get(f'/carts/{str("a"*24)}/size')

    assert response.status_code == 422
    assert "Invalid parameters: " in response.json()["detail"]


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_size_not_found(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_size_by_customer_id.side_effect = NotFoundError(
        "asd")

    response = client.get(f'/carts/{str("a"*24)}/size')

    assert response.status_code == 404
    assert response.json() == {"detail": "Cart not found"}


@patch('services.carts_service.CartsService.get_instance')
def test_get_cart_size_exception(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_cart_size_by_customer_id.side_effect = Exception("")

    response = client.get(f'/carts/{str("a"*24)}/size')

    assert response.status_code == 500
    assert response.json() == {"detail": ""}


def test_update_cart_change_without_quantity():
    response = client.patch(f'/carts/{mock_db_carts[0].customer_id}', json=jsonable_encoder(CartPatchDto(
        action=CartPatchDtoAction.CHANGE,
        product_id=mock_db_carts[0].items[0].product_id))
    )

    assert response.status_code == 422
    assert response.json()[
        "detail"] == "Invalid parameters: quantity is required for action CHANGE"


@patch('services.carts_service.CartsService.get_instance')
def test_update_cart_change_quantity(mock_get_instance):
    mock_cart = mock_db_carts[0]
    mock_response = CartFullDto.from_model(mock_cart)
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.edit_cart_item.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.patch(f'/carts/{str("a"*24)}', json=CartPatchDto(
        action=CartPatchDtoAction.CHANGE.value, product_id=str(uuid.uuid4()), quantity=1).dict()
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.carts_service.CartsService.get_instance')
def test_update_cart_add_product(mock_get_instance):
    mock_cart = mock_db_carts[0]
    mock_response = CartFullDto.from_model(mock_cart)
    mock_response.items[0].quantity += 1
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.edit_cart_item.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.patch(f'/carts/{str("a"*24)}', json=CartPatchDto(
        action=CartPatchDtoAction.CHANGE.value, product_id=str(uuid.uuid4()), quantity=1).dict()
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.carts_service.CartsService.get_instance')
def test_update_cart_decrement_product(mock_get_instance):
    mock_cart = mock_db_carts[0]
    mock_response = CartFullDto.from_model(mock_cart)
    mock_response.items[0].quantity -= 1
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.edit_cart_item.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.patch(f'/carts/{str("a"*24)}', json=CartPatchDto(
        action=CartPatchDtoAction.CHANGE.value, product_id=str(uuid.uuid4()), quantity=1).dict()
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.carts_service.CartsService.get_instance')
def test_create_cart(mock_get_instance):
    mock_cart = mock_db_carts[0]
    mock_response = CartFullDto.from_model(mock_cart)
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.create_cart.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.post(f'/carts/{str("a"*24)}')

    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)
