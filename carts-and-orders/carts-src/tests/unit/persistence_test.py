import unittest
from models.cart import CartItem
from mock_alchemy.mocking import UnifiedAlchemyMagicMock, AlchemyMagicMock
from unittest.mock import MagicMock, patch

from persistence.carts_persistence import CartsPersistence
from tests.mocks.db_mocks import mock_db_carts
from sqlalchemy.orm import Session


class TestCartsPersistence(unittest.TestCase):
    def test_get_cart_by_id(self):
        # UnifiedAlchemyMagicMock() does not support get which is used in tested method,
        # so we use AlchemyMagicMock that returns random data
        session = AlchemyMagicMock()

        persistence = CartsPersistence(session)

        cart = persistence.get_cart_by_customer_id(
            mock_db_carts[0].customer_id)
        self.assertIsNotNone(cart)

    def test_get_cart_size(self):
        # UnifiedAlchemyMagicMock() does not support complex queries
        session = AlchemyMagicMock()

        persistence = CartsPersistence(session)
        cart_size = persistence.get_cart_size_by_customer_id(
            mock_db_carts[0].customer_id)

        self.assertIsNotNone(cart_size)

    @patch.object(Session, 'get')
    def test_increment_cart_item_quantity(self, mock_get):
        persistence = CartsPersistence(Session())
        item = mock_db_carts[0].items[0]
        expected_qty = item.quantity + 1
        mock_get.return_value = item

        new_item = persistence.increment_cart_item_quantity(
            item.customer_id, item.product_id)

        self.assertEqual(expected_qty, new_item.quantity)

    @patch.object(Session, 'get')
    def test_decrement_cart_item_quantity(self, mock_get):
        persistence = CartsPersistence(Session())
        item = mock_db_carts[0].items[0]
        expected_qty = item.quantity - 1
        mock_get.return_value = item

        new_item = persistence.decrement_cart_item_quantity(
            item.customer_id, item.product_id)

        self.assertEqual(expected_qty, new_item.quantity)

    def test_remove_cart_item(self):
        session = AlchemyMagicMock()
        persistence = CartsPersistence(session)
        item = mock_db_carts[0].items[0]

        persistence.remove_cart_item(item.customer_id, item.product_id)

        session.delete.assert_called_once()

    def test_change_cart_item_quantity(self):
        session = AlchemyMagicMock()
        persistence = CartsPersistence(session)
        item = mock_db_carts[0].items[0]
        expected_qty = 10

        new_item = persistence.change_cart_item_quantity(
            item.customer_id, item.product_id, expected_qty)

        self.assertEqual(expected_qty, new_item.quantity)

    def test_create_cart(self):
        session = UnifiedAlchemyMagicMock()
        persistence = CartsPersistence(session)

        cart = persistence.create_cart(mock_db_carts[0].customer_id)

        self.assertIsNotNone(cart)
        session.add_all.assert_called_once()

    @patch.object(Session, 'refresh')
    @patch.object(Session, 'get')
    def test_update_cart_total_price(self, mock_get, mock_refresh):
        persistence = CartsPersistence(Session())
        cart = mock_db_carts[0]
        expected_price = cart.total_price * 10
        mock_get.return_value = cart

        new_cart = persistence.update_cart_total_price(
            cart.customer_id, expected_price)

        self.assertEqual(expected_price, new_cart.total_price)
