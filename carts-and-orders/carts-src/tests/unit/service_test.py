import uuid
import unittest
from unittest.mock import MagicMock, patch

from api.dto.cart_patch_dto import CartPatchDtoAction
from services.carts_service import CartsService
from api.dto.cart_full_dto import CartFullDto
from tests.mocks.db_mocks import mock_db_carts


class TestCartsService(unittest.TestCase):

    @patch('requests.get', return_value=MagicMock(status_code=200))
    @patch('persistence.carts_persistence.CartsPersistence.get_instance', return_value=MagicMock())
    def test_get_cart_by_customer_id(self, mock_get_instance, mock_requests):
        mock_get_instance.return_value.get_cart_by_customer_id.return_value = mock_db_carts[0]
        expected = CartFullDto.from_model(mock_db_carts[0])

        carts = CartsService().get_instance().get_cart_by_customer_id(
            mock_db_carts[0].customer_id)

        self.assertIsNotNone(carts)
        self.assertEqual(carts, expected)

    @patch('requests.get', return_value=MagicMock(status_code=200))
    @patch('persistence.carts_persistence.CartsPersistence.get_instance', return_value=MagicMock())
    def test_create_cart(self, mock_get_instance, mock_requests):
        cart = mock_db_carts[0]
        mock_get_instance.return_value.create_cart.return_value = cart
        expected = CartFullDto.from_model(cart)

        new_cart = CartsService().get_instance().create_cart(customer_id=cart.customer_id)

        self.assertIsNotNone(new_cart)
        self.assertEqual(new_cart, expected)

    @patch('requests.get', return_value=MagicMock(status_code=200))
    @patch('persistence.carts_persistence.CartsPersistence.get_instance', return_value=MagicMock())
    def test_get_cart_size_by_customer_id(self, mock_get_instance, mock_requests):
        expected = 10
        mock_get_instance.return_value.get_cart_size_by_customer_id.return_value = 10

        size = CartsService().get_instance().get_cart_size_by_customer_id(customer_id="a"*24)

        self.assertEqual(size, expected)

    @patch('requests.get', return_value=MagicMock(status_code=200))
    @patch('persistence.carts_persistence.CartsPersistence.get_instance', return_value=MagicMock())
    def test_edit_cart_item(self, mock_get_instance, mock_requests):
        mock_cart = mock_db_carts[0]
        mock_get_instance.return_value.edit_cart_item.return_value = mock_cart
        mock_get_instance.return_value.get_cart_by_customer_id.return_value = mock_cart
        mock_get_instance.return_value.update_cart_total_price.return_value = mock_cart
        expected = CartFullDto.from_model(mock_cart)

        cart = CartsService().get_instance().edit_cart_item(customer_id="a"*24,
                                                            product_id="b"*24,
                                                            quantity=10,
                                                            action=CartPatchDtoAction.CHANGE)

        self.assertEqual(cart, expected)
