from copy import deepcopy
import json
from fastapi.encoders import jsonable_encoder
from requests import Response
from fastapi.testclient import TestClient
import pytest
from unittest.mock import MagicMock, patch

from api.dto.cart_patch_dto import CartPatchDto, CartPatchDtoAction
from api.dto.cart_full_dto import CartFullDto
from api.carts_api import app
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_carts
from tests.utils.db_utils import setup_db, seed_db, drop_db, create_cart


client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown_function():
    yield

    drop_db()


PRODUCT_PRICE = 10


def _mocked_requests_get(*args, **kwargs):
    response_content = None
    request_url = kwargs.get('url', None)
    if request_url is None:
        request_url = args[0]  # if passed as positional argument
    if "products" in request_url:
        response_content = json.dumps(
            {
                "product_id": "asdasd",
                "quantity": mock_db_carts[0].items[0].quantity,
                "price": 10.0,
                "images": ["asdasd", "asdasdasd"],
                "name": "asdasd"
            }
        )
    response = Response()
    response.status_code = 200
    if response_content:
        response._content = str.encode(response_content)
    return response


def test_health_check():
    response = client.get("/carts/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('requests.get', return_value=MagicMock(status_code=200))
def test_get_cart_by_customer_id(mock_requests):
    cart1 = mock_db_carts[0]
    seed_db()
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.get(f'carts/{cart1.customer_id}')

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', return_value=MagicMock(status_code=200))
def test_get_cart_by_customer_id_unexistant(mock_requests):
    cart1 = mock_db_carts[0]
    expected_response = {"detail": "Cart not found"}

    response = client.get(f'carts/{cart1.customer_id}')

    assert response.status_code == 404
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', return_value=MagicMock(status_code=200))
def test_create_cart(mock_requests):
    cart1 = deepcopy(mock_db_carts[0])
    cart1.items = []  # on creation, items is empty
    cart1.total_price = 0
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.post(f'/carts/{cart1.customer_id}')

    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=_mocked_requests_get)
def test_update_cart_add_product(mock_requests):
    cart1 = deepcopy(mock_db_carts[0])
    cart1.total_price = PRODUCT_PRICE * cart1.items[0].quantity
    create_cart(cart1.customer_id)  # create cart with no items
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.patch(f'/carts/{cart1.customer_id}', json=jsonable_encoder(CartPatchDto(
        action=CartPatchDtoAction.CHANGE,
        product_id=cart1.items[0].product_id,
        quantity=cart1.items[0].quantity))
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=_mocked_requests_get)
def test_update_cart_remove_product(mock_requests):
    cart1 = deepcopy(mock_db_carts[0])
    create_cart(cart1.customer_id, cart1.items[:1])  # create cart with items
    cart1.total_price = 0
    cart1.items = []  # on removal, items is empty
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.patch(f'/carts/{mock_db_carts[0].customer_id}', json=jsonable_encoder(CartPatchDto(
        action=CartPatchDtoAction.REMOVE,
        product_id=mock_db_carts[0].items[0].product_id))
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=_mocked_requests_get)
def test_update_cart_change_product_quantity(mock_requests):
    cart1 = deepcopy(mock_db_carts[0])
    create_cart(cart1.customer_id, cart1.items[:1])  # create cart with items
    new_quantity = 2
    cart1.total_price = PRODUCT_PRICE * new_quantity
    cart1.items[0].quantity = new_quantity
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.patch(f'/carts/{mock_db_carts[0].customer_id}', json=jsonable_encoder(CartPatchDto(
        action=CartPatchDtoAction.CHANGE,
        product_id=mock_db_carts[0].items[0].product_id,
        quantity=new_quantity))
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=_mocked_requests_get)
def test_update_cart_increment_product_quantity(mock_requests):
    cart1 = deepcopy(mock_db_carts[0])
    create_cart(cart1.customer_id, cart1.items[:1])  # create cart with items
    new_quantity = cart1.items[0].quantity + 1
    cart1.total_price = PRODUCT_PRICE * new_quantity
    cart1.items[0].quantity = new_quantity
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.patch(f'/carts/{mock_db_carts[0].customer_id}', json=jsonable_encoder(CartPatchDto(
        action=CartPatchDtoAction.INCREMENT,
        product_id=mock_db_carts[0].items[0].product_id))
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=_mocked_requests_get)
def test_update_cart_decrement_product_quantity(mock_requests):
    cart1 = deepcopy(mock_db_carts[0])
    create_cart(cart1.customer_id, cart1.items[:1])  # create cart with items
    new_quantity = cart1.items[0].quantity - 1
    cart1.total_price = PRODUCT_PRICE * new_quantity
    cart1.items[0].quantity = new_quantity
    expected_response = CartFullDto.from_model(cart1).dict()

    response = client.patch(f'/carts/{mock_db_carts[0].customer_id}', json=jsonable_encoder(CartPatchDto(
        action=CartPatchDtoAction.DECREMENT,
        product_id=mock_db_carts[0].items[0].product_id))
    )

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)
