import os
from typing import List

from func_timeout import FunctionTimedOut, func_timeout

from api.dto.cart_item_dto import CartItemDto
from exceptions.not_found_error import NotFoundError
from sqlalchemy import create_engine, distinct, func
from sqlalchemy.orm import sessionmaker
from models.base import Base
from models.cart import Cart, CartItem


class CartsPersistence:

    _instance = None

    def __init__(self, session=None):
        if session is not None:
            self.session = session
            return
        database_name = os.environ.get('POSTGRES_DB')
        database_user = os.environ.get('POSTGRES_USER')
        database_password = os.environ.get('POSTGRES_PASSWORD')
        self.engine = create_engine(
            f'postgresql://{database_user}:{database_password}@carts-and-orders-psql:5432/{database_name}')
        self.session = sessionmaker(
            autocommit=False, autoflush=False, bind=self.engine)()
        # Creates the tables if they don't exist
        Base.metadata.create_all(bind=self.engine, tables=[
                                 Cart.__table__, CartItem.__table__])

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: self.session.query(Cart).first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def get_cart_by_customer_id(self,
                                customer_id: str) -> Cart:
        cart = self.session.get(Cart, customer_id)

        if cart is None:
            raise NotFoundError(
                f'Cart with customer_id {customer_id} not found')

        return cart

    def get_cart_size_by_customer_id(self, customer_id: str):
        query = self.session.\
            query(func.sum(CartItem.quantity)).\
            filter(CartItem.customer_id == customer_id)

        result = query.scalar()

        return result if result is not None else 0

    def increment_cart_item_quantity(self,
                                     customer_id: str,
                                     product_id: str,
                                     ):
        cart_item = self.session.get(CartItem, (customer_id, product_id))

        if cart_item is None:
            raise NotFoundError(
                f'Cart item with product_id {product_id} not found')

        cart_item.quantity += 1

        self.session.commit()
        return cart_item

    def decrement_cart_item_quantity(self,
                                     customer_id: str,
                                     product_id: str,
                                     ):
        cart_item = self.session.get(CartItem, (customer_id, product_id))

        if cart_item is None:
            raise NotFoundError(
                f'Cart item with product_id {product_id} not found')

        cart_item.quantity -= 1

        self.session.commit()
        return cart_item

    def remove_cart_item(self,
                         customer_id: str,
                         product_id: str,
                         ):
        cart_item = self.session.get(CartItem, (customer_id, product_id))

        if cart_item is None:
            raise NotFoundError(
                f'Cart item with product_id {product_id} not found')

        self.session.delete(cart_item)
        self.session.commit()

    def change_cart_item_quantity(self,
                                  customer_id: str,
                                  product_id: str,
                                  quantity: int,
                                  ):
        cart_item = self.session.get(CartItem, (customer_id, product_id))
        if cart_item is None:
            cart_item = CartItem(
                customer_id=customer_id,
                product_id=product_id,
                quantity=quantity,
            )
            self.session.add(cart_item)
        else:
            cart_item.quantity = quantity

        self.session.commit()
        return cart_item

    def create_cart(self,
                    customer_id: str,
                    ):
        cart = Cart(
            customer_id=customer_id,
        )

        self.session.add_all([cart])
        self.session.commit()
        self.session.refresh(cart)

        return cart

    def update_cart_total_price(self,
                                customer_id: str,
                                total_price: float,
                                ):
        cart = self.session.get(Cart, customer_id)

        if cart is None:
            raise NotFoundError(
                f'Cart with customer_id {customer_id} not found')

        cart.total_price = total_price

        self.session.commit()
        self.session.refresh(cart)

        return cart
