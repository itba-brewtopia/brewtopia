from datetime import datetime
import os
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Numeric, DateTime
from models.base import Base
from sqlalchemy.orm import relationship


class Cart(Base):
    __tablename__ = "carts" + (os.environ.get("TABLE_SUFFIX")
                               if os.environ.get("TABLE_SUFFIX") is not None else "")

    customer_id = Column(String, primary_key=True)
    created_at = Column(DateTime(timezone=True), default=datetime.utcnow)
    modified_at = Column(DateTime(timezone=True),
                         default=datetime.utcnow, onupdate=datetime.utcnow)
    total_price = Column(Numeric(10, 2), default=0)
    is_deleted = Column(Boolean, default=False)
    items = relationship("CartItem")


class CartItem(Base):
    __tablename__ = "cart_items" + (os.environ.get("TABLE_SUFFIX")
                                    if os.environ.get("TABLE_SUFFIX") is not None else "")

    # primary key is a composite of product_id and cart_id
    customer_id = Column(String, ForeignKey(
        Cart.customer_id), primary_key=True)
    product_id = Column(String, primary_key=True)
    quantity = Column(Integer)
