from enum import Enum


class OrderStatus(str, Enum):
    PENDING = 'PENDING'
    IN_TRANSIT = 'IN_TRANSIT'
    READY = 'READY'
    PICKED_UP = 'PICKED_UP'
    CANCELLED = 'CANCELLED'

    @classmethod
    def is_valid(cls, status: str):
        return status in cls.__members__
