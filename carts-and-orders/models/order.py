from datetime import datetime
import os

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Boolean, Column, ForeignKey, Integer, Numeric, String,  DateTime
from sqlalchemy.orm import relationship
from models.base import Base
import uuid
from sqlalchemy.ext.hybrid import hybrid_property

from models.order_status import OrderStatus


class Order(Base):
    __tablename__ = "orders" + (os.environ.get("TABLE_SUFFIX")
                                if os.environ.get("TABLE_SUFFIX") is not None else "")

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    customer_id = Column(String)
    created_at = Column(DateTime(timezone=True), default=datetime.utcnow)
    modified_at = Column(DateTime(timezone=True),
                         default=datetime.utcnow, onupdate=datetime.utcnow)
    pickup_date = Column(DateTime(timezone=True), nullable=True)
    pickup_store_id = Column(String)
    pickup_ready_date = Column(DateTime(timezone=True), nullable=True)
    status = Column(String, default=OrderStatus.PENDING.value)
    is_deleted = Column(Boolean, default=False)
    items = relationship("OrderItem")

    @hybrid_property
    def total_price(self):
        return sum([item.price * item.quantity for item in self.items])


class OrderItem(Base):
    __tablename__ = "order_items" + (os.environ.get("TABLE_SUFFIX")
                                     if os.environ.get("TABLE_SUFFIX") is not None else "")

    # primary key is a composite of product_id and order_id
    product_id = Column(String, primary_key=True)
    order_id = Column(UUID, ForeignKey(
        Order.id), primary_key=True)
    quantity = Column(Integer)
    price = Column(Numeric(10, 2))
    image = Column(String, nullable=True)
    name = Column(String)
