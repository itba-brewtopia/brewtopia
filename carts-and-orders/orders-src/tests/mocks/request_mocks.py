

import json

from requests import Response
from tests.mocks.db_mocks import mock_db_orders


def mocked_requests(*args, **kwargs):
    response_content = None
    request_url = kwargs.get('url', None)
    if request_url is None and len(args) > 0:
        request_url = args[0]  # if passed as positional argument

    response = Response()
    response.status_code = 200
    if "products" in request_url:
        response_content = json.dumps(
            {
                "product_id": mock_db_orders[0].items[0].product_id,
                "quantity": mock_db_orders[0].items[0].quantity,
                "price": mock_db_orders[0].items[0].price,
                "images": [mock_db_orders[0].items[0].image, "https://picsum.photos/201"],
                "name": mock_db_orders[0].items[0].name
            }
        )
    elif "stock/available" in request_url:
        response_content = "2023-07-23T15:48:40.281265"
    elif "carts" in request_url:
        response_content = json.dumps(
            {
                "customer_id": mock_db_orders[0].customer_id,
                "items": [
                    {
                        "product_id": mock_db_orders[0].items[0].product_id,
                        "quantity": mock_db_orders[0].items[0].quantity
                    }
                ]
            }
        )

    if response_content:
        response._content = str.encode(response_content)
    return response
