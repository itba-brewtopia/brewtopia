import datetime
import uuid
from models.order import Order, OrderItem
from models.order_status import OrderStatus

order1_id = uuid.uuid4()
mock_db_orders = [
    Order(
        id=order1_id,
        customer_id='a'*24,
        pickup_store_id='5f4f3f2f1e0d0c0b0a090811',
        pickup_ready_date=datetime.datetime.utcnow(),
        status=OrderStatus.PENDING.value,
        is_deleted=False,
        items=[OrderItem(
            product_id='5f4f3f2f1e0d0c0b0a090820',
            order_id=order1_id,
            quantity=1,
            price=100,
            name='product1',
            image='https://picsum.photos/200'
        )]
    ),
    Order(
        id=uuid.uuid4(),
        customer_id='b'*24,
        pickup_store_id='5f4f3f2f1e0d0c0b0a090811',
        pickup_ready_date=datetime.datetime.utcnow(),
        status=OrderStatus.PENDING.value,
        is_deleted=False,
        items=[]
    ),
    Order(
        id=uuid.uuid4(),
        customer_id='c'*24,
        pickup_store_id='5f4f3f2f1e0d0c0b0a090811',
        pickup_ready_date=datetime.datetime.utcnow(),
        status=OrderStatus.PENDING.value,
        is_deleted=False,
    ),
]
