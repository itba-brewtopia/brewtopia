import uuid
from fastapi.testclient import TestClient
from unittest.mock import MagicMock, patch

from api.dto.order_create_dto import OrderCreateDto
from models.order_status import OrderStatus
from api.orders_api import app
from api.dto.order_patch_dto import OrderPatchDto
from api.dto.order_full_dto import OrderFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_orders

client = TestClient(app)


@patch('services.orders_service.OrdersService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/orders/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('services.orders_service.OrdersService.get_instance')
def test_get_orders(mock_get_instance):
    mock_response = [OrderFullDto.from_model(
        order) for order in mock_db_orders]
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_orders.return_value = mock_response
    expected_response = [order.dict() for order in mock_response]

    response = client.get("/orders")

    assert response.status_code == 200
    assert_equal_list_of_dicts(response.json(), expected_response)


def test_get_invalid_status():
    response = client.get(f"/orders", params={"status": "INVALID"})

    assert response.status_code == 422


@patch('services.orders_service.OrdersService.get_instance')
def test_get_order_by_id(mock_get_instance):
    mock_response = OrderFullDto.from_model(
        mock_db_orders[0])
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_order_by_id.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.get(f"/orders/{mock_db_orders[0].id}")

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.orders_service.OrdersService.get_instance')
def test_get_order_by_id_not_found(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_order_by_id.side_effect = ValueError("asd")

    response = client.get(f"/orders/{mock_db_orders[0].id}")

    assert response.status_code == 404
    assert "Order not found" in response.json()["detail"]


@patch('services.orders_service.OrdersService.get_instance')
def test_get_order_by_id_not_found(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_order_by_id.return_value = None

    response = client.get(f'/orders/{uuid.uuid4()}')

    assert response.status_code == 404


@patch('services.orders_service.OrdersService.get_instance')
def test_create_order(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    order = mock_db_orders[0]
    mock_response = OrderFullDto.from_model(
        order)
    mock_service.create_order.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.post("/orders", json=OrderCreateDto(
        customer_id=order.customer_id,
        pickup_store_id=order.pickup_store_id
    ).dict())

    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


@patch('services.orders_service.OrdersService.get_instance')
def test_update_order(mock_get_instance):
    order = mock_db_orders[0]
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_response = OrderFullDto.from_model(
        order)
    mock_service.update_order.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.patch(f"/orders/{order.id}", json=OrderPatchDto(
        status=OrderStatus.PENDING,
    ).dict())

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.orders_service.OrdersService.get_instance')
def test_update_order_by_id_not_found(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.update_order.side_effect = ValueError("asd")

    response = client.patch(f"/orders/{mock_db_orders[0].id}", json=OrderPatchDto(
        status=OrderStatus.PENDING,
    ).dict())

    assert response.status_code == 404
    assert "Order not found" in response.json()["detail"]
