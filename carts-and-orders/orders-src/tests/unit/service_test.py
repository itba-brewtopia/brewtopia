import json
import unittest
from unittest.mock import MagicMock, patch

from requests import Response

from models.order_status import OrderStatus
from services.orders_service import OrdersService
from api.dto.order_full_dto import OrderFullDto
from tests.mocks.db_mocks import mock_db_orders
from tests.mocks.request_mocks import mocked_requests


class TestOrdersService(unittest.TestCase):

    @patch('persistence.orders_persistence.OrdersPersistence.get_instance', return_value=MagicMock())
    def test_get_orders(self, mock_get_instance):
        mock_get_instance.return_value.get_orders.return_value = mock_db_orders
        expected = [OrderFullDto.from_model(
            order) for order in mock_db_orders]

        orders = OrdersService().get_instance().get_orders()

        self.assertEqual(len(orders), len(expected))
        self.assertEqual(orders, expected)

    @patch('persistence.orders_persistence.OrdersPersistence.get_instance', return_value=MagicMock())
    def test_get_order_by_id(self, mock_get_instance):
        mock_get_instance.return_value.get_order_by_id.return_value = mock_db_orders[0]
        expected = OrderFullDto.from_model(mock_db_orders[0])

        orders = OrdersService().get_instance().get_order_by_id(
            mock_db_orders[0].id)

        self.assertIsNotNone(orders)
        self.assertEqual(orders, expected)

    @patch('requests.post', side_effect=mocked_requests)
    @patch('requests.patch', return_value=MagicMock(status_code=200))
    @patch('requests.get', side_effect=mocked_requests)
    @patch('persistence.orders_persistence.OrdersPersistence.get_instance', return_value=MagicMock())
    def test_create_order(self, mock_get_instance, mock_requests_get, mock_requests_patch, mock_requests_post):
        order = mock_db_orders[0]
        mock_get_instance.return_value.create_order.return_value = order
        expected = OrderFullDto.from_model(order)

        new_order = OrdersService().get_instance().create_order(
            customer_id=order.customer_id,
            pickup_store_id=order.pickup_store_id,
        )

        self.assertIsNotNone(new_order)
        self.assertEqual(new_order, expected)

    @patch('persistence.orders_persistence.OrdersPersistence.get_instance', return_value=MagicMock())
    def test_update_order(self, mock_get_instance):
        order = mock_db_orders[0]
        order.status = OrderStatus.PENDING.value
        mock_get_instance.return_value.update_order.return_value = order
        expected = OrderFullDto.from_model(order)

        order_with_items = OrdersService().get_instance().update_order(
            order_id=order.id,
            status=OrderStatus.READY,
        )

        self.assertIsNotNone(order_with_items)
        self.assertEqual(order_with_items, expected)
