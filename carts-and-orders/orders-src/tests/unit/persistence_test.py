import json
import unittest
import uuid

from requests import Response

from models.order_status import OrderStatus
from models.order import OrderItem
from mock_alchemy.mocking import UnifiedAlchemyMagicMock, AlchemyMagicMock
from unittest.mock import MagicMock, patch
from sqlalchemy.orm import Session

from api.dto.order_item_dto import OrderItemDto
from persistence.orders_persistence import OrdersPersistence
from tests.mocks.db_mocks import mock_db_orders


def _mocked_requests_get(*args, **kwargs):
    response_content = None
    request_url = kwargs.get('url', None)
    if request_url is None:
        request_url = args[0]  # if passed as positional argument
    if "products" in request_url:
        response_content = json.dumps(
            {
                "product_id": "asdasd",
                "quantity": 10,
                "price": 10.0,
                "images": ["asdasd", "asdasdasd"],
                "name": "asdasd"
            }
        )
    response = Response()
    response.status_code = 200
    if response_content:
        response._content = str.encode(response_content)
    return response


class TestOrdersPersistence(unittest.TestCase):
    def test_get_orders(self):
        session = UnifiedAlchemyMagicMock()
        for order in mock_db_orders:
            session.add(order)

        persistence = OrdersPersistence(session)
        orders = persistence.get_orders()

        self.assertEqual(len(orders), len(mock_db_orders))

    def test_get_orders_with_params(self):
        # UnifiedAlchemyMagicMock() does not support complex queries, therefore we just check if filter was called
        session = UnifiedAlchemyMagicMock()

        persistence = OrdersPersistence(session)
        persistence.get_orders(customer_id=uuid.uuid4(),
                               status=OrderStatus.PENDING)

        session.filter.assert_called_once()

    def test_get_order_by_id(self):
        # UnifiedAlchemyMagicMock() does not support get which is used in tested method,
        # so we use AlchemyMagicMock that returns random data
        session = AlchemyMagicMock()

        persistence = OrdersPersistence(session)

        order = persistence.get_order_by_id(mock_db_orders[0].id)
        self.assertIsNotNone(order)

    def test_create_order(self):
        session = AlchemyMagicMock()

        persistence = OrdersPersistence(session)
        mock_order = mock_db_orders[0]
        order = persistence.create_order(
            customer_id=mock_order.customer_id,
            pickup_store_id=mock_order.pickup_store_id,
            items=[OrderItemDto(
                product_id="asdasd",
                quantity=10,
                price=10.0,
                image="asdasd",
                name="asdasd"
            )],
            pickup_ready_date="2021-01-01"
        )

        self.assertIsNotNone(order)
        self.assertEqual(order.customer_id, mock_order.customer_id)
        self.assertEqual(order.pickup_store_id, mock_order.pickup_store_id)

    @patch.object(Session, 'refresh')
    @patch.object(Session, 'get')
    def test_update_order(self, mock_get, mock_refresh):
        persistence = OrdersPersistence(Session())
        mock_order = mock_db_orders[0]
        mock_get.return_value = mock_order

        new_order = persistence.update_order(
            order_id=mock_db_orders[0].id,
            status=OrderStatus.READY
        )

        self.assertEqual(new_order.status, OrderStatus.READY.value)
