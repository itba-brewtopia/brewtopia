import copy
import datetime
import json

from requests import Response
from models.cart import CartItem
from fastapi.testclient import TestClient
from unittest.mock import MagicMock, patch
import pytest

from api.orders_api import app
from api.dto.order_item_dto import OrderItemDto
from api.dto.order_patch_dto import OrderPatchDto
from api.dto.order_create_dto import OrderCreateDto
from api.dto.order_full_dto import OrderFullDto
from persistence.orders_persistence import OrdersPersistence
from models.order_status import OrderStatus
from services.orders_service import OrdersService
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_orders
from tests.utils.db_utils import setup_db, seed_db, drop_db, create_order, create_cart
from tests.mocks.request_mocks import mocked_requests

client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown_function():
    yield

    drop_db()


def test_health_check():
    response = client.get("/orders/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_get_orders():
    seed_db()
    expected_response = [OrderFullDto.from_model(
        order).dict() for order in mock_db_orders]

    response = client.get("/orders", params={})

    assert response.status_code == 200
    assert_equal_list_of_dicts(expected_response, response.json())


def test_get_order_by_id():
    seed_db()
    order = mock_db_orders[0]
    expected_response = OrderFullDto.from_model(order).dict()

    response = client.get(f'/orders/{order.id}')

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


def test_get_order_by_id_none():
    response = client.get(f'/orders/{mock_db_orders[0].id}')

    assert response.status_code == 404
    assert response.json() == {"detail": "Order not found"}


@patch('requests.post', side_effect=mocked_requests)
@patch('requests.patch', return_value=MagicMock(status_code=200))
@patch('requests.get', side_effect=mocked_requests)
def test_create_order(mock_requests_get, mock_requests_patch, mock_requests_post):
    order = mock_db_orders[0]
    create_cart(customer_id=order.customer_id,
                items=[CartItem(customer_id=order.customer_id, product_id=item.product_id,
                                quantity=item.quantity) for item in order.items])
    expected_response = OrderFullDto.from_model(
        order).dict()
    expected_response["status"] = OrderStatus.PENDING.value

    response = client.post("/orders", json=OrderCreateDto(
        customer_id=order.customer_id,
        pickup_store_id=order.pickup_store_id).dict())

    print(response.json())
    # assign values that are not known before creation
    expected_response["id"] = response.json()["id"]
    expected_response["pickup_ready_date"] = response.json()[
        "pickup_ready_date"]
    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', return_value=MagicMock(status_code=404))
def test_create_order_params_not_found(mock_requests):
    response = client.patch(f'/orders/{mock_db_orders[0].id}', json=OrderCreateDto(
        customer_id=mock_db_orders[0].customer_id,
        pickup_store_id=mock_db_orders[0].pickup_store_id).dict())

    assert response.status_code == 422


@patch('requests.get', side_effect=mocked_requests)
def test_update_order_in_transit(mock_requests):
    seed_db()
    order = mock_db_orders[0]
    expected_response = OrderFullDto.from_model(order).dict()
    expected_response["status"] = OrderStatus.IN_TRANSIT.value

    response = client.patch(f'/orders/{order.id}', json=OrderPatchDto(
        status=OrderStatus.IN_TRANSIT.value
    ).dict())

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=mocked_requests)
def test_update_order_ready(mock_requests):
    seed_db()
    order = mock_db_orders[0]
    expected_response = OrderFullDto.from_model(order).dict()
    expected_response["status"] = OrderStatus.READY.value
    expected_response["pickup_ready_date"] = datetime.datetime.utcnow().isoformat()[
        :10]

    response = client.patch(f'/orders/{order.id}', json=OrderPatchDto(
        status=OrderStatus.READY.value
    ).dict())

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=mocked_requests)
def test_update_order_picked_up(mock_requests):
    seed_db()
    order = mock_db_orders[0]
    expected_response = OrderFullDto.from_model(order).dict()
    expected_response["status"] = OrderStatus.PICKED_UP.value
    expected_response["pickup_date"] = datetime.datetime.utcnow().isoformat()[
        :10]

    response = client.patch(f'/orders/{order.id}', json=OrderPatchDto(
        status=OrderStatus.PICKED_UP.value
    ).dict())

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.get', side_effect=mocked_requests)
def test_update_order_not_found(mock_requests):
    response = client.patch(f'/orders/{mock_db_orders[0].id}', json=OrderPatchDto(
        status=OrderStatus.PICKED_UP.value
    ).dict())

    assert response.status_code == 404
    print(response.json())
    assert "Order not found" in response.json()["detail"]
