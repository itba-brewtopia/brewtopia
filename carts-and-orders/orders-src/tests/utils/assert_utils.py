import math


def assert_equal_dicts(d1, d2):
    assert isinstance(d1, dict) and isinstance(
        d2, dict), "Both arguments should be dict"

    d1_keys = list(sorted(d1.keys()))
    d2_keys = list(sorted(d2.keys()))

    print("L1", d1_keys)
    print("L2", d2_keys)
    assert d1_keys == d2_keys, "Dictionaries don't have the same keys"

    for key in d1_keys:
        if key == "created_at" or key == "modified_at" or key == "id":
            continue

        if isinstance(d1[key], dict):
            assert_equal_dicts(d1[key], d2[key])  # recursion on nested dict
        elif isinstance(d1[key], list):
            assert_equal_list_of_dicts(d1[key], d2[key])
        elif d1[key] is None and d2[key] is None:
            continue  # if both are None, just continue to the next key
        else:
            if "price" in key:
                p1 = float(d1[key])
                p2 = float(d2[key])
                print(p1, p2)
                assert math.isclose(
                    p1, p2), f"Values for key {key} are not equal: {p1} vs {p2}"
            elif "date" in key:
                # only compare year, month and day
                print(d1[key], d2[key], key)
                assert (d1[key] is None and d2[key] is None) or \
                    d1[key][:10] == d2[key][:
                                            10], f"Values for date key {key} are not equal: {d1[key]} vs {d2[key]}"
            else:
                assert d1[key] == d2[key], f"Values for key {key} are not equal: {d1[key]} vs {d2[key]}"


def assert_contains_dicts(d1, d2):
    assert isinstance(d1, dict) and isinstance(
        d2, dict), "Both arguments should be dict"
    d1_keys = d1.keys()
    d2_keys = d2.keys()

    for key in d1_keys:
        if key not in d2_keys:
            raise Exception("Dictionaries don't have the same keys")

        if isinstance(d1[key], dict):
            assert_contains_dicts(d1[key], d2[key])  # recursion on nested dict

        assert d1[key] == d2[key], f"Values for key {key} are not equal: {d1[key]} vs {d2[key]}"


def assert_equal_list_of_dicts(list1, list2):
    print("L1", list1)
    print("L2", list2)
    assert isinstance(list1, list) and isinstance(
        list2, list), "Both arguments should be list"
    assert len(list1) == len(list2), "Lists are not of the same length"
    list1 = list(sorted(list1, key=lambda x: str(x)))
    list2 = list(sorted(list2, key=lambda x: str(x)))
    for d1, d2 in zip(list1, list2):
        assert_equal_dicts(d1, d2)
