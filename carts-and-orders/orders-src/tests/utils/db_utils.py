from models.base import Base
from models.order import Order, OrderItem
from models.cart import Cart, CartItem
from tests.mocks.db_mocks import mock_db_orders
from persistence.orders_persistence import OrdersPersistence
import copy


def setup_db():
    engine = OrdersPersistence().get_instance().engine
    # create cart and cart items tables
    Base.metadata.create_all(bind=engine, tables=[
        Cart.__table__, CartItem.__table__])
    session = OrdersPersistence().get_instance().session
    session.query(OrderItem).delete()
    session.query(Order).delete()
    session.query(CartItem).delete()
    session.query(Cart).delete()
    session.commit()
    session.close()


def seed_db():
    session = OrdersPersistence().get_instance().session
    for order in mock_db_orders:
        insert_order = copy.deepcopy(order)
        session.add(insert_order)  # adds order and its items
        session.commit()

    session.commit()
    session.close()


def create_order(customer_id, items=[]):
    session = OrdersPersistence().get_instance().session
    order = Order(customer_id=customer_id)
    session.add(order)
    for item in items:
        session.add(OrderItem(
            order_id=order.id, product_id=item.product_id, quantity=item.quantity,
            price=item.price, image=item.image, name=item.name
        ))  # creates deepcopy
    session.commit()
    session.close()


def create_cart(customer_id, items=[]):
    session = OrdersPersistence().get_instance().session
    cart = Cart(customer_id=customer_id)
    session.add(cart)
    for item in items:
        session.add(CartItem(customer_id=customer_id, product_id=item.product_id,
                    quantity=item.quantity))  # creates deepcopy
    session.commit()
    session.close()


def drop_db():
    session = OrdersPersistence().get_instance().session
    # rollback any pending transaction, like errors, to avoid locking for future tests
    session.rollback()
    session.query(OrderItem).delete()
    session.query(Order).delete()
    session.query(CartItem).delete()
    session.query(Cart).delete()
    session.commit()
    session.close()
