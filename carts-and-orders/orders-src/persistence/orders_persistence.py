from datetime import datetime, timedelta
import os
from typing import Optional
from func_timeout import FunctionTimedOut, func_timeout
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from uuid import UUID

from models.base import Base
from models.order import Order, OrderItem
from models.order_status import OrderStatus
from models.cart import Cart, CartItem
from api.dto.order_item_dto import OrderItemDto


def _get_day_filter(date: str):
    date = date.split('T')[0]
    return Order.pickup_ready_date.between(datetime.strptime(
        date, '%Y-%m-%d'),
        datetime.strptime(date, '%Y-%m-%d') + timedelta(days=1))


class OrdersPersistence:

    _instance = None

    def __init__(self, session=None):
        if session is not None:
            self.session = session
            return
        database_name = os.environ.get('POSTGRES_DB')
        database_user = os.environ.get('POSTGRES_USER')
        database_password = os.environ.get('POSTGRES_PASSWORD')
        self.engine = create_engine(
            f'postgresql://{database_user}:{database_password}@carts-and-orders-psql:5432/{database_name}')
        self.session = sessionmaker(
            autocommit=False, autoflush=False, bind=self.engine)()
        # Creates the tables if they don't exist
        Base.metadata.create_all(bind=self.engine, tables=[
            Order.__table__, OrderItem.__table__
        ])

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: self.session.query(Order).first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def get_orders(self,
                   pickup_ready_date: Optional[str] = None,
                   status: Optional[OrderStatus] = None,
                   store_id: Optional[str] = None,
                   customer_id: Optional[str] = None):

        query = self.session.query(Order)
        filter_args = []
        if status is not None:
            filter_args.append(Order.status == status)
        if pickup_ready_date is not None:
            filter_args.append(
                _get_day_filter(pickup_ready_date))
        if store_id is not None:
            filter_args.append(Order.pickup_store_id == store_id)
        if customer_id is not None:
            filter_args.append(Order.customer_id == customer_id)

        if len(filter_args) > 0:
            return query.filter(*filter_args)
        else:
            return query.all()

    def get_order_by_id(self, order_id: UUID):
        return self.session.get(Order, order_id)

    def create_order(self,
                     customer_id: str,
                     pickup_store_id: str,
                     items: list[OrderItemDto],
                     pickup_ready_date: datetime):

        order = Order(
            customer_id=customer_id,
            pickup_store_id=pickup_store_id,
            pickup_ready_date=pickup_ready_date
        )

        self.session.add(order)
        self.session.commit()
        self.session.refresh(order)  # necessary to get the id afterwards
        db_order_items = []
        for item in items:
            db_order_items.append(OrderItem(
                order_id=order.id,
                product_id=item.product_id,
                quantity=item.quantity,
                price=item.price,
                name=item.name,
                image=item.image,
            ))

        self.session.add_all(db_order_items)

        # delete cart items
        cart = self.session.get(Cart, customer_id)
        if cart is None:
            raise ValueError(f'Cart with customer_id {customer_id} not found')

        for cart_item in cart.items:
            self.session.delete(cart_item)

        cart.total_price = 0
        self.session.commit()
        self.session.refresh(order)

        return order

    def update_order(self, order_id: UUID, status: OrderStatus):
        order = self.session.get(Order, order_id)
        if order is None:
            raise ValueError(f'Order with id {order_id} not found')
        order.status = status.value
        if status == OrderStatus.PICKED_UP.value:
            order.pickup_date = datetime.utcnow()

        self.session.commit()
        self.session.refresh(order)
        return order
