#!/bin/bash -e 
export TABLE_SUFFIX="orders"

echo "Running tests for service"
## pytest
python -m pytest "./tests" --junitxml=report.xml

echo "Running coverage command for service"
## Coverage
python -m pytest "./tests" -p no:warnings --cov="./" --cov-report html
