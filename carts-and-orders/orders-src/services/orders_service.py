import datetime
import time
from typing import Optional
from uuid import UUID
from fastapi import Response
import requests

from api.dto.order_item_dto import OrderItemDto
from api.dto.order_full_dto import OrderFullDto

from models.order_status import OrderStatus
from persistence.orders_persistence import OrdersPersistence


def _check_store_id(store_id: str):
    response = requests.get(
        f'http://stores-service:8080/stores/{store_id}', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            raise ValueError("Invalid store id: " + store_id)
        else:
            raise Exception("Store server internal error" +
                            str(response.content))


def _check_customer_id(customer_id: str):
    response = requests.get(
        url=f'http://customers-service:8080/customers/{str(customer_id)}', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            raise ValueError("Invalid customer id: " + + str(customer_id))
        else:
            raise Exception("Customer server internal error" +
                            str(response.content))


def _get_product(product_id: str):
    response = requests.get(
        f'http://products-service:8080/products/{product_id}', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            raise ValueError("Invalid product id: " + str(product_id))
        else:
            raise Exception("Products server internal error " +
                            str(response.content))

    return response.json()


def _get_order_items(customer_id: str):
    # get price, name, image at the creation time to save it in the order
    response = requests.get(
        url=f'http://carts-service:8080/carts/{str(customer_id)}', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            raise ValueError("Invalid customer id: " + str(customer_id))
        else:
            raise Exception("Carts server internal error " +
                            str(response.content))

    items = []
    for cart_item in response.json()['items']:
        product_json = _get_product(cart_item['product_id'])
        if product_json['images'] is None or len(product_json['images']) == 0:
            image = ''
        else:
            image = product_json['images'][0]

        items.append(OrderItemDto(
            product_id=cart_item['product_id'],
            quantity=cart_item['quantity'],
            price=product_json['price'],
            name=product_json['name'],
            image=image,
        ))

    return items


def _get_pickup_ready_date(store_id: str, items: list[OrderItemDto]):
    request = {
        'store_id': store_id,
        'items': [{
            'product_id': item.product_id,
            'quantity': item.quantity
        } for item in items]
    }
    response = requests.post(
        url=f'http://stock-service:8080/stock/available', timeout=10,
        json=request)
    if response.status_code != 200:
        if response.status_code == 404 or response.status_code == 422:
            raise ValueError("Invalid request: " + str(request))
        raise Exception(
            "Stock server internal error: " + str(response))

    return response.text.strip('"')


def _post_stock_decrease(store_id: str, items: list[OrderItemDto]):
    for item in items:
        # TODO: rollback on failure?¿
        request = {
            'store_id': store_id,
            'product_id': item.product_id,
            'quantity': item.quantity,
            'action': 'decrement'
        }
        response = requests.patch(
            url=f'http://stock-service:8080/stock', timeout=10,
            json=request)

        if response.status_code != 200:
            if response.status_code == 404 or response.status_code == 422:
                raise ValueError(
                    "Invalid stock decrement request: " + str(request))
            else:
                raise Exception(
                    "Stock server internal error: " + str(response.content))


class OrdersService:

    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return OrdersPersistence.get_instance().health_check()

    def get_orders(self,
                   pickup_ready_date: Optional[str] = None,
                   status: Optional[OrderStatus] = None,
                   store_id: Optional[str] = None,
                   customer_id: Optional[str] = None):
        if store_id is not None:
            _check_store_id(store_id)
        if customer_id is not None:
            _check_customer_id(customer_id)
        orders = OrdersPersistence.get_instance().get_orders(
            status=status,
            pickup_ready_date=pickup_ready_date,
            store_id=store_id,
            customer_id=customer_id
        )

        response = [OrderFullDto.from_model(order) for order in orders]
        return response

    def get_order_by_id(self, order_id: UUID):
        order = OrdersPersistence.get_instance().get_order_by_id(order_id)

        if order is None:
            return None

        return OrderFullDto.from_model(order)

    def create_order(self,
                     customer_id: str,
                     pickup_store_id: str):
        _check_customer_id(customer_id)

        order_items = _get_order_items(customer_id)
        pickup_ready_date_str = _get_pickup_ready_date(
            pickup_store_id, order_items)
        pickup_ready_date = datetime.datetime.strptime(
            pickup_ready_date_str.split('T')[0], "%Y-%m-%d")
        _post_stock_decrease(pickup_store_id, order_items)

        order = OrdersPersistence.get_instance().create_order(
            customer_id=customer_id,
            pickup_store_id=pickup_store_id,
            items=order_items,
            pickup_ready_date=pickup_ready_date
        )

        return OrderFullDto.from_model(order)

    def update_order(self, order_id: UUID, status: OrderStatus):
        order = OrdersPersistence.get_instance()\
            .update_order(order_id, status)
        return OrderFullDto.from_model(order)
