from pydantic import BaseModel
from models.order import OrderStatus


class OrderPatchDto(BaseModel):
    status: OrderStatus
