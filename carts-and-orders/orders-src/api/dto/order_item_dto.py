from models.order import OrderItem
from pydantic import BaseModel

from typing import Optional


class OrderItemDto(BaseModel):
    product_id: str
    quantity: int
    price: str
    image: Optional[str] = None
    name: str

    @staticmethod
    def from_model(order_item: OrderItem):
        return OrderItemDto(
            product_id=order_item.product_id,
            quantity=order_item.quantity,
            price=str(order_item.price),
            image=order_item.image,
            name=order_item.name
        )
