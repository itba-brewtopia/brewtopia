from pydantic import BaseModel


class OrderCreateDto(BaseModel):
    customer_id: str
    pickup_store_id: str
