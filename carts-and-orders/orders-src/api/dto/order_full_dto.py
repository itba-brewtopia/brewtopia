from models.order import Order
from api.dto.order_item_dto import OrderItemDto
from models.order_status import OrderStatus
from pydantic import BaseModel


import uuid
from typing import Optional


class OrderFullDto(BaseModel):
    id: uuid.UUID
    customer_id: str
    created_at: str
    modified_at: str
    total_price: str
    pickup_date: Optional[str] = None
    pickup_ready_date: Optional[str] = None
    pickup_store_id: str
    status: OrderStatus
    items: list[OrderItemDto]

    @staticmethod
    def from_model(order: Order):
        return OrderFullDto(
            id=uuid.UUID(str(order.id)),
            customer_id=str(order.customer_id),
            created_at=order.created_at.isoformat() if order.created_at else '',
            modified_at=order.modified_at.isoformat() if order.modified_at else '',
            total_price=str(order.total_price),
            pickup_date=order.pickup_date.isoformat() if order.pickup_date else None,
            pickup_ready_date=order.pickup_ready_date.isoformat(
            ) if order.pickup_ready_date else None,
            pickup_store_id=str(order.pickup_store_id),
            status=OrderStatus(order.status),
            items=[OrderItemDto.from_model(item) for item in order.items]
        )
