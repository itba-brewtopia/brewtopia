from uuid import UUID
from fastapi import FastAPI, HTTPException

from typing import List, Optional
from api.dto.order_full_dto import OrderFullDto
from api.dto.order_create_dto import OrderCreateDto
from api.dto.order_patch_dto import OrderPatchDto
from services.orders_service import OrdersService
from models.order_status import OrderStatus

app = FastAPI(openapi_url="/orders/openapi.json",
              docs_url="/orders/docs", redoc_url="/orders/redoc")


@app.get("/orders/health")
async def health_check():
    service_status = OrdersService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


def _normalize_query_param(param):
    return None if param == '' else param


@app.get("/orders", response_model=List[OrderFullDto])
async def get_orders(status: Optional[OrderStatus] = None,
                     customer_id: Optional[str] = None,
                     store_id: Optional[str] = None,
                     pickup_ready_date: Optional[str] = None,
                     ):

    try:
        orders = OrdersService.get_instance().get_orders(status=OrderStatus(status) if status is not None else None,
                                                         customer_id=_normalize_query_param(
                                                             customer_id),
                                                         store_id=_normalize_query_param(
                                                             store_id),
                                                         pickup_ready_date=_normalize_query_param(pickup_ready_date))
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))

    return orders


@app.get("/orders/{order_id}", response_model=OrderFullDto)
async def get_order_by_id(order_id: UUID):
    try:
        order = OrdersService.get_instance().get_order_by_id(order_id)
        if order is None:
            raise ValueError("Order not found")
    except ValueError as e:
        raise HTTPException(status_code=404, detail="Order not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return order


@app.post("/orders", response_model=OrderFullDto, status_code=201)
async def create_order(order: OrderCreateDto):
    try:
        new_order = OrdersService.get_instance().create_order(customer_id=order.customer_id,
                                                              pickup_store_id=order.pickup_store_id)
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return new_order


@app.patch("/orders/{order_id}", response_model=OrderFullDto)
async def update_order(order_id: str, patch: OrderPatchDto):
    try:
        order = OrdersService.get_instance().update_order(order_id=order_id,
                                                          status=patch.status)
    except ValueError as e:
        raise HTTPException(status_code=404, detail="Order not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return order
