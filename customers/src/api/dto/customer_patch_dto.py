from typing import Union
from pydantic import BaseModel


class CustomerPatchDto(BaseModel):
    name: Union[str, None] = None
    last_name: Union[str, None] = None
    email: Union[str, None] = None
    password: Union[str, None] = None
    is_verified: Union[bool, None] = None
    is_deleted: Union[bool, None] = None
