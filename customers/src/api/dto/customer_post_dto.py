from pydantic import BaseModel, Field


class CustomerPostDto(BaseModel):
    name: str = Field(max_length=50)
    last_name: str = Field(max_length=50)
    email: str = Field(max_length=50)
    password: str = Field(max_length=50)
