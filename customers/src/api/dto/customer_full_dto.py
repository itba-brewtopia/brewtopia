from pydantic import BaseModel

from models.customer import Customer


class CustomerFullDto(BaseModel):
    id: str
    name: str
    last_name: str
    email: str
    is_verified: bool
    is_deleted: bool
    last_login: str
    created_at: str
    modified_at: str

    def from_model(customer: Customer):
        return CustomerFullDto(id=str(customer.id),
                               name=customer.name,
                               last_name=customer.last_name,
                               email=customer.email,
                               is_verified=customer.is_verified,
                               is_deleted=customer.is_deleted,
                               last_login=str(customer.last_login),
                               created_at=str(customer.created_at),
                               modified_at=str(customer.modified_at))
