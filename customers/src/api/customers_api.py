from typing import List
from fastapi import FastAPI, HTTPException

from api.dto.customer_full_dto import CustomerFullDto
from api.dto.customer_patch_dto import CustomerPatchDto
from api.dto.customer_post_dto import CustomerPostDto
from services.customers_service import CustomersService

app = FastAPI(openapi_url="/customers/openapi.json",
              docs_url="/customers/docs", redoc_url="/customers/redoc")


@app.get("/customers/health")
async def health_check():
    service_status = CustomersService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@app.get("/customers", response_model=List[CustomerFullDto])
async def get_customers(email: str = '', name: str = '', last_name: str = ''):
    customers = CustomersService.get_instance().get_customers(
        email=email, name=name, last_name=last_name)
    response = []
    for customer in customers:
        response.append(CustomerFullDto.from_model(customer))
    return response


@app.get("/customers/{id}", response_model=CustomerFullDto)
async def get_customer_by_id(id: str):
    customer = CustomersService.get_instance().get_customer_by_id(id)
    if customer is None:
        raise HTTPException(status_code=404, detail="Customer not found")

    return CustomerFullDto.from_model(customer)


@app.post("/customers", response_model=CustomerFullDto, status_code=201)
async def create_customer(customerDto: CustomerPostDto):
    try:
        customer_model = CustomersService.get_instance().create_customer(name=customerDto.name, last_name=customerDto.last_name,
                                                                         email=customerDto.email, password=customerDto.password)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return CustomerFullDto.from_model(customer_model)


@app.patch("/customers/{id}", response_model=CustomerFullDto)
async def update_customer(id: str, customerPatchDto: CustomerPatchDto):
    customer_model = CustomersService.get_instance().update_customer(id=id, name=customerPatchDto.name, last_name=customerPatchDto.last_name,
                                                                     email=customerPatchDto.email, password=customerPatchDto.password,
                                                                     is_verified=customerPatchDto.is_verified, is_deleted=customerPatchDto.is_deleted)
    customer = CustomerFullDto.from_model(customer_model)
    if customer is None:
        raise HTTPException(status_code=404, detail="Customer not found")

    return customer
