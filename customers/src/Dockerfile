FROM python:3.11
LABEL maintainer="[ajerusalinsky@itba.edu.ar, cborinsky@itba.edu.ar, goarca@itba.edu.ar, manurodriguez@itba.edu.ar]"

RUN groupadd -g 999 python \
    && useradd -r -u 999 -g python python \
    && mkdir -p /usr/app \
    && python -m venv /usr/app/.venv \
    && chown -R python:python /usr/app 

ENV PATH="/usr/app/.venv/bin:$PATH"
ENV PIP_NO_CACHE_DIR=off
WORKDIR /usr/app
USER 999

COPY --chown=python:python requirements.txt requirements.txt
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

COPY --chown=python:python . .

EXPOSE 8080
HEALTHCHECK CMD (curl -f http://localhost:8080/customers/health | grep -i '\"OK\"') || exit 1

CMD ["uvicorn", "api.customers_api:app", "--host", "0.0.0.0", "--port", "8080", "--workers", "10"]
