from mongoengine import Document, StringField, BooleanField, DateTimeField


class Customer(Document):
    name = StringField(max_length=200)
    last_name = StringField(max_length=200)
    email = StringField(max_length=200, unique=True)
    password = StringField(max_length=200)
    is_verified = BooleanField(default=False)
    is_deleted = BooleanField(default=False)
    last_login = StringField(max_length=20)
    created_at = DateTimeField()
    modified_at = DateTimeField()

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "last_name": self.last_name,
            "email": self.email,
            "is_verified": self.is_verified,
            "is_deleted": self.is_deleted,
            "last_login": self.last_login.strftime("%Y-%m-%d %H:%M:%S.%f") if self.last_login else None,
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.created_at else None,
            "modified_at": self.modified_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.modified_at else None,
        }
