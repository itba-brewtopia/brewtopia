from typing import Union
from api.dto.customer_post_dto import CustomerPostDto
from persistence.customers_persistence import CustomersPersistence
import requests


# singleton class
class CustomersService:

    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return CustomersPersistence.get_instance().health_check()

    def get_customers(self, email: str = '', name: str = '', last_name: str = ''):
        return CustomersPersistence.get_instance().get_customers(email=email, name=name, last_name=last_name)

    def get_customer_by_id(self, id: str):
        return CustomersPersistence.get_instance().get_customer_by_id(id)

    def create_customer(self,
                        name: str,
                        last_name: str,
                        email: str,
                        password: str):
        new_customer = CustomersPersistence.get_instance()\
            .create_customer(name, last_name=last_name, email=email, password=password)

        try:
            response = requests.post(
                f'http://carts-service:8080/carts/{str(new_customer.id)}', timeout=10)
            print(response)
            if response.status_code != 201:
                raise Exception()
        except Exception:
            print('Error creating cart for customer, deleting new customer')
            CustomersPersistence.get_instance().delete_customer(
                str(new_customer.id))  # rollback
            raise Exception(
                'Customer could not be created due to an internal error in cart creation')

        return new_customer

    def update_customer(self, id: str,
                        name: Union[str, None] = None,
                        last_name: Union[str, None] = None,
                        email: Union[str, None] = None,
                        password: Union[str, None] = None,
                        is_verified: Union[bool, None] = None,
                        is_deleted: Union[bool, None] = None):
        return CustomersPersistence.get_instance()\
            .update_customer(id, name=name, last_name=last_name, email=email, password=password, is_verified=is_verified, is_deleted=is_deleted)
