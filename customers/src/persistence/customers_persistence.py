from typing import Optional, Union
from bson import ObjectId
# imported as mongoengine on purpose for tests to properly patch mongoengine.connect
import mongoengine
import os
import datetime
from func_timeout import FunctionTimedOut, func_timeout

from models.customer import Customer


class CustomersPersistence:

    _instance = None

    def __init__(self):
        database_name = os.environ.get('MONGO_INITDB_DATABASE')
        database_user = os.environ.get('MONGO_INITDB_ROOT_USERNAME')
        database_password = os.environ.get('MONGO_INITDB_ROOT_PASSWORD')
        mongoengine.connect(db=database_name, username=database_user,
                            password=database_password, host='customers-mongodb')

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: Customer.objects().first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def get_customers(self, email: str = '', name: str = '', last_name: str = ''):
        return Customer.objects(email__icontains=email, name__icontains=name, last_name__icontains=last_name, is_deleted=False)

    def get_customer_by_id(self, id: str):
        if not ObjectId.is_valid(id):
            return None

        return Customer.objects(id=ObjectId(id)).first()

    def create_customer(self,
                        name: str,
                        last_name: str,
                        email: str,
                        password: str):
        customer = Customer(
            name=name,
            last_name=last_name,
            email=email,
            password=password,
            created_at=datetime.datetime.utcnow(),
            modified_at=datetime.datetime.utcnow()
        )

        return customer.save()

    def update_customer(self, id: str,
                        name: Union[str, None] = None,
                        last_name: Union[str, None] = None,
                        email: Union[str, None] = None,
                        password: Union[str, None] = None,
                        is_verified: Union[bool, None] = None,
                        is_deleted: Union[bool, None] = None):
        if not ObjectId.is_valid(id):
            return None
        customer = Customer.objects(id=ObjectId(id)).first()

        if customer is None:
            return None

        if name is not None:
            customer.name = name
        if last_name is not None:
            customer.last_name = last_name
        if email is not None:
            customer.email = email
        if password is not None:
            customer.password = password
        if is_verified is not None:
            customer.is_verified = is_verified
        if is_deleted is not None:
            customer.is_deleted = is_deleted

        customer.modified_at = datetime.datetime.utcnow()
        return customer.save()

    def delete_customer(self, id: str):
        if not ObjectId.is_valid(id):
            return None
        customer = Customer.objects(id=ObjectId(id)).first()

        if customer is None:
            return None

        customer.delete()
        return True  # success
