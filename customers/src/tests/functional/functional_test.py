from fastapi.testclient import TestClient
import pytest
from api.customers_api import app
from api.dto.customer_post_dto import CustomerPostDto
from unittest.mock import MagicMock, patch

from persistence.customers_persistence import CustomersPersistence
from services.customers_service import CustomersService

from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_customer, mock_db_customers
from tests.utils.db_utils import setup_db, seed_db, drop_db

client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown():
    CustomersPersistence._instance = None
    CustomersService._instance = None

    yield

    drop_db()


def test_health_check():
    response = client.get("/customers/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_get_customers():
    seed_db()
    response = client.get("/customers")

    expected_response = [customer.to_dict() for customer in mock_db_customers]

    assert response.status_code == 200
    assert_equal_list_of_dicts(expected_response, response.json())


def test_get_customer_by_id():
    seed_db()

    customer1 = mock_db_customers[0]

    response = client.get(f"/customers/{customer1.id}")

    expected_response = customer1.to_dict()

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.post', return_value=MagicMock(status_code=201))
def test_create_customer(mock_requests):
    customer1 = mock_db_customers[0]

    response = client.post("/customers", json=CustomerPostDto(email=customer1.email, name=customer1.name,
                                                              last_name=customer1.last_name,
                                                              password=customer1.password).dict())
    expected_response = customer1.to_dict()
    response_json = response.json()
    print(response_json)
    expected_response["id"] = response_json["id"]
    assert response.status_code == 201
    assert_equal_dicts(response_json, expected_response)


def test_update_customer():
    seed_db()

    customer1 = mock_db_customers[0]
    customer1.name = "new name"

    response = client.patch(f"/customers/{customer1.id}", json=CustomerPostDto(email=customer1.email, name=customer1.name,
                                                                          last_name=customer1.last_name,
                                                                          password=customer1.password).dict())

    expected_response = customer1.to_dict()

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)
