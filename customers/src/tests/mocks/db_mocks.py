from models.customer import Customer
import datetime
from bson.objectid import ObjectId

mock_db_customer = Customer(id=ObjectId("5f4f3f2f1e0d0c0b0a090807"), name="customer1",
                            last_name="last_name1", email="email1", password="password1",
                            created_at=datetime.datetime.utcnow(),
                            modified_at=datetime.datetime.utcnow(),
                            )

mock_db_customers = [
    Customer(id=ObjectId("5f4f3f2f1e0d0c0b0a090807"), name="customer1",
             last_name="last_name1", email="email1", password="password1",
             created_at=datetime.datetime.utcnow(),
             modified_at=datetime.datetime.utcnow(),
             ),
    Customer(id=ObjectId("5f4f3f2f1e0d0c0b0a090801"), name="customer2",
             last_name="last_name2", email="email2", password="password2",
             created_at=datetime.datetime.utcnow(),
             modified_at=datetime.datetime.utcnow(),
             )
]
