import unittest
from unittest.mock import patch, MagicMock
from services.customers_service import CustomersService
from bson import ObjectId


class TestCustomersService(unittest.TestCase):  

    @patch('persistence.customers_persistence.CustomersPersistence.get_instance', return_value=MagicMock())
    def test_get_customers(self, mock_get_instance):
        id1, id2 = str("1")*24, str("2")*24
        mock_get_instance.return_value.get_customers.return_value = [
            MagicMock(
                id=ObjectId(id1)), MagicMock(id=ObjectId(id2))]
        
        customers_service = CustomersService()
        customers = customers_service.get_customers()

        self.assertEqual(len(customers), 2)
        self.assertEqual(str(customers[0].id), id1)
        self.assertEqual(str(customers[1].id), id2)

    @patch('persistence.customers_persistence.CustomersPersistence.get_instance', return_value=MagicMock())
    def test_get_customer_by_id(self, mock_get_instance):
        id = '1'*24
        mock_get_instance.return_value.get_customer_by_id.return_value = MagicMock(id=ObjectId(id))

        customers_service = CustomersService()
        customer = customers_service.get_customer_by_id(id)
        self.assertEqual(str(customer.id), id)
    
    @patch('requests.post', return_value=MagicMock(status_code=201))
    @patch('persistence.customers_persistence.CustomersPersistence.get_instance', return_value=MagicMock())
    def test_create_customer(self, mock_get_instance, mock_requests):
        params = {'name': 'Customer 1', 'email': 'asdasdasd@gmail.com',
                  'last_name': 'Last Name 1', 'password': '12345678'}
        id = '1'*24
        expected = MagicMock(id=ObjectId(id), **params)
        mock_get_instance.return_value.create_customer.return_value = expected
        customers_service = CustomersService()
        new_object = customers_service.create_customer(**params)

        self.assertEqual(new_object.name, expected.name)
        self.assertEqual(new_object.email, expected.email)
        self.assertEqual(new_object.last_name, expected.last_name)
        self.assertEqual(new_object.password, expected.password)
        self.assertEqual(str(new_object.id), id)

    @patch('persistence.customers_persistence.CustomersPersistence.get_instance', return_value=MagicMock())
    def test_update_customer(self, mock_get_instance):
        params = {'name': 'Customer 1', 'email': 'asdasdasd@gmail.com',
                  'last_name': 'Last Name 1', 'password': '12345678'}
        id = '1'*24
        expected = MagicMock(id=ObjectId(id), **params)

        mock_get_instance.return_value.update_customer.return_value = expected
        customer_service = CustomersService()
        new_object = customer_service.update_customer(**params, id=id)

        self.assertEqual(new_object.name, expected.name)
        self.assertEqual(new_object.email, expected.email)
        self.assertEqual(new_object.last_name, expected.last_name)
        self.assertEqual(new_object.password, expected.password)
        self.assertEqual(str(new_object.id), id)
        
