from datetime import datetime
import json
from fastapi.testclient import TestClient
from api.customers_api import app
from api.dto.customer_post_dto import CustomerPostDto
from api.dto.customer_patch_dto import CustomerPatchDto
from unittest.mock import MagicMock, patch
from models.customer import Customer

from api.dto.customer_full_dto import CustomerFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_customer, mock_db_customers


client = TestClient(app)


@patch('services.customers_service.CustomersService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/customers/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('services.customers_service.CustomersService.get_instance')
def test_get_customers(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.get_customers.return_value = mock_db_customers

    response = client.get("/customers")

    expected_response = [customer.to_dict() for customer in mock_db_customers]

    assert response.status_code == 200
    assert_equal_list_of_dicts(response.json(), expected_response)


@patch('services.customers_service.CustomersService.get_instance')
def test_get_customer_by_id(mock_get_instance):

    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_customer_by_id.return_value = mock_db_customer

    response = client.get(f"/customers/{mock_db_customer.id}")

    expected_response = mock_db_customer.to_dict()
    expected_response['last_login'] = 'None'  # default

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@patch('services.customers_service.CustomersService.get_instance')
def test_create_customer(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.create_customer.return_value = mock_db_customer

    response = client.post("/customers", json=CustomerPostDto(email=mock_db_customer.email, name=mock_db_customer.name,
                                                              last_name=mock_db_customer.last_name,
                                                              password=mock_db_customer.password).dict())

    expected_response = mock_db_customer.to_dict()
    expected_response['last_login'] = 'None'  # default

    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


@patch('services.customers_service.CustomersService.get_instance')
def test_update_customer(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.update_customer.return_value = mock_db_customer

    response = client.patch(f"/customers/{mock_db_customer.id}", json=CustomerPostDto(email=mock_db_customer.email, name=mock_db_customer.name,
                                                                                      last_name=mock_db_customer.last_name,
                                                                                      password=mock_db_customer.password).dict())
    expected_response = mock_db_customer.to_dict()

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)
