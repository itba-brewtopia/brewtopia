import unittest
from unittest.mock import MagicMock, patch
from persistence.customers_persistence import CustomersPersistence
from models.customer import Customer
from bson import ObjectId


@patch('mongoengine.connect')
class TestCustomersPersistence(unittest.TestCase):

    @patch.object(Customer, 'objects')
    def test_get_customers(self, mock_objects, _):
        id1, id2 = str("1")*24, str("2")*24
        mock_objects.return_value = [
            MagicMock(
                id=ObjectId(id1)), MagicMock(id=ObjectId(id2))]
        persistence = CustomersPersistence()
        objects = persistence.get_customers()

        self.assertEqual(len(objects), 2, _)
        self.assertEqual(str(objects[0].id), id1)
        self.assertEqual(str(objects[1].id), id2)

    @patch.object(Customer, 'objects')
    def test_get_customer_by_id(self, mock_objects, _):
        id = '5f4f3f2f1e0d0c0b0a090807'
        expected = MagicMock(id=ObjectId(id))
        mock_objects.return_value.first.return_value = expected
        persistence = CustomersPersistence()
        response_id = persistence.get_customer_by_id(id=id).id

        self.assertEqual(str(response_id), id)

    @patch.object(Customer, 'save')
    def test_create_customer(self, mock_save, _):
        params = {'name': 'Customer 1', 'email': 'asdasdasd@gmail.com',
                  'last_name': 'Last Name 1', 'password': '12345678'}
        expected = MagicMock(**params, id=ObjectId('1'*24))
        mock_save.return_value = expected

        persistence = CustomersPersistence()
        new_object = persistence.create_customer(**params)

        self.assertEqual(str(new_object.id), '1'*24)

    @patch.object(Customer, 'objects')
    def test_update_customer(self, mock_objects, _):
        params = {'name': 'Customer 1', 'email': 'asdasdasd@gmail.com',
                  'last_name': 'Last Name 1', 'password': '12345678'}

        expected = MagicMock(id=ObjectId('1'*24), **params)

        # Create the mock that will be returned by customers.save()
        mock_customer = MagicMock()
        mock_customer.save.return_value = expected

        # Setup the mock object chain
        mock_objects.return_value.first.return_value = mock_customer

        persistence = CustomersPersistence()
        new_object = persistence.update_customer(**params, id='1'*24)
        print(expected)
        print(new_object)

        self.assertEqual(new_object, expected)
        self.assertEqual(new_object.email, expected.email)
        self.assertEqual(new_object.last_name, expected.last_name)
        self.assertEqual(new_object.password, expected.password)
        self.assertEqual(str(new_object.id), '1'*24)

    @patch.object(Customer, 'objects')
    def test_delete_customer(self, mock_objects, _):
        mock_customer = MagicMock()
        mock_customer.delete.return_value = True

        mock_objects.return_value.first.return_value = mock_customer

        persistence = CustomersPersistence()
        response = persistence.delete_customer(id='1'*24)

        self.assertTrue(response)
