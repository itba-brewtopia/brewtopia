import os
from tests.mocks.db_mocks import mock_db_customers
from mongoengine import connect, disconnect
from models.customer import Customer
import copy

database_name = os.environ.get('MONGO_INITDB_DATABASE')
database_user = os.environ.get('MONGO_INITDB_ROOT_USERNAME')
database_password = os.environ.get('MONGO_INITDB_ROOT_PASSWORD')


def setup_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='customers-mongodb')
    Customer.objects().delete()
    disconnect()


def seed_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='customers-mongodb')
    customers = [copy.deepcopy(store) for store in mock_db_customers]

    Customer.objects().delete()

    for customer in customers:
        customer.save()
    disconnect()


def drop_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='customers-mongodb')
    Customer.objects().delete()
    disconnect()
