import math


def assert_equal_dicts(d1, d2):
    assert isinstance(d1, dict) and isinstance(
        d2, dict), "Both arguments should be dict"

    d1_keys = list(sorted(d1.keys()))
    d2_keys = list(sorted(d2.keys()))

    assert d1_keys == d2_keys, "Dictionaries don't have the same keys"

    for key in d1_keys:
        if key == "created_at" or key == "modified_at" or key == "last_login":
            continue
        
        if isinstance(d1[key], dict):
            assert_equal_dicts(d1[key], d2[key])  # recursion on nested dict
        elif d1[key] is None and d2[key] is None:
            continue  # if both are None, just continue to the next key
        else:
            assert d1[key] == d2[key], f"Values for key {key} are not equal: {d1[key]} vs {d2[key]}"



def assert_equal_list_of_dicts(list1, list2):
    assert isinstance(list1, list) and isinstance(
        list2, list), "Both arguments should be list"
    assert len(list1) == len(list2), "Lists are not of the same length"
    list1 = list(sorted(list1, key=lambda x: str(x["id"])))
    list2 = list(sorted(list2, key=lambda x: str(x["id"])))
    print("List 1: ", list1)
    print("List 2:", list2)
    for d1, d2 in zip(list1, list2):
        assert_equal_dicts(d1, d2)
