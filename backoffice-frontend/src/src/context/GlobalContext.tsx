import React, { useCallback } from "react";
import StoresService from "../api/services/StoresService";
import ProductsService from "../api/services/ProductsService";
import CustomersService from "../api/services/CustomersService";
import OrdersService from "../api/services/OrdersService";
import StocksService from "../api/services/StocksService";
import ShipmentsService from "../api/services/ShipmentsService";

const GlobalContext = React.createContext<GlobalContextType | null>(null);

type Props = {
	children: React.ReactNode;
};

export const GlobalContextProvider: React.FC<Props> = ({ children }) => {
	const [products, setProducts] = React.useState<Product[] | null>(null);
	const [categories, setCategories] = React.useState<Category[] | null>(null);
	const [stores, setStores] = React.useState<Store[] | null>(null);
	const [customers, setCustomers] = React.useState<Customer[] | null>(null);
	const [orders, setOrders] = React.useState<Order[] | null>(null);
	const [productStocks, setProductStocks] = React.useState<
		ProductStock[] | null
	>(null);
	const [shipments, setShipments] = React.useState<Shipment[] | null>(null);
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const [error, setError] = React.useState<string | null>(null);

	const fetchProducts = async () => {
		const productsRes = await ProductsService.getAllProducts();
		setProducts(productsRes.data);
	};

	const fetchCategories = async () => {
		const categoriesRes = await ProductsService.getAllCategories();
		setCategories(categoriesRes.data);
	};

	const fetchStores = async () => {
		const storesRes = await StoresService.getStores();
		setStores(storesRes.data);
	};

	const fetchCustomers = async () => {
		const customersRes = await CustomersService.getAllCustomers();
		setCustomers(customersRes.data);
	};

	const fetchOrders = async () => {
		const ordersRes = await OrdersService.getAllOrders();
		setOrders(ordersRes.data);
	};

	const fetchProductStocks = async () => {
		const productStocksRes = await StocksService.getAllStocks();
		setProductStocks(productStocksRes.data);
	};

	const fetchShipments = async () => {
		const shipmentsRes = await ShipmentsService.getAllShipments();
		setShipments(shipmentsRes.data);
	};

	const fetchAll = useCallback(async () => {
		await fetchProducts();
		await fetchCategories();
		await fetchStores();
		await fetchCustomers();
		await fetchOrders();
		await fetchProductStocks();
		await fetchShipments();
	}, []);

	React.useEffect(() => {
		if (
			isLoading ||
			error ||
			(products &&
				categories &&
				stores &&
				customers &&
				orders &&
				productStocks &&
				shipments)
		)
			return;

		setIsLoading(true);
		fetchAll()
			.catch((err) => {
				console.error(err);
				setError(err.message);
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, [
		fetchAll,
		isLoading,
		error,
		products,
		categories,
		stores,
		customers,
		orders,
		productStocks,
		shipments,
	]);

	return (
		<GlobalContext.Provider
			value={{
				products,
				categories,
				stores,
				customers,
				orders,
				productStocks,
				shipments,
				isLoading,
				setProducts,
				setCategories,
				setStores,
				setCustomers,
				setOrders,
				setProductStocks,
				setShipments,
				error,
				fetchProductStocks,
				fetchShipments,
				fetchStores,
				fetchOrders,
				fetchCustomers,
				fetchProducts,
			}}
		>
			{children}
		</GlobalContext.Provider>
	);
};

export default GlobalContext;
