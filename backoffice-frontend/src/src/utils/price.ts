export const printPrice = (price: string): string =>
	new Intl.NumberFormat("es-AR", {
		style: "currency",
		currency: "ARS",
	}).format(parseFloat(price));
