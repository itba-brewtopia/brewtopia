export const printAddress = (address: Address) => {
	return `${address.street} ${address.number}, ${address.postalCode} ${address.city} (${address.province})`;
};
