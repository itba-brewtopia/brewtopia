const modalStyle = {
	position: "absolute" as "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	boxShadow: 24,
	p: 4,
	overflow: "scroll",
	maxHeight: "100vh",
};

export default modalStyle;
