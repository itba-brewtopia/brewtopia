enum OrderStatus {
	PENDING = "PENDING",
	IN_TRANSIT = "IN_TRANSIT",
	CANCELLED = "CANCELLED",
	READY_FOR_PICKUP = "READY",
	PICKED_UP = "PICKED_UP",
}

export default OrderStatus;
