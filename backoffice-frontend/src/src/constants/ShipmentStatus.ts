enum ShipmentStatus {
	PENDING = "PENDING",
	SHIPPED = "SHIPPED",
	DELIVERED = "DELIVERED",
	CANCELLED = "CANCELLED",
}

export default ShipmentStatus;
