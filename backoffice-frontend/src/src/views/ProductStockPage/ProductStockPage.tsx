import { Edit } from "@mui/icons-material";
import {
	Container,
	Typography,
	TableContainer,
	Paper,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	IconButton,
	Modal,
	Breadcrumbs,
} from "@mui/material";
import React, { useContext, useState } from "react";
import { Link, useParams } from "react-router-dom";
import EditProductStockModal, {
	EditProductStockForm,
} from "../../modals/EditProductStockModal/EditProductStockModal";
import GlobalContext from "../../context/GlobalContext";
import StocksService from "../../api/services/StocksService";

const ProductStockPage = () => {
	const { stores, productStocks, products, setProductStocks } = useContext(
		GlobalContext
	) as GlobalContextType;

	const { id } = useParams<{
		id: string;
	}>();

	const [openEditModal, setOpenEditModal] = useState(false);

	const [selectedStock, setSelectedStock] = useState<ProductStock | null>(null);

	const handleEdit = async (
		values: EditProductStockForm,
		productId: string,
		storeId: string
	) => {
		if (!productId || !storeId || !productStocks) return;
		console.log(values);
		console.log(productId);

		const res = await StocksService.editStock({
			productId,
			storeId,
			quantity: values.quantity,
			minimumQuantity: values.minimumQuantity,
		});

		// if it was just created, add it to the list
		if (
			!productStocks.find(
				(stock) =>
					stock.productId === res.data.productId &&
					stock.storeId === res.data.storeId
			)
		) {
			setProductStocks((prevStocks) =>
				prevStocks ? [...prevStocks, res.data] : null
			);
		} else {
			setProductStocks(
				(prevStocks) =>
					prevStocks?.map((stock) => {
						if (stock.productId === productId && stock.storeId === storeId) {
							return res.data;
						}
						return stock;
					}) || null
			);
		}

		setOpenEditModal(false);
	};

	// if there's no stock for a store, create a placeholder
	const productStockInStores = stores?.map(
		(store) =>
			productStocks?.find(
				(stock) => stock.productId === id && stock.storeId === store.id
			) ||
			({
				productId: id,
				storeId: store.id,
				quantity: 0,
				minimumQuantity: 0,
			} as ProductStock)
	);

	const product = products?.find((product) => product.id === id);

	return (
		<>
			<Container>
				<Breadcrumbs aria-label="breadcrumb" sx={{ marginBottom: "1rem" }}>
					<Link to="/products">Products</Link>
					<Typography color="text.primary">Product Stock</Typography>
				</Breadcrumbs>

				<Typography variant="h3" component="h1" className="py-4">
					Product Stock
				</Typography>

				<Typography variant="h5" component="h2" className="py-4">
					{product?.name} - {id}
				</Typography>

				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell align="left">Store</TableCell>
								<TableCell align="left">Quantity</TableCell>
								<TableCell align="left">Min. Quantity</TableCell>
								<TableCell align="right"></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{productStockInStores?.map((stock) => (
								<TableRow
									key={stock.storeId}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row" align="left">
										{stock.storeId} -{" "}
										{stores?.find((store) => store.id === stock.storeId)?.alias}
									</TableCell>
									<TableCell align="left">{stock.quantity}</TableCell>
									<TableCell align="left">{stock.minimumQuantity}</TableCell>

									<TableCell align="left">
										<IconButton
											aria-label="edit"
											size="large"
											onClick={() => {
												setSelectedStock(stock);
												setOpenEditModal(true);
											}}
										>
											<Edit />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>

			<Modal open={openEditModal} onClose={() => setOpenEditModal(false)}>
				<>
					{selectedStock && (
						<EditProductStockModal
							onEdit={handleEdit}
							productStock={selectedStock}
						/>
					)}
				</>
			</Modal>
		</>
	);
};

export default ProductStockPage;
