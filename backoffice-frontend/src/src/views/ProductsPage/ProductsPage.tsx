import { useContext, useState } from "react";
import CreateProductModal, {
	CreateProductForm,
} from "../../modals/CreateProductModal/CreateProductModal";
import {
	Autocomplete,
	Button,
	Container,
	IconButton,
	Modal,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	TextField,
	Typography,
} from "@mui/material";
import { Add, Delete, Edit, Inventory } from "@mui/icons-material";
import { titleCase } from "title-case";
import { useNavigate } from "react-router-dom";
import GlobalContext from "../../context/GlobalContext";
import ProductsService from "../../api/services/ProductsService";
import StocksService from "../../api/services/StocksService";
import { FormikProvider, useFormik } from "formik";
import { printPrice } from "../../utils/price";

type FilterProductsForm = {
	categoryId: string | null;
	query: string;
	minPrice: number | string;
	maxPrice: number | string;
};

const ProductPage = () => {
	const filterFormik = useFormik<FilterProductsForm>({
		initialValues: {
			categoryId: null,
			query: "",
			minPrice: "",
			maxPrice: "",
		},
		onSubmit: async (values) => {
			await handleFilter(values);
		},
	});
	const {
		products,
		categories,
		setProducts,
		fetchShipments,
		fetchProductStocks,
		fetchProducts,
	} = useContext(GlobalContext) as GlobalContextType;

	const navigate = useNavigate();
	const [filteredProducts, setFilteredProducts] = useState<Product[] | null>(
		null
	);
	const [openCreateModal, setOpenCreateModal] = useState(false);
	const [openEditModal, setOpenEditModal] = useState(false);

	const [isRestocking, setIsRestocking] = useState(false);

	const [selectedProduct, setSelectedProduct] = useState<Product | null>(null);

	const handleCreate = async (values: CreateProductForm) => {
		console.log(values);

		const res = await ProductsService.createProduct({
			name: values.name,
			description: values.description,
			images: values.images,
			categoryId: values.categoryId,
			price: values.price.toString(),
		});

		setProducts((prevProducts) =>
			prevProducts ? [...prevProducts, res.data] : null
		);

		await fetchProductStocks();
		setOpenCreateModal(false);
	};

	const handleEdit = async (values: CreateProductForm, productId: string) => {
		console.log(values);
		console.log(productId);

		const res = await ProductsService.editProduct({
			id: productId,
			name: values.name,
			description: values.description,
			images: values.images,
			categoryId: values.categoryId,
			price: values.price.toString(),
		});

		setProducts(
			(prevProducts) =>
				prevProducts?.map((product) => {
					if (product.id === productId) {
						return res.data;
					}
					return product;
				}) || null
		);

		setOpenEditModal(false);
	};

	const handleRestock = async () => {
		try {
			setIsRestocking(true);
			await StocksService.restock();
			await fetchShipments();
		} finally {
			setIsRestocking(false);
		}
	};

	const handleViewStock = (productId: string) => {
		navigate(`/products/${productId}/stock`);
	};

	const handleFilter = async (values: FilterProductsForm) => {
		console.log(values);

		if (
			!values.categoryId &&
			!values.query &&
			!values.minPrice &&
			!values.maxPrice
		) {
			await fetchProducts();
			setFilteredProducts(null);
			return;
		}

		const res = await ProductsService.getAllProducts({
			category: values.categoryId,
			query: values.query,
			minPrice: values.minPrice ? values.minPrice.toString() : undefined,
			maxPrice: values.maxPrice ? values.maxPrice.toString() : undefined,
		});

		setFilteredProducts(res.data);
	};

	const handleDelete = async (productId: string) => {
		await ProductsService.deleteProduct(productId);

		setProducts(
			(prevProducts) =>
				prevProducts?.filter((product) => product.id !== productId) || null
		);
	};

	return (
		<>
			<Container>
				<Typography variant="h3" component="h1" className="py-4">
					Products
				</Typography>

				<div className="flex justify-between items-center w-100">
					<Button
						variant="contained"
						color="primary"
						startIcon={<Add />}
						onClick={() => setOpenCreateModal(true)}
					>
						Create New Product
					</Button>

					<Button
						variant="contained"
						color="primary"
						startIcon={<Add />}
						disabled={isRestocking}
						onClick={handleRestock}
					>
						Create Restock Shipments
					</Button>
				</div>

				<FormikProvider value={filterFormik}>
					<div className="flex items-center w-100 justify-center my-5 px-2">
						<Autocomplete<Category>
							id="categoryId"
							options={categories ?? []}
							getOptionLabel={(option) => option.name}
							onChange={(e, value) => {
								filterFormik.setFieldValue("categoryId", value?.id || null);
							}}
							value={
								categories?.find(
									(category) => category.id === filterFormik.values.categoryId
								) || null
							}
							renderInput={(params) => (
								<TextField
									{...params}
									sx={{
										width: 300,
									}}
									label="Category"
								/>
							)}
						/>

						<TextField
							name="minPrice"
							type="number"
							label="Min Price"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.minPrice}
							onChange={filterFormik.handleChange}
						/>

						<TextField
							name="maxPrice"
							type="number"
							label="Max Price"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.maxPrice}
							onChange={filterFormik.handleChange}
						/>

						<TextField
							name="query"
							label="Product Name"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.query}
							onChange={filterFormik.handleChange}
						/>

						<Button
							variant="contained"
							color="primary"
							onClick={filterFormik.submitForm}
						>
							Search
						</Button>
					</div>
				</FormikProvider>

				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell align="left">ID</TableCell>
								<TableCell align="left">Name</TableCell>
								<TableCell align="left">Description</TableCell>
								<TableCell align="left">Images</TableCell>
								<TableCell align="right">Category</TableCell>
								<TableCell align="right">Price</TableCell>
								<TableCell align="right">Actions</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{(filteredProducts || products)?.map((product) => (
								<TableRow
									key={product.id}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row" align="left">
										{product.id}
									</TableCell>
									<TableCell align="left">{product.name}</TableCell>
									<TableCell align="left">{product.description}</TableCell>
									<TableCell align="left">
										<ul>
											{product.images.map((image, i) => (
												<li key={i}>{image}</li>
											))}
										</ul>
									</TableCell>
									<TableCell align="left">
										{titleCase(product.category.name)}
									</TableCell>
									<TableCell align="left">
										{printPrice(product.price)}
									</TableCell>

									<TableCell align="left">
										<IconButton
											aria-label="edit"
											size="large"
											onClick={() => {
												setSelectedProduct(product);
												setOpenEditModal(true);
											}}
										>
											<Edit />
										</IconButton>

										<IconButton
											aria-label="view stock"
											size="large"
											onClick={() => handleViewStock(product.id)}
										>
											<Inventory />
										</IconButton>

										<IconButton onClick={() => handleDelete(product.id)}>
											<Delete />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
			<Modal open={openCreateModal} onClose={() => setOpenCreateModal(false)}>
				<CreateProductModal onCreate={handleCreate} />
			</Modal>
			<Modal open={openEditModal} onClose={() => setOpenEditModal(false)}>
				<CreateProductModal onEdit={handleEdit} product={selectedProduct} />
			</Modal>
		</>
	);
};

export default ProductPage;
