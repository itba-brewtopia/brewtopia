import { Add, Delete, Edit } from "@mui/icons-material";
import {
	Container,
	Typography,
	Button,
	TableContainer,
	Paper,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
	Modal,
	IconButton,
} from "@mui/material";
import React, { useContext, useState } from "react";
import CreateCategoryModal, {
	CreateCategoryForm,
} from "../../modals/CreateCategoryModal/CreateCategoryModal";
import GlobalContext from "../../context/GlobalContext";
import ProductsService from "../../api/services/ProductsService";

type Props = {};

const CategoriesPage = (props: Props) => {
	const { categories, setCategories } = useContext(
		GlobalContext
	) as GlobalContextType;
	const [openCreateModal, setOpenCreateModal] = useState(false);
	const [openEditModal, setOpenEditModal] = useState(false);

	const [selectedCategory, setSelectedCategory] = useState<Category | null>(
		null
	);

	const handleCreate = async (values: CreateCategoryForm) => {
		console.log(values);
		const res = await ProductsService.createProductCategory({
			name: values.name,
			image: values.image,
		});

		setCategories((prevCategories) =>
			prevCategories ? [...prevCategories, res.data] : null
		);
		setOpenCreateModal(false);
	};

	const handleDelete = async (categoryId: string) => {
		await ProductsService.deleteProductCategory(categoryId);

		setCategories(
			(prevCategories) =>
				prevCategories?.filter((category) => category.id !== categoryId) || null
		);
	};

	const handleEdit = async (values: CreateCategoryForm, categoryId: string) => {
		console.log(values);
		console.log(categoryId);

		const res = await ProductsService.editProductCategory({
			id: categoryId,
			name: values.name,
			image: values.image,
		});

		setCategories(
			(prevCategories) =>
				prevCategories?.map((category) => {
					if (category.id === categoryId) {
						return res.data;
					}
					return category;
				}) || null
		);

		setOpenEditModal(false);
	};

	return (
		<>
			<Container>
				<Typography variant="h3" component="h1" className="py-4">
					Categories
				</Typography>

				<Button
					variant="contained"
					color="primary"
					startIcon={<Add />}
					onClick={() => setOpenCreateModal(true)}
				>
					Create Category
				</Button>

				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell align="left">ID</TableCell>
								<TableCell align="left">Name</TableCell>
								<TableCell align="left">Image</TableCell>
								<TableCell align="right"></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{categories?.map((category) => (
								<TableRow
									key={category.id}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row" align="left">
										{category.id}
									</TableCell>
									<TableCell align="left">{category.name}</TableCell>
									<TableCell align="left">{category.image}</TableCell>
									<TableCell align="right">
										<IconButton
											onClick={() => {
												setSelectedCategory(category);
												setOpenEditModal(true);
											}}
										>
											<Edit />
										</IconButton>

										<IconButton onClick={() => handleDelete(category.id)}>
											<Delete />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
			<Modal open={openCreateModal} onClose={() => setOpenCreateModal(false)}>
				<CreateCategoryModal onCreate={handleCreate} />
			</Modal>
			<Modal open={openEditModal} onClose={() => setOpenEditModal(false)}>
				<CreateCategoryModal onEdit={handleEdit} category={selectedCategory} />
			</Modal>
		</>
	);
};

export default CategoriesPage;
