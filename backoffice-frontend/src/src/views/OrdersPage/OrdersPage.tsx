import {
	Container,
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
	Modal,
	TextField,
	Autocomplete,
	MenuItem,
	Button,
} from "@mui/material";
import { Edit, List } from "@mui/icons-material";
import { useContext, useState } from "react";
import OrderItemsModal from "../../modals/OrderItemsModal/OrderItemsModal";
import EditOrderModal, {
	EditOrderForm,
} from "../../modals/EditOrderModal/EditOrderModal";
import GlobalContext from "../../context/GlobalContext";
import OrdersService from "../../api/services/OrdersService";
import { FormikProvider, useFormik } from "formik";
import OrderStatus from "../../constants/OrderStatus";
import { DateField } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { titleCase } from "title-case";
import { printPrice } from "../../utils/price";

type FilterOrdersForm = {
	customerId: string | null;
	pickupReadyDate: string | null;
	storeId: string | null;
	status: OrderStatus | null;
};

const OrdersPage = () => {
	const filterFormik = useFormik<FilterOrdersForm>({
		initialValues: {
			customerId: "",
			pickupReadyDate: "",
			storeId: null,
			status: null,
		},
		onSubmit: async (values) => {
			await handleFilter(values);
		},
	});
	const { orders, setOrders, fetchOrders, stores, customers } = useContext(
		GlobalContext
	) as GlobalContextType;
	const [filteredOrders, setFilteredOrders] = useState<Order[] | null>(null);
	const [selectedOrderForItems, setSelectedOrderForItems] =
		useState<Order | null>(null);
	const [selectedOrderForEditing, setSelectedOrderForEditing] =
		useState<Order | null>(null);

	const handleEdit = async (values: EditOrderForm, orderId: string) => {
		const res = await OrdersService.editOrder({
			id: orderId,
			status: values.status,
		});

		setOrders(
			(prevOrders) =>
				prevOrders?.map((order) => {
					if (order.id === orderId) {
						return res.data;
					}
					return order;
				}) || null
		);

		setSelectedOrderForEditing(null);
	};

	const handleFilter = async (values: FilterOrdersForm) => {
		if (
			!values.customerId &&
			!values.pickupReadyDate &&
			!values.storeId &&
			!values.status
		) {
			await fetchOrders();
			setFilteredOrders(null);
			return;
		}

		const res = await OrdersService.getAllOrders(values);
		setFilteredOrders(res.data);
	};

	return (
		<>
			<Container>
				<Typography variant="h3" component="h1" className="py-4">
					Orders
				</Typography>

				<FormikProvider value={filterFormik}>
					<div className="flex items-center w-100 justify-center my-5 px-2">
						<DateField
							fullWidth
							id="pickupReadyDate"
							label="Ready for Pickup Date"
							value={dayjs(filterFormik.values.pickupReadyDate)}
							onChange={(value) => {
								filterFormik.setFieldValue(
									"pickupReadyDate",
									value?.isValid() ? value.format("YYYY-MM-DD") : null
								);
							}}
							helperText={
								filterFormik.touched.pickupReadyDate &&
								filterFormik.errors.pickupReadyDate
							}
						/>

						<Autocomplete<Store>
							id="storeId"
							options={stores ?? []}
							getOptionLabel={(option) => option.alias}
							onChange={(e, value) => {
								filterFormik.setFieldValue("storeId", value?.id || "");
							}}
							value={
								stores?.find(
									(store) => store.id === filterFormik.values.storeId
								) || null
							}
							renderInput={(params) => (
								<TextField
									{...params}
									sx={{
										width: 300,
									}}
									label="Pickup Store"
									error={
										filterFormik.touched.storeId &&
										Boolean(filterFormik.errors.storeId)
									}
									helperText={
										filterFormik.touched.storeId && filterFormik.errors.storeId
									}
								/>
							)}
						/>

						<Autocomplete<Customer>
							id="Customer"
							options={customers ?? []}
							getOptionLabel={(
								option
							) => `${option.name} ${option.lastName} - ${option.email}
								`}
							onChange={(e, value) => {
								filterFormik.setFieldValue("customerId", value?.id || "");
							}}
							value={
								customers?.find(
									(customer) => customer.id === filterFormik.values.customerId
								) || null
							}
							renderInput={(params) => (
								<TextField
									{...params}
									label="Customer"
									error={
										filterFormik.touched.customerId &&
										Boolean(filterFormik.errors.customerId)
									}
									sx={{
										width: 300,
									}}
									helperText={
										filterFormik.touched.customerId &&
										filterFormik.errors.customerId
									}
								/>
							)}
						/>

						<TextField
							fullWidth
							id="status"
							name="status"
							label="Status"
							value={filterFormik.values.status}
							select
							onChange={(e) => {
								filterFormik.setFieldValue(
									"status",
									e.target.value === "All" ? null : e.target.value
								);
							}}
							error={
								filterFormik.touched.status &&
								Boolean(filterFormik.errors.status)
							}
							onBlur={filterFormik.handleBlur}
							helperText={
								filterFormik.touched.status && filterFormik.errors.status
							}
						>
							{[...Object.values(OrderStatus), "All"].map((status) => (
								<MenuItem key={status} value={status}>
									{titleCase(status)}
								</MenuItem>
							))}
						</TextField>

						<Button
							variant="contained"
							color="primary"
							onClick={filterFormik.submitForm}
							className="mx-2"
						>
							Search
						</Button>
					</div>
				</FormikProvider>

				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell align="left">Order ID</TableCell>
								<TableCell align="left">Customer ID</TableCell>
								<TableCell align="left">Created At</TableCell>
								<TableCell align="left">Total Price</TableCell>
								<TableCell align="left">Pickup Store ID</TableCell>
								<TableCell align="left">Status</TableCell>
								<TableCell align="left">Delivered At</TableCell>
								<TableCell align="left">Order Ready At</TableCell>
								<TableCell align="left">Items</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{(filteredOrders || orders)?.map((order) => (
								<TableRow
									key={order.id}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row" align="left">
										{order.id}
									</TableCell>
									<TableCell align="left">{order.customerId}</TableCell>
									<TableCell align="left">
										{new Date(order.createdAt).toLocaleString()}
									</TableCell>
									<TableCell align="left">
										{printPrice(order.totalPrice)}
									</TableCell>
									<TableCell align="left">{order.pickupStoreId}</TableCell>
									<TableCell align="left">
										{order.status}
										<IconButton
											aria-label="edit"
											size="large"
											onClick={() => {
												setSelectedOrderForEditing(order);
											}}
										>
											<Edit />
										</IconButton>
									</TableCell>
									<TableCell align="left">
										{order.pickupDate
											? new Date(order.pickupDate).toLocaleString()
											: "N/A"}
									</TableCell>
									<TableCell align="left">
										{new Date(order.pickupReadyDate).toLocaleString()}
									</TableCell>
									<TableCell align="left">
										<IconButton
											aria-label="edit"
											size="large"
											onClick={() => {
												setSelectedOrderForItems(order);
											}}
										>
											<List />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
			<Modal
				open={!!selectedOrderForItems}
				onClose={() => setSelectedOrderForItems(null)}
			>
				<>
					{selectedOrderForItems && (
						<OrderItemsModal order={selectedOrderForItems} />
					)}
				</>
			</Modal>
			<Modal
				open={!!selectedOrderForEditing}
				onClose={() => setSelectedOrderForEditing(null)}
			>
				<>
					{selectedOrderForEditing && (
						<EditOrderModal
							order={selectedOrderForEditing}
							onSubmit={handleEdit}
						/>
					)}
				</>
			</Modal>
		</>
	);
};

export default OrdersPage;
