import {
	Button,
	Container,
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	TextField,
	Typography,
} from "@mui/material";
import { useContext, useState } from "react";
import GlobalContext from "../../context/GlobalContext";
import { FormikProvider, useFormik } from "formik";
import CustomersService from "../../api/services/CustomersService";
import { Delete } from "@mui/icons-material";

const CustomersPage = () => {
	const filterFormik = useFormik({
		initialValues: {
			email: "",
			name: "",
			lastName: "",
		},
		onSubmit: async (values) => {
			await handleFilter(values);
		},
	});
	const { customers, fetchCustomers, setCustomers } = useContext(
		GlobalContext
	) as GlobalContextType;
	const [filteredCustomers, setFilteredCustomers] = useState<Customer[] | null>(
		null
	);

	const handleFilter = async (values: any) => {
		console.log(values);

		if (!values.email && !values.name && !values.lastName) {
			await fetchCustomers();
			setFilteredCustomers(null);
			return;
		}

		const res = await CustomersService.getAllCustomers(values);
		setFilteredCustomers(res.data);
	};

	const handleDelete = async (customerId: string) => {
		await CustomersService.deleteCustomer(customerId);

		setCustomers(
			(prevCustomers) =>
				prevCustomers?.filter((customer) => customer.id !== customerId) || null
		);
	};

	return (
		<>
			<Container>
				<Typography variant="h3" component="h1" className="py-4">
					Customers
				</Typography>

				<FormikProvider value={filterFormik}>
					<div className="flex items-center w-100 justify-center">
						<TextField
							name="email"
							label="Filter by email"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.email}
							onChange={filterFormik.handleChange}
						/>

						<TextField
							name="name"
							label="Filter by first name"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.name}
							onChange={filterFormik.handleChange}
						/>

						<TextField
							name="lastName"
							label="Filter by last name"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.lastName}
							onChange={filterFormik.handleChange}
						/>

						<Button
							variant="contained"
							color="primary"
							onClick={filterFormik.submitForm}
							className="mx-2"
						>
							Search
						</Button>
					</div>
				</FormikProvider>

				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell align="left">ID</TableCell>
								<TableCell align="left">First Name</TableCell>
								<TableCell align="left">Last Name</TableCell>
								<TableCell align="left">Email</TableCell>
								<TableCell align="left">Created At</TableCell>
								<TableCell align="left">Modified At</TableCell>
								<TableCell align="right"></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{(filteredCustomers || customers)?.map((customer) => (
								<TableRow
									key={customer.id}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row" align="left">
										{customer.id}
									</TableCell>
									<TableCell align="left">{customer.name}</TableCell>
									<TableCell align="left">{customer.lastName}</TableCell>
									<TableCell align="left">{customer.email}</TableCell>
									<TableCell align="left">
										{new Date(customer.createdAt).toLocaleString()}
									</TableCell>
									<TableCell align="left">
										{new Date(customer.modifiedAt).toLocaleString()}
									</TableCell>
									<TableCell align="right">
										<IconButton onClick={() => handleDelete(customer.id)}>
											<Delete />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
		</>
	);
};

export default CustomersPage;
