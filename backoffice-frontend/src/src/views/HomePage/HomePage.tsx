import { Card, CardActionArea, CardContent, Typography } from "@mui/material";
import routes from "../../router/routes";
import { Link } from "react-router-dom";

const generalLinks = Object.values(routes).filter(
	(route) =>
		![routes.Home.path, routes.ProductStock.path].find(
			(path) => path === route.path
		)
);

const HomePage = () => {
	return (
		<div className="flex flex-col items-center justify-center w-full h-full">
			<Typography variant="h3" component="h1" className="py-4">
				Dashboard
			</Typography>

			{generalLinks.map((route) => (
				<Card sx={{ width: 345, textAlign: "center" }} className="mb-2">
					<CardActionArea component={Link} to={route.path}>
						<CardContent>
							<Typography gutterBottom variant="h5" component="div">
								{route.name}
							</Typography>
						</CardContent>
					</CardActionArea>
				</Card>
			))}
		</div>
	);
};

export default HomePage;
