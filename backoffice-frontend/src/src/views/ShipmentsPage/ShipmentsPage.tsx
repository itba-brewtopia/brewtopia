import {
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
	Button,
	Modal,
	TextField,
	Autocomplete,
	MenuItem,
} from "@mui/material";
import { titleCase } from "title-case";
import { Add, Edit, List } from "@mui/icons-material";
import { useContext, useState } from "react";
import CreateShipmentModal, {
	CreateShipmentForm,
} from "../../modals/CreateShipmentModal/CreateShipmentModal";
import GlobalContext from "../../context/GlobalContext";
import ShipmentsService from "../../api/services/ShipmentsService";
import modalStyle from "../../styles/modalStyles";
import { FormikProvider, useFormik } from "formik";
import { DateField } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import ShipmentStatus from "../../constants/ShipmentStatus";

const ShipmentsPage = () => {
	const filterFormik = useFormik({
		initialValues: {
			status: "",
			estimatedDeliveryDate: "",
			estimatedShipDate: "",
			sourceStoreId: "",
			destinationStoreId: "",
			shipDate: "",
			deliveryDate: "",
		},
		onSubmit: async (values) => {
			await handleFilter(values);
		},
	});
	const { shipments, setShipments, fetchShipments, stores } = useContext(
		GlobalContext
	) as GlobalContextType;

	const [selectedShipmentForItems, setSelectedShipmentForItems] =
		useState<Shipment | null>(null);
	const [selectedShipmentForEditing, setSelectedShipmentForEditing] =
		useState<Shipment | null>(null);
	const [openCreateModal, setOpenCreateModal] = useState(false);
	const [filteredShipments, setFilteredShipments] = useState<Shipment[] | null>(
		null
	);

	const handleEdit = async (values: CreateShipmentForm, shipmentId: string) => {
		const res = await ShipmentsService.updateShipment(
			shipmentId,
			values.status
		);
		setShipments(
			(prevShipments) =>
				prevShipments?.map((shipment) => {
					if (shipment.id === shipmentId) {
						return res.data;
					}
					return shipment;
				}) || null
		);

		console.log(values);
		setSelectedShipmentForEditing(null);
	};

	const handleCreate = async (values: CreateShipmentForm) => {
		console.log(values);

		const res = await ShipmentsService.createShipment({
			sourceStoreId: values.sourceStoreId,
			destinationStoreId: values.destinationStoreId,
			items: values.items,
			estimatedDeliveryDate: values.estimatedDeliveryDate,
			estimatedShipDate: values.estimatedShipDate,
		});

		setShipments((prevShipments) =>
			prevShipments ? [...prevShipments, res.data] : null
		);

		setOpenCreateModal(false);
	};

	const handleFilter = async (values: any) => {
		console.log(values);

		if (
			!values.status &&
			!values.estimatedDeliveryDate &&
			!values.estimatedShipDate &&
			!values.sourceStoreId &&
			!values.destinationStoreId &&
			!values.shipDate &&
			!values.deliveryDate
		) {
			await fetchShipments();
			setFilteredShipments(null);
			return;
		}

		const res = await ShipmentsService.getAllShipments(values);

		setFilteredShipments(res.data);
	};

	return (
		<>
			<Typography variant="h3" component="h1" className="py-4">
				Shipments
			</Typography>

			<Button
				variant="contained"
				color="primary"
				startIcon={<Add />}
				onClick={() => setOpenCreateModal(true)}
			>
				Create New Shipment
			</Button>

			<FormikProvider value={filterFormik}>
				<div className="flex items-center w-100 justify-center my-5 px-2">
					<DateField
						fullWidth
						id="estimatedShipDate"
						label="Estimated Ship Date"
						value={dayjs(filterFormik.values.estimatedShipDate)}
						onChange={(value) => {
							filterFormik.setFieldValue(
								"estimatedShipDate",
								value?.isValid() ? value.format("YYYY-MM-DD") : null
							);
						}}
						helperText={
							filterFormik.touched.estimatedShipDate &&
							filterFormik.errors.estimatedShipDate
						}
					/>

					<DateField
						fullWidth
						id="estimatedDeliveryDate"
						label="Estimated Delivery Date"
						value={dayjs(filterFormik.values.estimatedDeliveryDate)}
						onChange={(value) => {
							filterFormik.setFieldValue(
								"estimatedDeliveryDate",
								value?.isValid() ? value.format("YYYY-MM-DD") : null
							);
						}}
						helperText={
							filterFormik.touched.estimatedDeliveryDate &&
							filterFormik.errors.estimatedDeliveryDate
						}
					/>

					<DateField
						fullWidth
						id="shipDate"
						label="Shipped Date"
						value={dayjs(filterFormik.values.shipDate)}
						onChange={(value) => {
							filterFormik.setFieldValue(
								"shipDate",
								value?.isValid() ? value.format("YYYY-MM-DD") : null
							);
						}}
						helperText={
							filterFormik.touched.shipDate && filterFormik.errors.shipDate
						}
					/>

					<DateField
						fullWidth
						id="deliveryDate"
						label="Delivered Date"
						value={dayjs(filterFormik.values.deliveryDate)}
						onChange={(value) => {
							filterFormik.setFieldValue(
								"deliveryDate",
								value?.isValid() ? value.format("YYYY-MM-DD") : null
							);
						}}
						helperText={
							filterFormik.touched.deliveryDate &&
							filterFormik.errors.deliveryDate
						}
					/>

					<Autocomplete<Store>
						id="sourceStoreId"
						options={stores ?? []}
						getOptionLabel={(option) => option.alias}
						onChange={(e, value) => {
							filterFormik.setFieldValue("sourceStoreId", value?.id || "");
						}}
						value={
							stores?.find(
								(store) => store.id === filterFormik.values.sourceStoreId
							) || null
						}
						renderInput={(params) => (
							<TextField
								{...params}
								sx={{
									width: 300,
								}}
								label="Source Store"
								error={
									filterFormik.touched.sourceStoreId &&
									Boolean(filterFormik.errors.sourceStoreId)
								}
								helperText={
									filterFormik.touched.sourceStoreId &&
									filterFormik.errors.sourceStoreId
								}
							/>
						)}
					/>

					<Autocomplete<Store>
						id="destinationStoreId"
						options={stores ?? []}
						getOptionLabel={(option) => option.alias}
						onChange={(e, value) => {
							filterFormik.setFieldValue("destinationStoreId", value?.id || "");
						}}
						value={
							stores?.find(
								(store) => store.id === filterFormik.values.destinationStoreId
							) || null
						}
						renderInput={(params) => (
							<TextField
								{...params}
								label="Destination Store"
								error={
									filterFormik.touched.destinationStoreId &&
									Boolean(filterFormik.errors.destinationStoreId)
								}
								sx={{
									width: 300,
								}}
								helperText={
									filterFormik.touched.destinationStoreId &&
									filterFormik.errors.destinationStoreId
								}
							/>
						)}
					/>

					<TextField
						fullWidth
						id="status"
						name="status"
						label="Status"
						value={filterFormik.values.status}
						select
						onChange={(e) => {
							filterFormik.setFieldValue(
								"status",
								e.target.value === "All" ? null : e.target.value
							);
						}}
						error={
							filterFormik.touched.status && Boolean(filterFormik.errors.status)
						}
						onBlur={filterFormik.handleBlur}
						helperText={
							filterFormik.touched.status && filterFormik.errors.status
						}
					>
						{[...Object.values(ShipmentStatus), "All"].map((status) => (
							<MenuItem key={status} value={status}>
								{titleCase(status)}
							</MenuItem>
						))}
					</TextField>

					<Button
						variant="contained"
						color="primary"
						onClick={filterFormik.submitForm}
						className="mx-2"
					>
						Search
					</Button>
				</div>
			</FormikProvider>

			<TableContainer component={Paper}>
				<Table>
					<TableHead>
						<TableRow>
							<TableCell align="left">ID</TableCell>
							<TableCell align="left">Dest. Store ID</TableCell>
							<TableCell align="left">Src. Store ID</TableCell>
							<TableCell align="left">Status</TableCell>
							<TableCell align="left">Created At</TableCell>
							<TableCell align="left">Estimated Ship Date</TableCell>
							<TableCell align="left">Estimated Delivery Date</TableCell>
							<TableCell align="left">Shipped Date</TableCell>
							<TableCell align="left">Delivered Date</TableCell>
							<TableCell align="left">Items</TableCell>
							<TableCell align="left"></TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{(filteredShipments || shipments)?.map((shipment) => (
							<TableRow
								key={shipment.id}
								sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
							>
								<TableCell component="th" scope="row" align="left">
									{shipment.id}
								</TableCell>
								<TableCell align="left">
									{shipment.destinationStoreId}
								</TableCell>
								<TableCell align="left">{shipment.sourceStoreId}</TableCell>
								<TableCell align="left">{titleCase(shipment.status)}</TableCell>
								<TableCell align="left">
									{new Date(shipment.createdAt).toLocaleString()}
								</TableCell>
								<TableCell align="left">
									{new Date(shipment.estimatedShipDate).toLocaleString()}
								</TableCell>
								<TableCell align="left">
									{new Date(shipment.estimatedDeliveryDate).toLocaleString()}
								</TableCell>
								<TableCell align="left">
									{shipment.shipDate
										? new Date(shipment.shipDate).toLocaleString()
										: "N/A"}
								</TableCell>
								<TableCell align="left">
									{shipment.deliveryDate
										? new Date(shipment.deliveryDate).toLocaleString()
										: "N/A"}
								</TableCell>
								<TableCell align="left">
									<IconButton
										aria-label="items"
										size="large"
										onClick={() => {
											setSelectedShipmentForItems(shipment);
										}}
									>
										<List />
									</IconButton>
								</TableCell>
								<TableCell align="right">
									<IconButton
										aria-label="edit"
										size="large"
										onClick={() => {
											setSelectedShipmentForEditing(shipment);
										}}
									>
										<Edit />
									</IconButton>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
			<Modal open={openCreateModal} onClose={() => setOpenCreateModal(false)}>
				<CreateShipmentModal onCreate={handleCreate} />
			</Modal>
			<Modal
				open={!!selectedShipmentForEditing}
				onClose={() => setSelectedShipmentForEditing(null)}
			>
				{selectedShipmentForEditing ? (
					<CreateShipmentModal
						onEdit={handleEdit}
						shipment={selectedShipmentForEditing}
					/>
				) : (
					<></>
				)}
			</Modal>
			<Modal
				open={!!selectedShipmentForItems}
				onClose={() => setSelectedShipmentForItems(null)}
			>
				{selectedShipmentForItems ? (
					<div>
						<Paper sx={modalStyle}>
							<TableContainer component={Paper}>
								<Table>
									<TableHead>
										<TableRow>
											<TableCell align="left">Product ID</TableCell>
											<TableCell align="left">Quantity</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{selectedShipmentForItems.items.map((item) => (
											<TableRow
												key={item.productId}
												sx={{
													"&:last-child td, &:last-child th": { border: 0 },
												}}
											>
												<TableCell component="th" scope="row" align="left">
													{item.productId}
												</TableCell>
												<TableCell align="left">{item.quantity}</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
						</Paper>
					</div>
				) : (
					<></>
				)}
			</Modal>
		</>
	);
};

export default ShipmentsPage;
