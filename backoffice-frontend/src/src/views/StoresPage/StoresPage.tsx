import {
	Accordion,
	AccordionDetails,
	AccordionSummary,
	Container,
	IconButton,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
	Button,
	Modal,
	TextField,
} from "@mui/material";
import { printAddress } from "../../utils/address";
import { titleCase } from "title-case";
import { Add, Delete, Edit, ExpandMore } from "@mui/icons-material";
import { useContext, useState } from "react";
import CreateStoreModal from "../../modals/CreateStoreModal";
import { CreateStoreForm } from "../../modals/CreateStoreModal/CreateStoreModal";
import GlobalContext from "../../context/GlobalContext";
import StoresService from "../../api/services/StoresService";
import { FormikProvider, useFormik } from "formik";

const StoresPage = () => {
	const filterFormik = useFormik({
		initialValues: {
			alias: "",
			city: "",
		},
		onSubmit: async (values) => {
			await handleFilter(values);
		},
	});

	const { stores, setStores, fetchStores } = useContext(
		GlobalContext
	) as GlobalContextType;

	const [openCreateModal, setOpenCreateModal] = useState(false);
	const [openEditModal, setOpenEditModal] = useState(false);

	const [selectedStore, setSelectedStore] = useState<Store | null>(null);

	const [filteredStores, setFilteredStores] = useState<Store[] | null>(null);

	const handleCreateStore = async (values: CreateStoreForm) => {
		console.log(values);

		const res = await StoresService.createStore({
			alias: values.alias,
			address: {
				street: values.street,
				number: values.addressNumber,
				city: values.city,
				province: values.province,
				postalCode: values.postalCode,
			},
			phone: values.phone,
			openHours: values.openHours,
		});

		setStores((prevStores) => (prevStores ? [...prevStores, res.data] : null));

		setOpenCreateModal(false);
	};

	const handleEditStore = async (values: CreateStoreForm, storeId: string) => {
		console.log(values);
		console.log(storeId);

		const res = await StoresService.editStore({
			id: storeId,
			alias: values.alias,
			address: {
				street: values.street,
				number: values.addressNumber,
				city: values.city,
				province: values.province,
				postalCode: values.postalCode,
			},
			phone: values.phone,
			openHours: values.openHours,
		});

		setStores(
			(prevStores) =>
				prevStores?.map((store) => {
					if (store.id === storeId) {
						return res.data;
					}
					return store;
				}) || null
		);

		setOpenEditModal(false);
	};

	const handleFilter = async (values: any) => {
		console.log(values);

		if (!values.alias && !values.city) {
			await fetchStores();
			setFilteredStores(null);
			return;
		}

		const res = await StoresService.getStores(values);
		setFilteredStores(res.data);
	};

	const handleDelete = async (storeId: string) => {
		await StoresService.deleteStore(storeId);

		setStores(
			(prevStores) =>
				prevStores?.filter((store) => store.id !== storeId) || null
		);
	};

	return (
		<>
			<Container>
				<Typography variant="h3" component="h1" className="py-4">
					Stores
				</Typography>

				<Button
					variant="contained"
					color="primary"
					startIcon={<Add />}
					onClick={() => setOpenCreateModal(true)}
				>
					Create New Store
				</Button>

				<FormikProvider value={filterFormik}>
					<div className="flex items-center w-100 justify-center">
						<TextField
							name="alias"
							label="Filter by Alias"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.alias}
							onChange={filterFormik.handleChange}
						/>

						<TextField
							name="city"
							label="Filter by City"
							variant="outlined"
							sx={{ mx: 2 }}
							value={filterFormik.values.city}
							onChange={filterFormik.handleChange}
						/>

						<Button
							variant="contained"
							color="primary"
							onClick={filterFormik.submitForm}
							className="mx-2"
						>
							Search
						</Button>
					</div>
				</FormikProvider>

				<TableContainer component={Paper}>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell align="left">Alias</TableCell>
								<TableCell align="left">Address</TableCell>
								<TableCell align="left">Phone</TableCell>
								<TableCell align="left">Open Hours</TableCell>
								<TableCell align="right"></TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{(filteredStores || stores)?.map((store) => (
								<TableRow
									key={store.id}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row" align="left">
										{store.alias}
									</TableCell>
									<TableCell align="left">
										{printAddress(store.address)}
									</TableCell>
									<TableCell align="left">{store.phone}</TableCell>
									<TableCell align="left">
										<Accordion>
											<AccordionSummary
												expandIcon={<ExpandMore />}
												aria-controls="panel1a-content"
											>
												<Typography>View Open Hours</Typography>
											</AccordionSummary>
											<AccordionDetails>
												{Object.entries(store.openHours).map(([day, hours]) => (
													<div>
														<strong>{titleCase(day)}</strong>
														{hours.map((hour) => (
															<p>{hour}</p>
														))}
													</div>
												))}
											</AccordionDetails>
										</Accordion>
									</TableCell>
									<TableCell align="right">
										<IconButton
											aria-label="edit"
											size="large"
											onClick={() => {
												setSelectedStore(store);
												setOpenEditModal(true);
											}}
										>
											<Edit />
										</IconButton>

										<IconButton onClick={() => handleDelete(store.id)}>
											<Delete />
										</IconButton>
									</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Container>
			<Modal open={openCreateModal} onClose={() => setOpenCreateModal(false)}>
				<CreateStoreModal onCreate={handleCreateStore} />
			</Modal>
			<Modal open={openEditModal} onClose={() => setOpenEditModal(false)}>
				<CreateStoreModal
					onEdit={handleEditStore}
					initialValues={selectedStore}
				/>
			</Modal>
		</>
	);
};

export default StoresPage;
