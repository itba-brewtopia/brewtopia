import { LocalizationProvider } from "@mui/x-date-pickers";
import Router from "./router/Router";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { GlobalContextProvider } from "./context/GlobalContext";
import ApiWrapper from "./components/ApiWrapper/ApiWrapper";

function App() {
	return (
		<div>
			<LocalizationProvider dateAdapter={AdapterDayjs}>
				<GlobalContextProvider>
					<ApiWrapper>
						<Router />
					</ApiWrapper>
				</GlobalContextProvider>
			</LocalizationProvider>
		</div>
	);
}

export default App;
