type Category = {
	id: string;
	name: string;
	image: string;
	isDeleted: boolean;
	createdAt: string;
	modifiedAt: string;
};

type Product = {
	id: string;
	name: string;
	description: string;
	price: string;
	createdAt: string;
	modifiedAt: string;
	isDeleted: boolean;
	category: Category;
	images: string[];
};

type Customer = {
	id: string;
	name: string;
	lastName: string;
	email: string;
	isVerified: boolean;
	isDeleted: boolean;
	modifiedAt: string;
	createdAt: string;
	lastLogin: string;
};

type CartItem = {
	productId: string;
	quantity: number;
};

type Cart = {
	customerId: string;
	modifiedAt: string;
	createdAt: string;
	isDeleted: boolean;
	items: CartItem[];
	availableDate: string;
	storeId: string;
	totalPrice: string;
};

type Address = {
	street: string;
	number: string;
	postalCode: string;
	city: string;
	province: string;
};

type OpenHours = {
	monday: string[];
	tuesday: string[];
	wednesday: string[];
	thursday: string[];
	friday: string[];
	saturday: string[];
	sunday: string[];
};

type Store = {
	id: string;
	address: Address;
	alias: string;
	phone: string;
	openHours: OpenHours;
	isDeleted: boolean;
	createdAt: string;
	modifiedAt: string;
};

type OrderItem = {
	productId: string;
	quantity: number;
	price: string;
	image: string;
	name: string;
};

type Order = {
	id: string;
	customerId: string;
	createdAt: string;
	modifiedAt: string;
	totalPrice: string;
	pickupDate: string | null;
	pickupReadyDate: string;
	pickupStoreId: string;
	isDeleted: boolean;
	status: OrderStatus;
	items: OrderItem[];
};

type FullCartItem = Product & { quantity: number };

type ShipmentItem = {
	productId: string;
	quantity: number;
};

type Shipment = {
	id: string;
	destinationStoreId: string;
	sourceStoreId: string;
	status: import("../constants/ShipmentStatus").default;
	shipDate: string | null;
	estimatedShipDate: string;
	estimatedDeliveryDate: string;
	deliveryDate: string | null;
	modifiedAt: string;
	createdAt: string;
	items: ShipmentItem[];
};

type ProductStock = {
	productId: string;
	storeId: string;
	quantity: number;
	modifiedAt: string;
	createdAt: string;
	isDeleted: boolean;
	minimumQuantity: number;
};

type GlobalContextType = {
	error: string | null;
	isLoading: boolean;
	categories: Category[] | null;
	products: Product[] | null;
	customers: Customer[] | null;
	stores: Store[] | null;
	orders: Order[] | null;
	shipments: Shipment[] | null;
	productStocks: ProductStock[] | null;
	setCategories: React.Dispatch<React.SetStateAction<Category[] | null>>;
	setProducts: React.Dispatch<React.SetStateAction<Product[] | null>>;
	setCustomers: React.Dispatch<React.SetStateAction<Customer[] | null>>;
	setStores: React.Dispatch<React.SetStateAction<Store[] | null>>;
	setOrders: React.Dispatch<React.SetStateAction<Order[] | null>>;
	setShipments: React.Dispatch<React.SetStateAction<Shipment[] | null>>;
	setProductStocks: React.Dispatch<React.SetStateAction<ProductStock[] | null>>;
	fetchProductStocks: () => Promise<void>;
	fetchShipments: () => Promise<void>;
	fetchStores: () => Promise<void>;
	fetchOrders: () => Promise<void>;
	fetchCustomers: () => Promise<void>;
	fetchProducts: () => Promise<void>;
};
