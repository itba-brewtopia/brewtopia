import React, { useContext } from "react";
import GlobalContext from "../../context/GlobalContext";
import { CircularProgress } from "@mui/material";

type Props = {
	children: React.ReactNode;
};

const ApiWrapper = (props: Props) => {
	const { isLoading, error } = useContext(GlobalContext) as GlobalContextType;

	if (isLoading) {
		return (
			<div className="w-screen h-screen flex justify-center items-center">
				<CircularProgress />
			</div>
		);
	}

	if (error) {
		return (
			<div className="w-screen h-screen flex justify-center items-center">
				<p>{error}</p>
			</div>
		);
	}

	return <>{props.children}</>;
};

export default ApiWrapper;
