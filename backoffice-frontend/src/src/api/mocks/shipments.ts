import { faker } from "@faker-js/faker";
import { PRODUCTS } from "./products";
import { STORES } from "./stores";
import ShipmentStatus from "../../constants/ShipmentStatus";

export const createRandomShipmentItem = (product: Product): ShipmentItem => ({
	productId: product.id,
	quantity: faker.number.int({ min: 20, max: 200 }),
});

export const createRandomShipment = (): Shipment => {
	const shipmentProducts = faker.helpers.arrayElements(PRODUCTS, 8);
	const shipmentItems = shipmentProducts.map((item) =>
		createRandomShipmentItem(item)
	);
	const [sourceStoreId, destinationStoreId] = faker.helpers
		.arrayElements(STORES, 2)
		.map((store) => store.id);

	return {
		id: faker.string.uuid(),
		createdAt: faker.date.past().toISOString(),
		modifiedAt: faker.date.past().toISOString(),
		items: shipmentItems,
		deliveryDate: faker.date.future().toISOString(),
		estimatedDeliveryDate: faker.date.future().toISOString(),
		estimatedShipDate: faker.date.future().toISOString(),
		shipDate: faker.date.future().toISOString(),
		sourceStoreId,
		destinationStoreId,
		status: faker.helpers.arrayElement(Object.values(ShipmentStatus)),
	};
};

export const SHIPMENTS = Array.from({ length: 10 }, createRandomShipment);
