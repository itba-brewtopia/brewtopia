import { faker } from "@faker-js/faker";

export const createRandomCustomer = (): Customer => ({
	id: faker.string.uuid(),
	name: faker.person.firstName(),
	lastName: faker.person.lastName(),
	email: faker.internet.email(),
	isVerified: true,
	isDeleted: false,
	modifiedAt: faker.date.past().toISOString(),
	createdAt: faker.date.past().toISOString(),
	lastLogin: faker.date.past().toISOString(),
});

export const CUSTOMERS = Array.from({ length: 10 }, createRandomCustomer);
