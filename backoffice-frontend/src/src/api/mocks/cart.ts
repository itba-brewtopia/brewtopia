import { faker } from "@faker-js/faker";
import { PRODUCTS } from "./products";
import { STORES } from "./stores";

export const createRandomCartItem = (productId: string): CartItem => ({
	productId,
	quantity: faker.number.int({ min: 1, max: 10 }),
});

export const createRandomCart = (): Cart => {
	const cartProducts = faker.helpers.arrayElements(PRODUCTS, 5);
	const cartItems = cartProducts.map((item) => createRandomCartItem(item.id));
	const totalPrice = cartItems.reduce((acc, item) => {
		const product = PRODUCTS.find((product) => product.id === item.productId)!;
		return acc + parseFloat(product.price) * item.quantity;
	}, 0);

	return {
		customerId: faker.string.uuid(),
		modifiedAt: faker.date.past().toISOString(),
		createdAt: faker.date.past().toISOString(),
		isDeleted: false,
		items: cartItems,
		availableDate: faker.date.future().toISOString(),
		storeId: faker.helpers.arrayElement(STORES).id,
		totalPrice: totalPrice.toFixed(2).toString(),
	};
};

export const CARTS = Array.from({ length: 10 }, createRandomCart);
