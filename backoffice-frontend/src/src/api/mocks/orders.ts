import { faker } from "@faker-js/faker";
import { CUSTOMERS } from "./customer";
import { PRODUCTS } from "./products";
import { STORES } from "./stores";
import OrderStatus from "../../constants/OrderStatus";

export const createRandomOrderItem = (product: Product): OrderItem => ({
	productId: product.id,
	quantity: faker.number.int({ min: 1, max: 10 }),
	price: product.price,
	image: product.images[0],
	name: product.name,
});

export const createRandomOrder = (): Order => {
	const orderProducts = faker.helpers.arrayElements(PRODUCTS, 8);
	const orderItems = orderProducts.map((item) => createRandomOrderItem(item));
	const totalPrice = orderItems.reduce((acc, item) => {
		return acc + parseFloat(item.price) * item.quantity;
	}, 0);

	return {
		id: faker.string.uuid(),
		customerId: faker.helpers.arrayElement(CUSTOMERS).id,
		createdAt: faker.date.past().toISOString(),
		modifiedAt: faker.date.past().toISOString(),
		isDeleted: false,
		status: faker.helpers.arrayElement(Object.values(OrderStatus)),
		items: orderItems,
		totalPrice: totalPrice.toFixed(2).toString(),
		pickupDate: faker.date.future().toISOString(),
		pickupStoreId: faker.helpers.arrayElement(STORES).id,
		pickupReadyDate: faker.date.future().toISOString(),
	};
};

export const ORDERS = Array.from({ length: 10 }, createRandomOrder);
