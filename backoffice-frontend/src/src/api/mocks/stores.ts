import { faker } from "@faker-js/faker";

export const createRandomStore = (): Store => ({
	id: faker.string.uuid(),
	address: {
		street: faker.location.street(),
		number: faker.location.buildingNumber(),
		postalCode: faker.location.zipCode(),
		city: faker.location.city(),
		province: faker.location.state(),
	},
	openHours: {
		monday: ["08:00 - 12:00", "14:00 - 18:00"],
		tuesday: ["08:00 - 12:00", "14:00 - 18:00"],
		wednesday: ["08:00 - 12:00", "14:00 - 18:00"],
		thursday: ["08:00 - 12:00", "14:00 - 18:00"],
		friday: ["08:00 - 12:00", "14:00 - 18:00"],
		saturday: ["Closed"],
		sunday: ["Closed"],
	},
	alias: faker.company.name(),
	phone: faker.phone.number(),
	isDeleted: false,
	createdAt: faker.date.past().toISOString(),
	modifiedAt: faker.date.past().toISOString(),
});

export const STORES = Array.from({ length: 10 }, createRandomStore);
