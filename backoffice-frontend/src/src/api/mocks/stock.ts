import { faker } from "@faker-js/faker";
import { PRODUCTS } from "./products";
import { STORES } from "./stores";

export const createRandomProductStock = (
	productId: string,
	storeId: string
): ProductStock => ({
	productId,
	storeId,
	quantity: faker.number.int({ min: 0, max: 100 }),
	minimumQuantity: faker.number.int({ min: 10, max: 100 }),
	modifiedAt: faker.date.past().toISOString(),
	createdAt: faker.date.past().toISOString(),
	isDeleted: false,
});

export const PRODUCT_STOCKS = PRODUCTS.map((product) =>
	STORES.map((store) => createRandomProductStock(product.id, store.id))
).flat();
