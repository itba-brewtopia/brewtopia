import BrewtopiaApi from "../BrewtopiaApi";

const getCustomersByEmail = (email: string) =>
	BrewtopiaApi.get<GetCustomerResponseDto[]>("/customers", {
		params: { email },
	});

const getCustomerById = (customerId: string) =>
	BrewtopiaApi.get<GetCustomerResponseDto>("/customers", {
		params: { customerId },
	});

const getAllCustomers = (filter?: {
	email?: string;
	name?: string;
	lastName?: string;
}) =>
	BrewtopiaApi.get<GetCustomerResponseDto[]>("/customers", {
		params: filter,
	});

const deleteCustomer = (customerId: string) =>
	BrewtopiaApi.patch<GetCustomerResponseDto>(`/customers/${customerId}`, {
		isDeleted: true,
	});

const CustomersService = {
	getCustomersByEmail,
	getCustomerById,
	getAllCustomers,
	deleteCustomer,
};

export default CustomersService;
