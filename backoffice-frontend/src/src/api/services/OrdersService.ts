import OrderStatus from "../../constants/OrderStatus";
import BrewtopiaApi from "../BrewtopiaApi";

const getAllOrders = (filters?: {
	status?: OrderStatus | null;
	customerId?: string | null;
	pickupReadyDate?: string | null;
	storeId?: string | null;
}) => BrewtopiaApi.get<GetOrderResponseDto[]>("/orders", { params: filters });

const getOrdersByCustomerId = (customerId: string) =>
	BrewtopiaApi.get<GetOrderResponseDto[]>("/orders", {
		params: { customerId },
	});

const buyCart = (customerId: string, pickupStoreId: string) =>
	BrewtopiaApi.post<GetOrderResponseDto>(`/orders`, {
		customerId,
		pickupStoreId,
	});

const editOrder = ({ id, status }: { id: string; status: OrderStatus }) =>
	BrewtopiaApi.patch<GetOrderResponseDto>(`/orders/${id}`, { status });

const OrdersService = {
	getOrdersByCustomerId,
	buyCart,
	getAllOrders,
	editOrder,
};

export default OrdersService;
