import BrewtopiaApi from "../BrewtopiaApi";

const getProductById = (id: string) =>
	BrewtopiaApi.get<GetProductResponseDto>(`/products/${id}`);

const getProductsByCategory = (category: string) =>
	BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
		params: { category },
	});

const getAllProducts = (filters?: {
	category?: string | null;
	query?: string | null;
	minPrice?: string | null;
	maxPrice?: string | null;
}) =>
	BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
		params: filters,
	});

const getProductsByPrice = (minPrice: number, maxPrice: number) =>
	BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
		params: { minPrice, maxPrice },
	});

const searchProducts = (searchTerm: string) =>
	BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
		params: { query: searchTerm },
	});

const getAllCategories = () =>
	BrewtopiaApi.get<GetCategoryResponseDto[]>(`/categories`);

const createProduct = ({
	name,
	description,
	price,
	categoryId,
	images,
}: {
	name: string;
	description: string;
	price: string;
	categoryId: string;
	images: string[];
}) =>
	BrewtopiaApi.post<GetProductResponseDto>(`/products`, {
		name,
		description,
		price,
		categoryId,
		images,
	});

const editProduct = ({
	id,
	name,
	description,
	price,
	categoryId,
	images,
}: {
	id: string;
	name: string;
	description: string;
	price: string;
	categoryId: string;
	images: string[];
}) =>
	BrewtopiaApi.patch<GetProductResponseDto>(`/products/${id}`, {
		name,
		description,
		price,
		categoryId,
		images,
	});

const deleteProduct = (id: string) =>
	BrewtopiaApi.patch<GetProductResponseDto>(`/products/${id}`, {
		isDeleted: true,
	});

const createProductCategory = ({
	name,
	image,
}: {
	name: string;
	image: string;
}) =>
	BrewtopiaApi.post<GetCategoryResponseDto>(`/categories`, {
		name,
		image,
	});

const editProductCategory = ({
	id,
	name,
	image,
}: {
	id: string;
	name: string;
	image: string;
}) =>
	BrewtopiaApi.patch<GetCategoryResponseDto>(`/categories/${id}`, {
		name,
		image,
	});

const deleteProductCategory = (id: string) =>
	BrewtopiaApi.patch<GetCategoryResponseDto>(`/categories/${id}`, {
		isDeleted: true,
	});

const ProductsService = {
	getProductById,
	getProductsByCategory,
	getAllProducts,
	getProductsByPrice,
	searchProducts,
	getAllCategories,
	createProduct,
	editProduct,
	deleteProduct,
	createProductCategory,
	editProductCategory,
	deleteProductCategory,
};

export default ProductsService;
