import ShipmentStatus from "../../constants/ShipmentStatus";
import BrewtopiaApi from "../BrewtopiaApi";

const getAllShipments = (filter?: {
	status?: ShipmentStatus;
	sourceStoreId?: string;
	destinationStoreId?: string;
	shipDate?: string;
	deliveryDate?: string;
	estimatedShipDate?: string;
	estimatedDeliveryDate?: string;
}) =>
	BrewtopiaApi.get<GetShipmentResponseDto[]>("/store-shipments", {
		params: filter,
	});

const createShipment = ({
	destinationStoreId,
	sourceStoreId,
	estimatedShipDate,
	estimatedDeliveryDate,
	items,
}: {
	destinationStoreId: string;
	sourceStoreId: string;
	estimatedShipDate: string;
	estimatedDeliveryDate: string;
	items: ShipmentItem[];
}) =>
	BrewtopiaApi.post<GetShipmentResponseDto>("/store-shipments", {
		destinationStoreId,
		sourceStoreId,
		estimatedShipDate,
		estimatedDeliveryDate,
		items,
	});

const updateShipment = (id: string, status: ShipmentStatus) =>
	BrewtopiaApi.patch<GetShipmentResponseDto>(`/store-shipments/${id}`, {
		status,
	});

const ShipmentsService = {
	getAllShipments,
	createShipment,
	updateShipment,
};

export default ShipmentsService;
