import BrewtopiaApi from "../BrewtopiaApi";

const getStoreById = (storeId: string) =>
	BrewtopiaApi.get<GetStoreResponseDto>("/stores", {
		params: { storeId },
	});

const getStores = (filters?: { alias?: string; city?: string }) =>
	BrewtopiaApi.get<GetStoreResponseDto[]>("/stores", {
		params: filters,
	});

const createStore = ({
	alias,
	address,
	phone,
	openHours,
}: {
	alias: string;
	address: Address;
	phone: string;
	openHours: OpenHours;
}) =>
	BrewtopiaApi.post<GetStoreResponseDto>("/stores", {
		alias,
		address,
		phone,
		openHours,
	});

const editStore = ({
	id,
	alias,
	address,
	phone,
	openHours,
}: {
	id: string;
	alias: string;
	address: Address;
	phone: string;
	openHours: OpenHours;
}) =>
	BrewtopiaApi.patch<GetStoreResponseDto>(`/stores/${id}`, {
		alias,
		address,
		phone,
		openHours,
	});

const deleteStore = (storeId: string) =>
	BrewtopiaApi.patch<GetStoreResponseDto>(`/stores/${storeId}`, {
		isDeleted: true,
	});

const StoresService = {
	getStoreById,
	getStores,
	createStore,
	editStore,
	deleteStore,
};

export default StoresService;
