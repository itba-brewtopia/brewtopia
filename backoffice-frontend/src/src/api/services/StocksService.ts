import BrewtopiaApi from "../BrewtopiaApi";

const getAllStocks = () => BrewtopiaApi.get<GetProductStockDto[]>("/stock");

const editStock = ({
	productId,
	storeId,
	quantity,
	minimumQuantity,
	action = "change",
}: {
	productId: string;
	storeId: string;
	quantity: number;
	minimumQuantity: number;
	action?: string;
}) =>
	BrewtopiaApi.patch<GetProductStockDto>(`/stock`, {
		productId,
		storeId,
		quantity,
		minimumQuantity,
		action,
	});

const getStockAvailableDate = (storeId: string, items: CartItem[]) =>
	BrewtopiaApi.post<string>(`/stock/available`, {
		storeId,
		items,
	});

const restock = () => BrewtopiaApi.post(`/stock/restock`);

const StocksService = {
	getAllStocks,
	editStock,
	getStockAvailableDate,
	restock,
};

export default StocksService;
