import { BrowserRouter, Route, Routes } from "react-router-dom";
import routes from "./routes";
import HomePage from "../views/HomePage";
import OrdersPage from "../views/OrdersPage";
import StoresPage from "../views/StoresPage";
import Navbar from "../components/Navbar";
import ShipmentsPage from "../views/ShipmentsPage";
import ProductsPage from "../views/ProductsPage";
import CustomersPage from "../views/CustomersPage";
import CategoriesPage from "../views/CategoriesPage/CategoriesPage";
import ProductStockPage from "../views/ProductStockPage/ProductStockPage";

const Router = () => {
	return (
		<BrowserRouter>
			<Navbar />
			<Routes>
				<Route path={routes.Home.path} element={<HomePage />} />
				<Route path={routes.Shipments.path} element={<ShipmentsPage />} />
				<Route path={routes.ProductsAndStock.path} element={<ProductsPage />} />
				<Route path={routes.ProductStock.path} element={<ProductStockPage />} />

				<Route path={routes.Orders.path} element={<OrdersPage />} />
				<Route path={routes.Customers.path} element={<CustomersPage />} />
				<Route path={routes.Stores.path} element={<StoresPage />} />
				<Route path={routes.Categories.path} element={<CategoriesPage />} />
			</Routes>
		</BrowserRouter>
	);
};

export default Router;
