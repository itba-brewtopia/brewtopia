const routes = {
	Home: {
		path: "/",
		name: "Home",
	},
	ProductsAndStock: {
		path: "/products",
		name: "Products & Stock",
	},
	Orders: {
		path: "/orders",
		name: "Orders",
	},
	Customers: {
		path: "/customers",
		name: "Customers",
	},
	Shipments: {
		path: "/shipments",
		name: "Shipments",
	},
	Stores: {
		path: "/stores",
		name: "Stores",
	},
	Categories: {
		path: "/categories",
		name: "Categories",
	},
	ProductStock: {
		path: "/products/:id/stock",
		name: "Product Stock",
	},
};

export default routes;
