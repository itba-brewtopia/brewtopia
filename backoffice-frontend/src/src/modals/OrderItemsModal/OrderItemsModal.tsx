import {
	TableContainer,
	Paper,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
} from "@mui/material";
import React from "react";
import modalStyle from "../../styles/modalStyles";
import { printPrice } from "../../utils/price";

type OrderItemsModalProps = {
	order: Order;
};

const OrderItemsModal = ({ order }: OrderItemsModalProps) => {
	return (
		<div>
			<Paper sx={modalStyle}>
				<TableContainer component={Paper}>
					<Table aria-label="order item list">
						<TableHead>
							<TableRow>
								<TableCell>Product</TableCell>
								<TableCell align="right">Quantity</TableCell>
								<TableCell align="right">Price per unit</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{order.items.map((item) => (
								<TableRow
									key={item.productId}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row">
										{item.name}
									</TableCell>
									<TableCell align="right">{item.quantity}</TableCell>
									<TableCell align="right">{printPrice(item.price)}</TableCell>
								</TableRow>
							))}
							<TableRow>
								<TableCell colSpan={2}>Total</TableCell>
								<TableCell align="right">
									{printPrice(order.totalPrice)}
								</TableCell>
							</TableRow>
						</TableBody>
					</Table>
				</TableContainer>
			</Paper>
		</div>
	);
};

export default OrderItemsModal;
