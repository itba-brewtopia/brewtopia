import { Button, Paper, TextField, Typography } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import React from "react";
import * as Yup from "yup";
import modalStyle from "../../styles/modalStyles";

const validationSchema = Yup.object({
	name: Yup.string().required("Required"),
	image: Yup.string().url("Invalid URL").required("Required"),
});

export type CreateCategoryForm = {
	name: string;
	image: string;
};

export type CreateCategoryProps = {
	onCreate?: (form: CreateCategoryForm) => Promise<void>;
	onEdit?: (form: CreateCategoryForm, categoryId: string) => Promise<void>;
	category?: Category | null;
};

const CreateCategoryModal = ({
	category,
	onCreate,
	onEdit,
}: CreateCategoryProps) => {
	const isEditing = Boolean(category && onEdit);

	const formik = useFormik({
		initialValues: {
			name: category?.name || "",
			image: category?.image || "",
		},
		validationSchema,
		onSubmit: async (values) => {
			if (isEditing) {
				await onEdit!(values, category!.id);
				return;
			}

			if (onCreate) {
				await onCreate(values);
			}
		},
	});

	return (
		<div>
			<Paper sx={modalStyle}>
				<Typography id="modal-modal-title" variant="h6" component="h2">
					{isEditing ? "Edit" : "Create"} Category
				</Typography>
				<FormikProvider value={formik}>
					<TextField
						id="name"
						name="name"
						label="Category Name"
						value={formik.values.name}
						onChange={formik.handleChange}
						error={formik.touched.name && Boolean(formik.errors.name)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.name && formik.errors.name}
						fullWidth
					/>

					<TextField
						id="image"
						name="image"
						label="Image URL"
						value={formik.values.image}
						onChange={formik.handleChange}
						error={formik.touched.image && Boolean(formik.errors.image)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.image && formik.errors.image}
						fullWidth
					/>

					<Button variant="contained" fullWidth onClick={formik.submitForm}>
						{isEditing ? "Edit" : "Create"}
					</Button>
				</FormikProvider>
			</Paper>
		</div>
	);
};

export default CreateCategoryModal;
