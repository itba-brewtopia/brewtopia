import { Button, MenuItem, Paper, TextField } from "@mui/material";
import modalStyle from "../../styles/modalStyles";
import { FormikProvider, useFormik } from "formik";
import OrderStatus from "../../constants/OrderStatus";
import * as Yup from "yup";

const statusOptions = Object.values(OrderStatus);

const validationSchema = Yup.object({
	status: Yup.string().oneOf(statusOptions).required("Required"),
});

export type EditOrderForm = {
	status: OrderStatus;
};

export type EditOrderModalProps = {
	order: Order;
	onSubmit: (values: EditOrderForm, orderId: string) => Promise<void>;
};

const EditOrderModal = ({ order, onSubmit }: EditOrderModalProps) => {
	const formik = useFormik<EditOrderForm>({
		initialValues: {
			status: order.status,
		},
		onSubmit: (values) => {
			onSubmit(values, order.id);
		},
		validationSchema,
	});

	return (
		<div>
			<Paper sx={modalStyle}>
				<FormikProvider value={formik}>
					<TextField
						select
						name="status"
						id="status"
						label="Status"
						value={formik.values.status}
						onChange={formik.handleChange}
						error={formik.touched.status && Boolean(formik.errors.status)}
						helperText={formik.touched.status && formik.errors.status}
						sx={{
							width: "20vw",
						}}
					>
						{statusOptions.map((option) => (
							<MenuItem key={option} value={option}>
								{option}
							</MenuItem>
						))}
					</TextField>

					<div className="mb-3" />

					<Button
						variant="contained"
						sx={{ marginTop: 2 }}
						disabled={formik.isSubmitting || !formik.isValid}
						onClick={formik.handleSubmit as any}
						fullWidth
					>
						Save
					</Button>
				</FormikProvider>
			</Paper>
		</div>
	);
};

export default EditOrderModal;
