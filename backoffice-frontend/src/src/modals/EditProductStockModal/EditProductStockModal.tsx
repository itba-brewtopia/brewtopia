import { Button, Paper, TextField, Typography } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import React from "react";
import * as Yup from "yup";
import modalStyle from "../../styles/modalStyles";

export type EditProductStockForm = {
	quantity: number;
	minimumQuantity: number;
};

type EditProductStockModalProps = {
	onEdit: (
		form: EditProductStockForm,
		productId: string,
		storeId: string
	) => Promise<void>;
	productStock: ProductStock;
};

const validationSchema = Yup.object({
	quantity: Yup.number().required("Required"),
	minimumQuantity: Yup.number().required("Required"),
});

const EditProductStockModal = ({
	onEdit,
	productStock,
}: EditProductStockModalProps) => {
	const formik = useFormik({
		initialValues: {
			quantity: productStock?.quantity || 0,
			minimumQuantity: productStock?.minimumQuantity || 0,
		},
		validationSchema,
		onSubmit: async (values) => {
			await onEdit(values, productStock?.productId, productStock?.storeId);
		},
	});

	return (
		<Paper sx={modalStyle}>
			<Typography id="modal-modal-title" variant="h6" component="h2">
				Edit Product Stock
			</Typography>

			<FormikProvider value={formik}>
				<TextField
					id="quantity"
					name="quantity"
					label="Quantity"
					type="number"
					value={formik.values.quantity}
					onChange={formik.handleChange}
					error={Boolean(formik.errors.quantity)}
					helperText={formik.errors.quantity}
					sx={{ marginBottom: 2 }}
					fullWidth
				/>

				<TextField
					id="minimumQuantity"
					name="minimumQuantity"
					label="Minimum Quantity"
					type="number"
					value={formik.values.minimumQuantity}
					onChange={formik.handleChange}
					error={Boolean(formik.errors.minimumQuantity)}
					helperText={formik.errors.minimumQuantity}
					sx={{ marginBottom: 2 }}
					fullWidth
				/>

				<Button variant="contained" color="primary" onClick={formik.submitForm}>
					Save
				</Button>
			</FormikProvider>
		</Paper>
	);
};

export default EditProductStockModal;
