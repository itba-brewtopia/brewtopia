import {
	TableContainer,
	Paper,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
} from "@mui/material";
import React from "react";
import modalStyle from "../../styles/modalStyles";

type CartItemsModalProps = {
	cart: Cart;
};

const CartItemsModal = ({ cart }: CartItemsModalProps) => {
	return (
		<div>
			<Paper sx={modalStyle}>
				<TableContainer component={Paper}>
					<Table aria-label="order item list">
						<TableHead>
							<TableRow>
								<TableCell>Product ID</TableCell>
								<TableCell align="right">Quantity</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{cart.items.map((item) => (
								<TableRow
									key={item.productId}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row">
										{item.productId}
									</TableCell>
									<TableCell align="right">{item.quantity}</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			</Paper>
		</div>
	);
};

export default CartItemsModal;
