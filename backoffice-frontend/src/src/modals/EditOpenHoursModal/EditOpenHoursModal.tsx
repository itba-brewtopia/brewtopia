import { FormikProvider, useFormik } from "formik";
import * as Yup from "yup";
import { Button, Paper, TextField, Typography } from "@mui/material";
import { CreateStoreForm } from "../CreateStoreModal/CreateStoreModal";
import modalStyle from "../../styles/modalStyles";

type EditOpenHoursModalProps = {
	initialValues: CreateStoreForm["openHours"];
	onSubmit: (values: CreateStoreForm["openHours"]) => void;
};

export type EditOpenHoursForm = {
	monday: string;
	tuesday: string;
	wednesday: string;
	thursday: string;
	friday: string;
	saturday: string;
	sunday: string;
};

const validationSchema = Yup.object({
	monday: Yup.string(),
	tuesday: Yup.string(),
	wednesday: Yup.string(),
	thursday: Yup.string(),
	friday: Yup.string(),
	saturday: Yup.string(),
	sunday: Yup.string(),
});

const arrayToString = (openHours: string[]) => openHours.join(",");
const stringToArray = (openHours: string) =>
	openHours ? openHours.split(",") : [];

const EditOpenHoursModal = ({
	initialValues,
	onSubmit,
}: EditOpenHoursModalProps) => {
	const formik = useFormik<EditOpenHoursForm>({
		initialValues: {
			monday: arrayToString(initialValues.monday),
			tuesday: arrayToString(initialValues.tuesday),
			wednesday: arrayToString(initialValues.wednesday),
			thursday: arrayToString(initialValues.thursday),
			friday: arrayToString(initialValues.friday),
			saturday: arrayToString(initialValues.saturday),
			sunday: arrayToString(initialValues.sunday),
		},
		onSubmit: (values) => {
			console.log(values);
			onSubmit({
				monday: stringToArray(values.monday),
				tuesday: stringToArray(values.tuesday),
				wednesday: stringToArray(values.wednesday),
				thursday: stringToArray(values.thursday),
				friday: stringToArray(values.friday),
				saturday: stringToArray(values.saturday),
				sunday: stringToArray(values.sunday),
			});
		},
		validationSchema,
		validateOnMount: true,
		enableReinitialize: true,
	});

	return (
		<div>
			<Paper sx={modalStyle}>
				<Typography id="modal-modal-title" variant="h6" component="h2">
					Edit Open Hours
				</Typography>
				<FormikProvider value={formik}>
					<Typography variant="body1" component="p">
						Enter the open hours for each day of the week, separated by commas.
						If the store is closed on a certain day, leave the field empty.
					</Typography>

					<TextField
						id={`monday`}
						name={`monday`}
						label="Monday"
						value={formik.values.monday}
						onChange={formik.handleChange}
						error={formik.touched.monday && Boolean(formik.errors.monday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.monday && formik.errors.monday}
						fullWidth
					/>

					<TextField
						id={`tuesday`}
						name={`tuesday`}
						label="Tuesday"
						value={formik.values.tuesday}
						onChange={formik.handleChange}
						error={formik.touched.tuesday && Boolean(formik.errors.tuesday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.tuesday && formik.errors.tuesday}
						fullWidth
					/>
					<TextField
						id={`wednesday`}
						name={`wednesday`}
						label="Wednesday"
						value={formik.values.wednesday}
						onChange={formik.handleChange}
						error={formik.touched.wednesday && Boolean(formik.errors.wednesday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.wednesday && formik.errors.wednesday}
						fullWidth
					/>
					<TextField
						id={`thursday`}
						name={`thursday`}
						label="Thursday"
						value={formik.values.thursday}
						onChange={formik.handleChange}
						error={formik.touched.thursday && Boolean(formik.errors.thursday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.thursday && formik.errors.thursday}
						fullWidth
					/>
					<TextField
						id={`friday`}
						name={`friday`}
						label="Friday"
						value={formik.values.friday}
						onChange={formik.handleChange}
						error={formik.touched.friday && Boolean(formik.errors.friday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.friday && formik.errors.friday}
						fullWidth
					/>
					<TextField
						id={`saturday`}
						name={`saturday`}
						label="Saturday"
						value={formik.values.saturday}
						onChange={formik.handleChange}
						error={formik.touched.saturday && Boolean(formik.errors.saturday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.saturday && formik.errors.saturday}
						fullWidth
					/>
					<TextField
						id={`sunday`}
						name={`sunday`}
						label="Sunday"
						value={formik.values.sunday}
						onChange={formik.handleChange}
						error={formik.touched.sunday && Boolean(formik.errors.sunday)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.sunday && formik.errors.sunday}
						fullWidth
					/>

					<div className="mb-2" />
					<Button
						variant="contained"
						onClick={formik.handleSubmit as any}
						fullWidth
					>
						Save
					</Button>
				</FormikProvider>
			</Paper>
		</div>
	);
};

export default EditOpenHoursModal;
