import React, { useContext } from "react";
import ShipmentStatus from "../../constants/ShipmentStatus";
import {
	ArrayHelpers,
	FieldArray,
	FormikErrors,
	FormikProvider,
	useFormik,
} from "formik";
import * as Yup from "yup";
import modalStyle from "../../styles/modalStyles";
import {
	Autocomplete,
	Button,
	IconButton,
	MenuItem,
	Paper,
	TextField,
	Typography,
} from "@mui/material";
import { DateTimeField } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { titleCase } from "title-case";
import { Delete } from "@mui/icons-material";
import GlobalContext from "../../context/GlobalContext";

export type CreateShipmentForm = {
	destinationStoreId: string;
	sourceStoreId: string;
	status: ShipmentStatus;
	estimatedShipDate: string;
	estimatedDeliveryDate: string;
	items: ShipmentItem[];
};

export type CreateShipmentModalProps = {
	onCreate?: (values: CreateShipmentForm) => Promise<void>;
	shipment?: Shipment;
	onEdit?: (values: CreateShipmentForm, shipmentId: string) => Promise<void>;
};

const validationSchema = Yup.object({
	destinationStoreId: Yup.string().required("Required"),
	sourceStoreId: Yup.string().required("Required"),
	status: Yup.string()
		.oneOf(Object.values(ShipmentStatus))
		.required("Required"),
	estimatedShipDate: Yup.string().required("Required"),
	estimatedDeliveryDate: Yup.string().required("Required"),
	items: Yup.array().of(
		Yup.object({
			productId: Yup.string().required("Required"),
			quantity: Yup.number().required("Required"),
		})
	),
});

const CreateShipmentModal = ({
	shipment,
	onCreate,
	onEdit,
}: CreateShipmentModalProps) => {
	const { stores, products } = useContext(GlobalContext) as GlobalContextType;
	const formik = useFormik<CreateShipmentForm>({
		initialValues: {
			destinationStoreId: shipment?.destinationStoreId || "",
			sourceStoreId: shipment?.sourceStoreId || "",
			status: shipment?.status || ShipmentStatus.PENDING,
			estimatedShipDate: shipment?.estimatedShipDate || "",
			estimatedDeliveryDate: shipment?.estimatedDeliveryDate || "",
			items: shipment?.items || [],
		},
		onSubmit: (values) => {
			if (shipment && onEdit) {
				onEdit(values, shipment.id);
			} else if (onCreate) {
				onCreate(values);
			}
		},
		validationSchema,
		enableReinitialize: true,
	});

	const isEditing = Boolean(shipment && onEdit);

	return (
		<div>
			<Paper sx={modalStyle}>
				<Typography id="modal-modal-title" variant="h6" component="h2">
					{isEditing ? "Edit" : "Create"} Shipment
				</Typography>

				<FormikProvider value={formik}>
					<Autocomplete<Store>
						id="destinationStoreId"
						options={stores ?? []}
						getOptionLabel={(option) => option.alias}
						onChange={(e, value) => {
							formik.setFieldValue("destinationStoreId", value?.id || "");
						}}
						value={
							stores?.find(
								(store) => store.id === formik.values.destinationStoreId
							) || null
						}
						renderInput={(params) => (
							<TextField
								{...params}
								label="Destination Store"
								error={
									formik.touched.destinationStoreId &&
									Boolean(formik.errors.destinationStoreId)
								}
								helperText={
									formik.touched.destinationStoreId &&
									formik.errors.destinationStoreId
								}
							/>
						)}
					/>

					<Autocomplete<Store>
						id="sourceStoreId"
						options={stores ?? []}
						getOptionLabel={(option) => option.alias}
						onChange={(e, value) => {
							formik.setFieldValue("sourceStoreId", value?.id || "");
						}}
						value={
							stores?.find(
								(store) => store.id === formik.values.sourceStoreId
							) || null
						}
						renderInput={(params) => (
							<TextField
								{...params}
								label="Source Store"
								error={
									formik.touched.sourceStoreId &&
									Boolean(formik.errors.sourceStoreId)
								}
								helperText={
									formik.touched.sourceStoreId && formik.errors.sourceStoreId
								}
							/>
						)}
					/>

					<DateTimeField
						fullWidth
						id="estimatedShipDate"
						label="Estimated Ship Date"
						value={dayjs(formik.values.estimatedShipDate)}
						onChange={(value) => {
							formik.setFieldValue(
								"estimatedShipDate",
								value?.toISOString() ?? null
							);
						}}
						helperText={
							formik.touched.estimatedShipDate &&
							formik.errors.estimatedShipDate
						}
					/>

					<DateTimeField
						fullWidth
						id="estimatedDeliveryDate"
						label="Estimated Delivery Date"
						value={dayjs(formik.values.estimatedDeliveryDate)}
						onChange={(value) => {
							formik.setFieldValue(
								"estimatedDeliveryDate",
								value?.toISOString() ?? null
							);
						}}
						helperText={
							formik.touched.estimatedShipDate &&
							formik.errors.estimatedShipDate
						}
					/>

					<TextField
						fullWidth
						id="status"
						name="status"
						label="Status"
						value={formik.values.status}
						select
						onChange={formik.handleChange}
						error={formik.touched.status && Boolean(formik.errors.status)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.status && formik.errors.status}
					>
						{Object.values(ShipmentStatus).map((status) => (
							<MenuItem key={status} value={status}>
								{titleCase(status)}
							</MenuItem>
						))}
					</TextField>

					<hr className="my-2" />

					<Typography variant="h6" component="h3">
						Items
					</Typography>

					<FieldArray name="items">
						{(arrayHelpers: ArrayHelpers) => (
							<div>
								{formik.values.items.map((item, index) => (
									<div className="flex" key={index}>
										<Autocomplete<Product>
											id={`items.${index}.productId`}
											options={products ?? []}
											getOptionLabel={(option) => option.name}
											onChange={(e, value) => {
												formik.setFieldValue(
													`items.${index}.productId`,
													value?.id || ""
												);
											}}
											value={
												products?.find(
													(product) =>
														product.id === formik.values.items[index].productId
												) || null
											}
											renderInput={(params) => (
												<TextField
													{...params}
													sx={{
														width: "30vw",
													}}
													label="Product"
													error={
														formik.touched.items?.[index]?.productId &&
														Boolean(
															(
																formik.errors.items?.[
																	index
																] as FormikErrors<ShipmentItem>
															)?.productId
														)
													}
													helperText={
														formik.touched.items?.[index]?.productId &&
														(
															formik.errors.items?.[
																index
															] as FormikErrors<ShipmentItem>
														)?.productId
													}
												/>
											)}
										/>

										<TextField
											fullWidth
											id={`items.${index}.quantity`}
											name={`items.${index}.quantity`}
											label="Quantity"
											value={formik.values.items[index].quantity}
											onChange={formik.handleChange}
											error={
												formik.touched.items?.[index]?.quantity &&
												Boolean(
													(
														formik.errors.items?.[
															index
														] as FormikErrors<ShipmentItem>
													)?.quantity
												)
											}
											onBlur={formik.handleBlur}
											helperText={
												formik.touched.items?.[index]?.quantity &&
												((
													formik.errors.items?.[
														index
													] as FormikErrors<ShipmentItem>
												)?.quantity as string)
											}
										/>

										<IconButton
											onClick={() => arrayHelpers.remove(index)}
											sx={{
												marginLeft: "auto",
											}}
										>
											<Delete />
										</IconButton>
									</div>
								))}

								<Button
									variant="contained"
									onClick={() =>
										arrayHelpers.push({
											productId: "",
											quantity: 0,
										})
									}
								>
									Add Item
								</Button>
							</div>
						)}
					</FieldArray>

					<hr className="my-2" />

					<Button
						variant="contained"
						color="primary"
						fullWidth
						onClick={formik.submitForm}
					>
						{isEditing ? "Save" : "Create"}
					</Button>
				</FormikProvider>
			</Paper>
		</div>
	);
};

export default CreateShipmentModal;
