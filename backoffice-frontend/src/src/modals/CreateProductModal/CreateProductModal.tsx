import {
	Paper,
	Typography,
	TextField,
	Button,
	Autocomplete,
	IconButton,
} from "@mui/material";
import {
	ArrayHelpers,
	FieldArray,
	FormikProvider,
	getIn,
	useFormik,
} from "formik";
import React, { useContext } from "react";
import * as Yup from "yup";
import modalStyle from "../../styles/modalStyles";
import { Delete } from "@mui/icons-material";
import GlobalContext from "../../context/GlobalContext";

export type CreateProductModalProps = {
	onCreate?: (form: CreateProductForm) => Promise<void>;
	onEdit?: (form: CreateProductForm, productId: string) => Promise<void>;
	product?: Product | null;
};

export type CreateProductForm = {
	name: string;
	description: string;
	price: string;
	categoryId: string;
	images: string[];
};

const validationSchema = Yup.object({
	name: Yup.string().required("Required"),
	images: Yup.array()
		.of(Yup.string().required("Required"))
		.required("Required"),
	description: Yup.string().required("Required"),
	price: Yup.number().required("Required"),
	categoryId: Yup.string().required("Required"),
});

const CreateProductModal = ({
	onCreate,
	onEdit,
	product,
}: CreateProductModalProps) => {
	const { categories } = useContext(GlobalContext) as GlobalContextType;
	const isEditing = Boolean(product && onEdit);

	const formik = useFormik({
		initialValues: {
			name: product?.name || "",
			images: product?.images || [],
			description: product?.description || "",
			price: product?.price || "",
			categoryId: product?.category.id || "",
		},
		validationSchema,
		onSubmit: async (values) => {
			if (isEditing) {
				await onEdit!(values, product!.id);
				return;
			}

			if (onCreate) {
				await onCreate(values);
			}
		},
	});

	return (
		<div>
			<Paper sx={modalStyle}>
				<Typography id="modal-modal-title" variant="h6" component="h2">
					{isEditing ? "Edit" : "Create"} Product
				</Typography>
				<FormikProvider value={formik}>
					<TextField
						id="name"
						name="name"
						label="Product Name"
						value={formik.values.name}
						onChange={formik.handleChange}
						error={formik.touched.name && Boolean(formik.errors.name)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.name && formik.errors.name}
						fullWidth
					/>

					<TextField
						id="description"
						name="description"
						label="Description"
						value={formik.values.description}
						onChange={formik.handleChange}
						error={
							formik.touched.description && Boolean(formik.errors.description)
						}
						onBlur={formik.handleBlur}
						helperText={formik.touched.description && formik.errors.description}
						fullWidth
					/>

					<TextField
						id="price"
						name="price"
						type="number"
						label="Price"
						value={formik.values.price}
						onChange={formik.handleChange}
						error={formik.touched.price && Boolean(formik.errors.price)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.price && formik.errors.price}
						fullWidth
					/>

					<Autocomplete<Category>
						id="categoryId"
						options={categories || []}
						getOptionLabel={(option) => option.name}
						onChange={(e, value) => {
							formik.setFieldValue("categoryId", value?.id || "");
						}}
						value={
							categories?.find(
								(category) => category.id === formik.values.categoryId
							) || null
						}
						renderInput={(params) => (
							<TextField
								{...params}
								label="Product Category"
								error={
									formik.touched.categoryId && Boolean(formik.errors.categoryId)
								}
								helperText={
									formik.touched.categoryId && formik.errors.categoryId
								}
							/>
						)}
					/>

					<Typography variant="h6" component="h2">
						Images
					</Typography>

					<FieldArray name="images">
						{({ push, remove }: ArrayHelpers) => (
							<>
								{formik.values.images.map((image, index) => (
									<div key={index} className="flex align-items-center">
										<TextField
											id={`images.${index}`}
											name={`images.${index}`}
											label="Image URL"
											value={image}
											onChange={formik.handleChange}
											error={
												getIn(formik.touched, `images.${index}`) &&
												!!getIn(formik.errors, `images.${index}`)
											}
											onBlur={formik.handleBlur}
											helperText={getIn(formik.errors, `images.${index}`)}
											fullWidth
										/>
										<IconButton onClick={() => remove(index)}>
											<Delete />
										</IconButton>
									</div>
								))}
								<Button variant="contained" onClick={() => push("")}>
									Add Image
								</Button>
							</>
						)}
					</FieldArray>

					<Button variant="contained" fullWidth onClick={formik.submitForm}>
						{isEditing ? "Edit" : "Create"}
					</Button>
				</FormikProvider>
			</Paper>
		</div>
	);
};

export default CreateProductModal;
