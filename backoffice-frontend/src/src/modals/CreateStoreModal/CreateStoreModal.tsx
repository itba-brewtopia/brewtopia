import React from "react";
import { FormikProvider, useFormik } from "formik";
import * as Yup from "yup";
import { Button, Modal, Paper, TextField, Typography } from "@mui/material";
import EditOpenHoursModal from "../EditOpenHoursModal/EditOpenHoursModal";
import modalStyle from "../../styles/modalStyles";

type Props = {
	onCreate?: (values: CreateStoreForm) => Promise<void>;
	onEdit?: (values: CreateStoreForm, storeId: string) => Promise<void>;
	initialValues?: Store | null;
};

export type CreateStoreForm = {
	alias: string;
	street: string;
	addressNumber: string;
	postalCode: string;
	city: string;
	province: string;
	phone: string;
	openHours: {
		monday: string[];
		tuesday: string[];
		wednesday: string[];
		thursday: string[];
		friday: string[];
		saturday: string[];
		sunday: string[];
	};
};

const validationSchema = Yup.object({
	alias: Yup.string().required("Required"),
	street: Yup.string().required("Required"),
	addressNumber: Yup.string().required("Required"),
	postalCode: Yup.string().required("Required"),
	city: Yup.string().required("Required"),
	province: Yup.string().required("Required"),
	phone: Yup.string().required("Required"),
	openHours: Yup.object({
		monday: Yup.array().of(Yup.string()).required("Required"),
		tuesday: Yup.array().of(Yup.string()).required("Required"),
		wednesday: Yup.array().of(Yup.string()).required("Required"),
		thursday: Yup.array().of(Yup.string()).required("Required"),
		friday: Yup.array().of(Yup.string()).required("Required"),
		saturday: Yup.array().of(Yup.string()).required("Required"),
		sunday: Yup.array().of(Yup.string()).required("Required"),
	}),
});

const CreateStoreModal = ({ onCreate, onEdit, initialValues }: Props) => {
	const isEditing = Boolean(initialValues && onEdit);
	const [openEditHoursModal, setOpenEditHoursModal] = React.useState(false);
	const formik = useFormik<CreateStoreForm>({
		initialValues: {
			alias: initialValues?.alias || "",
			street: initialValues?.address?.street || "",
			addressNumber: initialValues?.address?.number || "",
			postalCode: initialValues?.address.postalCode || "",
			city: initialValues?.address.city || "",
			province: initialValues?.address.province || "",
			phone: initialValues?.phone || "",
			openHours: initialValues?.openHours || {
				monday: [],
				tuesday: [],
				wednesday: [],
				thursday: [],
				friday: [],
				saturday: [],
				sunday: [],
			},
		},
		onSubmit: async (values) => {
			console.log(values);
			if (isEditing) {
				await onEdit!(values, initialValues!.id);
				return;
			}

			if (onCreate) {
				await onCreate(values);
			}
		},
		validationSchema,
		validateOnMount: true,
	});

	return (
		<div>
			<Paper sx={modalStyle}>
				<Typography id="modal-modal-title" variant="h6" component="h2">
					{isEditing ? "Edit" : "Create"} Store
				</Typography>
				<FormikProvider value={formik}>
					<TextField
						id="alias"
						name="alias"
						label="Alias"
						value={formik.values.alias}
						onChange={formik.handleChange}
						error={formik.touched.alias && Boolean(formik.errors.alias)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.alias && formik.errors.alias}
						fullWidth
					/>

					<TextField
						id="street"
						name="street"
						label="Street"
						value={formik.values.street}
						onChange={formik.handleChange}
						error={formik.touched.street && Boolean(formik.errors.street)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.street && formik.errors.street}
						fullWidth
					/>

					<TextField
						id="addressNumber"
						name="addressNumber"
						label="Address Number"
						value={formik.values.addressNumber}
						onChange={formik.handleChange}
						error={
							formik.touched.addressNumber &&
							Boolean(formik.errors.addressNumber)
						}
						onBlur={formik.handleBlur}
						helperText={
							formik.touched.addressNumber && formik.errors.addressNumber
						}
						fullWidth
					/>

					<TextField
						id="postalCode"
						name="postalCode"
						label="Postal Code"
						value={formik.values.postalCode}
						onChange={formik.handleChange}
						error={
							formik.touched.postalCode && Boolean(formik.errors.postalCode)
						}
						onBlur={formik.handleBlur}
						helperText={formik.touched.postalCode && formik.errors.postalCode}
						fullWidth
					/>

					<TextField
						id="city"
						name="city"
						label="City"
						value={formik.values.city}
						onChange={formik.handleChange}
						error={formik.touched.city && Boolean(formik.errors.city)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.city && formik.errors.city}
						fullWidth
					/>

					<TextField
						id="province"
						name="province"
						label="Province"
						value={formik.values.province}
						onChange={formik.handleChange}
						error={formik.touched.province && Boolean(formik.errors.province)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.province && formik.errors.province}
						fullWidth
					/>

					<TextField
						id="phone"
						name="phone"
						label="Phone"
						value={formik.values.phone}
						onChange={formik.handleChange}
						error={formik.touched.phone && Boolean(formik.errors.phone)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.phone && formik.errors.phone}
						fullWidth
					/>

					<Button
						variant="contained"
						onClick={() => setOpenEditHoursModal(true)}
						fullWidth
					>
						Edit Open Hours
					</Button>

					<div className="mb-2" />
					<Button
						variant="contained"
						onClick={formik.handleSubmit as any}
						fullWidth
					>
						{initialValues ? "Edit Store" : "Create Store"}
					</Button>
				</FormikProvider>
			</Paper>
			<Modal
				open={openEditHoursModal}
				onClose={() => setOpenEditHoursModal(false)}
			>
				<EditOpenHoursModal
					initialValues={formik.values.openHours}
					onSubmit={(values: CreateStoreForm["openHours"]) => {
						formik.setFieldValue("openHours", values);
						setOpenEditHoursModal(false);
					}}
				/>
			</Modal>
		</div>
	);
};

export default CreateStoreModal;
