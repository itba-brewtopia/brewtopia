import { Button, Paper, TextField } from "@mui/material";
import React from "react";
import modalStyle from "../../styles/modalStyles";
import { FormikProvider, useFormik } from "formik";
import * as Yup from "yup";

export type EditCustomerForm = {
	name: string;
	lastName: string;
	email: string;
};

type EditCustomerModalProps = {
	customer: Customer;
	onSubmit: (values: EditCustomerForm, customerId: string) => Promise<void>;
};

const validationSchema = Yup.object({
	name: Yup.string().required("Required"),
	lastName: Yup.string().required("Required"),
	email: Yup.string().email("Invalid email").required("Required"),
});

const EditCustomerModal = ({ customer, onSubmit }: EditCustomerModalProps) => {
	const formik = useFormik<EditCustomerForm>({
		initialValues: {
			name: customer.name,
			lastName: customer.lastName,
			email: customer.email,
		},
		onSubmit: (values) => {
			onSubmit(values, customer.id);
		},
		validationSchema,
		enableReinitialize: true,
	});

	return (
		<div>
			<Paper sx={modalStyle}>
				<FormikProvider value={formik}>
					<TextField
						id="name"
						name="name"
						label="First Name"
						value={formik.values.name}
						onChange={formik.handleChange}
						error={formik.touched.name && Boolean(formik.errors.name)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.name && formik.errors.name}
						fullWidth
					/>

					<TextField
						id="lastName"
						name="lastName"
						label="Last Name"
						value={formik.values.lastName}
						onChange={formik.handleChange}
						error={formik.touched.lastName && Boolean(formik.errors.lastName)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.lastName && formik.errors.lastName}
						fullWidth
					/>

					<TextField
						id="email"
						name="email"
						label="Email"
						value={formik.values.email}
						onChange={formik.handleChange}
						error={formik.touched.email && Boolean(formik.errors.email)}
						onBlur={formik.handleBlur}
						helperText={formik.touched.email && formik.errors.email}
						fullWidth
					/>

					<Button
						variant="contained"
						color="primary"
						onClick={formik.submitForm}
						disabled={formik.isSubmitting}
						fullWidth
					>
						Save
					</Button>
				</FormikProvider>
			</Paper>
		</div>
	);
};

export default EditCustomerModal;
