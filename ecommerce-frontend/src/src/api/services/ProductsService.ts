import BrewtopiaApi from "../BrewtopiaApi";

const getProductById = (id: string) =>
  BrewtopiaApi.get<GetProductResponseDto>(`/products/${id}`);

const getProductsByCategory = (category: string) =>
  BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
    params: { category },
  });

const getAllProducts = () =>
  BrewtopiaApi.get<GetProductResponseDto[]>(`/products`);

const getProductsByPrice = (minPrice: number, maxPrice: number) =>
  BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
    params: { minPrice, maxPrice },
  });

const searchProducts = ({
  searchTerm,
  minPrice,
  maxPrice,
  category,
}: {
  searchTerm?: string;
  category?: string;
  minPrice?: number;
  maxPrice?: number;
}) =>
  BrewtopiaApi.get<GetProductResponseDto[]>(`/products`, {
    params: { query: searchTerm, category: category, minPrice, maxPrice },
  });

const getAllCategories = () =>
  BrewtopiaApi.get<GetCategoryResponseDto[]>(`/categories`);

const ProductsService = {
  getProductById,
  getProductsByCategory,
  getAllProducts,
  getProductsByPrice,
  searchProducts,
  getAllCategories,
};

export default ProductsService;
