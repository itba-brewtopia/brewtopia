import UpdateCartAction from "../../constants/UpdateCartAction";
import BrewtopiaApi from "../BrewtopiaApi";

const getCartByCustomerId = (customerId: string) =>
	BrewtopiaApi.get<GetCartResponseDto>(`/carts/${customerId}`);

const incrementCartItem = (customerId: string, productId: string) =>
	BrewtopiaApi.patch<GetCartResponseDto>(`/carts/${customerId}`, {
		productId,
		action: UpdateCartAction.INCREMENT,
	});

const decrementCartItem = (customerId: string, productId: string) =>
	BrewtopiaApi.patch<GetCartResponseDto>(`/carts/${customerId}`, {
		productId,
		action: UpdateCartAction.DECREMENT,
	});

const removeCartItem = (customerId: string, productId: string) =>
	BrewtopiaApi.patch<GetCartResponseDto>(`/carts/${customerId}`, {
		productId,
		action: UpdateCartAction.REMOVE,
	});

const getCartSizeByCustomerId = (customerId: string) =>
	BrewtopiaApi.get<number>(`/carts/${customerId}/size`);

const changeCartItem = (
	customerId: string,
	productId: string,
	quantity: number
) =>
	BrewtopiaApi.patch<GetCartResponseDto>(`/carts/${customerId}`, {
		productId,
		quantity,
		action: UpdateCartAction.CHANGE,
	});

const CartsService = {
	getCartByCustomerId,
	incrementCartItem,
	decrementCartItem,
	removeCartItem,
	changeCartItem,
	getCartSizeByCustomerId,
};

export default CartsService;
