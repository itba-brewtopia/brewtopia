import BrewtopiaApi from "../BrewtopiaApi";

const getOrdersByCustomerId = (customerId: string) =>
	BrewtopiaApi.get<GetOrderResponseDto[]>("/orders", {
		params: { customerId },
	});

const buyCart = (customerId: string, pickupStoreId: string) =>
	BrewtopiaApi.post<GetOrderResponseDto>(`/orders`, {
		customerId,
		pickupStoreId,
	});

const OrdersService = {
	getOrdersByCustomerId,
	buyCart,
};

export default OrdersService;
