import BrewtopiaApi from "../BrewtopiaApi";

const getStockAvailableDate = (storeId: string, items: CartItem[]) =>
	BrewtopiaApi.post<string>(`/stock/available`, {
		storeId,
		items,
	});

const StocksService = {
	getStockAvailableDate,
};

export default StocksService;
