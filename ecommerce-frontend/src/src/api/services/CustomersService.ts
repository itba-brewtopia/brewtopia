import BrewtopiaApi from "../BrewtopiaApi";

const getCustomersByEmail = (email: string) =>
	BrewtopiaApi.get<GetCustomerResponseDto[]>("/customers", {
		params: { email },
	});

const getCustomerById = (customerId: string) =>
	BrewtopiaApi.get<GetCustomerResponseDto>(`/customers/${customerId}`);

const createCustomer = (customer: CreateCustomerRequestDto) =>
	BrewtopiaApi.post<GetCustomerResponseDto>("/customers", customer);

const editCustomer = (customerId: string, customer: UpdateCustomerRequestDto) =>
	BrewtopiaApi.patch<GetCustomerResponseDto>(
		`/customers/${customerId}`,
		customer
	);

const CustomersService = {
	getCustomersByEmail,
	getCustomerById,
	createCustomer,
	editCustomer,
};

export default CustomersService;
