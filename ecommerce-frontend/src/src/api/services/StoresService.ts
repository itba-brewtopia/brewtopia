import BrewtopiaApi from "../BrewtopiaApi";

const getStoreById = (storeId: string) =>
	BrewtopiaApi.get<GetStoreResponseDto>("/stores", {
		params: { storeId },
	});

const getStores = () => BrewtopiaApi.get<GetStoreResponseDto[]>("/stores");

const StoresService = {
	getStoreById,
	getStores,
};

export default StoresService;
