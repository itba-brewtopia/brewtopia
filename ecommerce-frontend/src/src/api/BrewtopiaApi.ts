import axios from "axios";
import humps from "humps";

const BrewtopiaApi = axios.create({
	baseURL: process.env.NODE_ENV === "production" ? "/api" : "",
});

BrewtopiaApi.interceptors.request.use((config) => {
	if (config.data) {
		config.data = humps.decamelizeKeys(config.data);
	}

	if (config.params) {
		config.params = humps.decamelizeKeys(config.params);
	}

	return config;
});

BrewtopiaApi.interceptors.response.use((response) => {
	if (response.data) {
		response.data = humps.camelizeKeys(response.data);
	}

	return response;
});

export default BrewtopiaApi;
