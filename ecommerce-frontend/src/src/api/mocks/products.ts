import { faker } from "@faker-js/faker";

export const createRandomCategory = (): Category => ({
	id: faker.string.uuid(),
	name: faker.commerce.department(),
	image: faker.image.url(),
	isDeleted: true,
	createdAt: faker.date.past().toISOString(),
	modifiedAt: faker.date.past().toISOString(),
});

export const PRODUCT_CATEGORIES = Array.from(
	{ length: 10 },
	createRandomCategory
);

export const createRandomProduct = (): Product => ({
	id: faker.string.uuid(),
	name: faker.commerce.productName(),
	description: faker.commerce.productDescription(),
	price: faker.commerce.price({ min: 1, max: 100 }),
	images: Array.from({ length: 3 }, () => faker.image.url()),
	createdAt: faker.date.past().toISOString(),
	modifiedAt: faker.date.past().toISOString(),
	isDeleted: false,
	category: faker.helpers.arrayElement(PRODUCT_CATEGORIES),
});

export const PRODUCTS = Array.from({ length: 100 }, createRandomProduct);
