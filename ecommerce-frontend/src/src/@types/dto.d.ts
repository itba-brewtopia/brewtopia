type CreateCustomerRequestDto = {
	name: string;
	lastName: string;
	email: string;
	password: string;
};

type UpdateCustomerRequestDto = {
	name?: string;
	lastName?: string;
	email?: string;
	password?: string;
	isDeleted?: boolean;
	isVerified?: boolean;
};

type GetCustomerResponseDto = {
	id: string;
	name: string;
	lastName: string;
	email: string;
	isVerified: boolean;
	isDeleted: boolean;
	modifiedAt: string;
	createdAt: string;
	lastLogin: string;
};

type UpdateCartRequestDto = {
	productId: string;
	quantity?: number;
	action: import("../../constants/UpdateCartAction").default;
};

type CartItemDto = {
	productId: string;
	quantity: number;
};

type GetCartResponseDto = {
	customerId: string;
	modifiedAt: string;
	createdAt: string;
	isDeleted: boolean;
	items: CartItemDto[];
	totalPrice: string;
};

type OrderItemDto = {
	productId: string;
	quantity: number;
	price: string;
	image: string;
	name: string;
};

type GetOrderResponseDto = {
	id: string;
	customerId: string;
	createdAt: string;
	modifiedAt: string;
	totalPrice: string;
	pickupDate: string;
	pickupReadyDate: string;
	pickupStore: string;
	isDeleted: boolean;
	status: OrderStatus;
	items: OrderItemDto[];
};

type GetStoreResponseDto = {
	id: string;
	address: {
		street: string;
		number: string;
		postalCode: string;
		city: string;
		province: string;
	};
	alias: string;
	phone: string;
	openHours: {
		monday: string[];
		tuesday: string[];
		wednesday: string[];
		thursday: string[];
		friday: string[];
		saturday: string[];
		sunday: string[];
	};
	isDeleted: boolean;
	createdAt: string;
	modifiedAt: string;
};

type GetCategoryResponseDto = {
	id: string;
	name: string;
	image: string;
	isDeleted: boolean;
	createdAt: string;
	modifiedAt: string;
};

type GetProductResponseDto = {
	id: string;
	name: string;
	description: string;
	price: string;
	createdAt: string;
	modifiedAt: string;
	isDeleted: boolean;
	category: GetCategoryResponseDto;
	images: string[];
};
