type Category = {
	id: string;
	name: string;
	image: string;
	isDeleted: boolean;
	createdAt: string;
	modifiedAt: string;
};

type Product = {
	id: string;
	name: string;
	description: string;
	price: string;
	createdAt: string;
	modifiedAt: string;
	isDeleted: boolean;
	category: Category;
	images: string[];
};

type Customer = {
	id: string;
	name: string;
	lastName: string;
	email: string;
	isVerified: boolean;
	isDeleted: boolean;
	modifiedAt: string;
	createdAt: string;
	lastLogin: string;
};

type CartItem = {
	productId: string;
	quantity: number;
};

type Cart = {
	customerId: string;
	modifiedAt: string;
	createdAt: string;
	isDeleted: boolean;
	items: CartItem[];
	totalPrice: string;
};

type Address = {
	street: string;
	number: string;
	postalCode: string;
	city: string;
	province: string;
};

type OpenHours = {
	monday: string[];
	tuesday: string[];
	wednesday: string[];
	thursday: string[];
	friday: string[];
	saturday: string[];
	sunday: string[];
};

type Store = {
	id: string;
	address: Address;
	alias: string;
	phone: string;
	openHours: OpenHours;
	isDeleted: boolean;
	createdAt: string;
	modifiedAt: string;
};

type OrderItem = {
	productId: string;
	quantity: number;
	price: string;
	image: string;
	name: string;
};

type Order = {
	id: string;
	customerId: string;
	createdAt: string;
	modifiedAt: string;
	totalPrice: string;
	pickupDate: string;
	pickupReadyDate: string;
	pickupStore: string;
	isDeleted: boolean;
	status: OrderStatus;
	items: OrderItem[];
};

type FullCartItem = Product & { quantity: number };

type GlobalContextType = {
	customer: Customer | null;
	isLoading: boolean;
	authComplete: boolean;
	isLoggedIn: boolean;
	login: (customer: Customer) => void;
	setCustomer: (customer: Customer) => void;
	logout: () => void;
	cart: Cart | null;
	setCart: (cart: Cart) => void;
	categories: Category[] | null;
	stores: Store[] | null;
	globalError: string | null;
};
