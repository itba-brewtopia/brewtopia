import { VisibilityOff, Visibility } from "@mui/icons-material";
import {
	Button,
	FormControl,
	FormHelperText,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
	TextField,
	Typography,
} from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { useContext, useState } from "react";
import * as Yup from "yup";
import GlobalContext from "../../context/GlobalContext";
import CustomersService from "../../api/services/CustomersService";

type MyProfileForm = {
	firstName: string;
	lastName: string;
	email: string;
	password: string;
};

const validationSchema = Yup.object({
	firstName: Yup.string().required("First name is required"),
	lastName: Yup.string().required("Last name is required"),
	email: Yup.string()
		.email("Invalid email address")
		.required("Email is required"),
	password: Yup.string()
		.min(8, "Password must be at least 8 characters")
		.required("Password is required"),
});

const MyProfilePage = () => {
	const { customer, setCustomer } = useContext(
		GlobalContext
	) as GlobalContextType;
	const [showPassword, setShowPassword] = useState(false);

	const formik = useFormik<MyProfileForm>({
		initialValues: {
			firstName: customer?.name || "",
			lastName: customer?.lastName || "",
			email: customer?.email || "",
			password: "",
		},
		validationSchema,
		onSubmit: async (values) => {
			const res = await CustomersService.editCustomer(customer!.id, {
				name: values.firstName,
				lastName: values.lastName,
				email: values.email,
				password: values.password,
			});

			setCustomer(res.data);
		},
		validateOnMount: true,
	});

	const handleClickShowPassword = () => setShowPassword((show) => !show);

	const handleMouseDownPassword = (
		event: React.MouseEvent<HTMLButtonElement>
	) => {
		event.preventDefault();
	};

	return (
		<div
			className="flex flex-col items-center justify-center"
			style={{
				width: "50vw",
				margin: "auto",
			}}
		>
			<Typography variant="h3" component="h1" textAlign="center">
				My Profile
			</Typography>
			<br />
			<FormikProvider value={formik}>
				<TextField
					fullWidth
					id="firstName"
					name="firstName"
					label="First Name"
					value={formik.values.firstName}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					error={formik.touched.firstName && Boolean(formik.errors.firstName)}
					helperText={formik.touched.firstName && formik.errors.firstName}
				/>
				<br />

				<TextField
					fullWidth
					id="lastName"
					name="lastName"
					label="Last Name"
					value={formik.values.lastName}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					error={formik.touched.lastName && Boolean(formik.errors.lastName)}
					helperText={formik.touched.lastName && formik.errors.lastName}
				/>
				<br />

				<TextField
					fullWidth
					id="email"
					name="email"
					label="Email Address"
					value={formik.values.email}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					error={formik.touched.email && Boolean(formik.errors.email)}
					helperText={formik.touched.email && formik.errors.email}
				/>

				<br />

				<FormControl variant="outlined" className="w-full">
					<InputLabel htmlFor="outlined-adornment-password">
						Password
					</InputLabel>
					<OutlinedInput
						fullWidth
						name="password"
						error={formik.touched.password && Boolean(formik.errors.password)}
						id="outlined-adornment-password"
						type={showPassword ? "text" : "password"}
						value={formik.values.password}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						endAdornment={
							<InputAdornment position="end">
								<IconButton
									aria-label="toggle password visibility"
									onClick={handleClickShowPassword}
									onMouseDown={handleMouseDownPassword}
									edge="end"
								>
									{showPassword ? <VisibilityOff /> : <Visibility />}
								</IconButton>
							</InputAdornment>
						}
						label="Password"
					/>

					{formik.touched.password && formik.errors.password && (
						<FormHelperText error>{formik.errors.password}</FormHelperText>
					)}
				</FormControl>

				<br />

				<Button
					fullWidth
					variant="contained"
					color="secondary"
					onClick={formik.handleSubmit as any}
					disabled={formik.isSubmitting}
				>
					Save Changes
				</Button>
			</FormikProvider>
		</div>
	);
};

export default MyProfilePage;
