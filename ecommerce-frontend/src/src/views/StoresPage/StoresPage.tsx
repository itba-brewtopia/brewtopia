import { useContext } from "react";
import StoreCard from "../../components/StoreCard/StoreCard";
import GlobalContext from "../../context/GlobalContext";

const StoresPage = () => {
	const { stores } = useContext(GlobalContext) as GlobalContextType;

	return (
		<div>
			{stores?.map((store) => (
				<StoreCard store={store} key={store.id} />
			))}
		</div>
	);
};

export default StoresPage;
