import { VisibilityOff, Visibility } from "@mui/icons-material";
import {
	Button,
	FormControl,
	FormHelperText,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
	TextField,
	Typography,
} from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import { useContext, useState } from "react";
import * as Yup from "yup";
import CustomersService from "../../api/services/CustomersService";
import GlobalContext from "../../context/GlobalContext";
import { useNavigate } from "react-router-dom";
import routes from "../../router/routes";

type LoginForm = {
	email: string;
	password: string;
};

const validationSchema = Yup.object({
	email: Yup.string()
		.email("Invalid email address")
		.required("Email is required"),
	password: Yup.string().required("Password is required"),
});

const LoginPage = () => {
	const navigate = useNavigate();
	const { login } = useContext(GlobalContext) as GlobalContextType;
	const [showPassword, setShowPassword] = useState(false);

	const formik = useFormik<LoginForm>({
		initialValues: {
			email: "",
			password: "",
		},
		validationSchema,
		onSubmit: async (values) => {
			const res = await CustomersService.getCustomersByEmail(values.email);

			if (res.data.length === 0) {
				alert("Customer not found");
				return;
			}

			const customer = res.data[0];
			login(customer);

			navigate(routes.Home.path);
		},
		validateOnMount: true,
	});

	const handleClickShowPassword = () => setShowPassword((show) => !show);

	const handleMouseDownPassword = (
		event: React.MouseEvent<HTMLButtonElement>
	) => {
		event.preventDefault();
	};

	return (
		<div
			className="flex flex-col items-center justify-center"
			style={{
				width: "50vw",
				margin: "auto",
			}}
		>
			<Typography variant="h3" component="h1" textAlign="center">
				Login
			</Typography>
			<br />
			<FormikProvider value={formik}>
				<TextField
					fullWidth
					id="email"
					name="email"
					label="Email Address"
					value={formik.values.email}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					error={formik.touched.email && Boolean(formik.errors.email)}
					helperText={formik.touched.email && formik.errors.email}
				/>

				<br />

				<FormControl variant="outlined" className="w-full">
					<InputLabel htmlFor="outlined-adornment-password">
						Password
					</InputLabel>
					<OutlinedInput
						fullWidth
						name="password"
						error={formik.touched.password && Boolean(formik.errors.password)}
						id="outlined-adornment-password"
						type={showPassword ? "text" : "password"}
						value={formik.values.password}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						endAdornment={
							<InputAdornment position="end">
								<IconButton
									aria-label="toggle password visibility"
									onClick={handleClickShowPassword}
									onMouseDown={handleMouseDownPassword}
									edge="end"
								>
									{showPassword ? <VisibilityOff /> : <Visibility />}
								</IconButton>
							</InputAdornment>
						}
						label="Password"
					/>

					{formik.touched.password && formik.errors.password && (
						<FormHelperText error>{formik.errors.password}</FormHelperText>
					)}
				</FormControl>

				<br />

				<Button
					fullWidth
					variant="contained"
					color="secondary"
					onClick={formik.handleSubmit as any}
					disabled={formik.isSubmitting}
				>
					Login
				</Button>
			</FormikProvider>
		</div>
	);
};

export default LoginPage;
