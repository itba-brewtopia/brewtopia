import { useEffect, useState } from "react";
import ProductsService from "../../api/services/ProductsService";
import ProductCard from "../../components/ProductCard";
type Props = {};

const HomePage = (props: Props) => {
	const [products, setProducts] = useState<GetProductResponseDto[]>([]);

	useEffect(() => {
		ProductsService.getAllProducts().then((res) => setProducts(res.data));
	}, []);

	if (products.length === 0) {
		return <div>No products available</div>;
	}

	return (
		<div>
			{products.map((product) => {
				return <ProductCard product={product} key={product.id} />;
			})}
		</div>
	);
};

export default HomePage;
