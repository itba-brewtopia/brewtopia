import { CircularProgress, Typography } from "@mui/material";
import OrderCard from "../../components/OrderCard/OrderCard";
import React, { useContext } from "react";
import OrdersService from "../../api/services/OrdersService";
import GlobalContext from "../../context/GlobalContext";

const OrdersPage = () => {
	const { customer } = useContext(GlobalContext) as GlobalContextType;
	const [orders, setOrders] = React.useState<Order[] | null>(null);
	const [isLoading, setIsLoading] = React.useState<boolean>(false);

	React.useEffect(() => {
		setIsLoading(true);
		OrdersService.getOrdersByCustomerId(customer!.id)
			.then((res) => {
				setOrders(res.data);
			})
			.catch(console.error)
			.finally(() => {
				setIsLoading(false);
			});
	}, [customer]);

	if (isLoading || !orders) {
		return (
			<div className="h-screen w-screen flex items-center justify-center">
				<CircularProgress />
			</div>
		);
	}

	return (
		<div className="flex flex-col items-center justify-center">
			<Typography variant="h3" component="h1">
				My Orders
			</Typography>

			{orders.length === 0 && (
				<Typography variant="h5" component="h2">
					No orders found
				</Typography>
			)}

			{orders.map((order) => {
				return (
					<>
						<OrderCard key={order.id} order={order} />
						<br />
					</>
				);
			})}
		</div>
	);
};

export default OrdersPage;
