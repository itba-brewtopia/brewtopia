import { useNavigate, useParams } from "react-router-dom";
import Carousel from "react-material-ui-carousel";
import { Button, CircularProgress, Typography } from "@mui/material";
import GlobalContext from "../../context/GlobalContext";
import React from "react";
import ProductsService from "../../api/services/ProductsService";
import CartsService from "../../api/services/CartsService";
import routes from "../../router/routes";
import { useCallback } from "react";
import { printPrice } from "../../utils/price";

type Props = {};

const ProductPage = (props: Props) => {
	const navigate = useNavigate();

	const { isLoggedIn, customer, setCart } = React.useContext(
		GlobalContext
	) as GlobalContextType;
	const { id } = useParams<{ id: string }>();
	const [product, setProduct] = React.useState<Product | null>(null);
	const [isLoading, setIsLoading] = React.useState<boolean>(false);

	React.useEffect(() => {
		if (isLoading || product || !id) return;

		setIsLoading(true);
		ProductsService.getProductById(id)
			.then((res) => setProduct(res.data))
			.catch(console.error)
			.finally(() => setIsLoading(false));
	}, [id, isLoading, product]);

	const handleAddToCart = useCallback(async () => {
		if (!isLoggedIn || !product) return;

		const res = await CartsService.changeCartItem(customer!.id, product?.id, 1);

		setCart(res.data);
		navigate(routes.Cart.path);
	}, [isLoggedIn, product, customer, setCart, navigate]);

	if (isLoading || !product)
		return (
			<div className="flex h-screen w-screen justify-center items-center">
				<CircularProgress />
			</div>
		);

	return (
		<div>
			<Carousel autoPlay={false}>
				{product.images.map((image) => (
					<img
						src={image}
						style={{
							objectFit: "contain",
							width: "100%",
							height: "20rem",
						}}
						alt=""
					/>
				))}
			</Carousel>
			<Typography variant="body1" component="p" style={{ textAlign: "center" }}>
				{product.category.name}
			</Typography>

			<Typography variant="h4" component="h1" style={{ textAlign: "center" }}>
				{product.name}
			</Typography>

			<Typography variant="h5" component="h2" style={{ textAlign: "center" }}>
				{printPrice(product.price)}
			</Typography>

			<Typography variant="body1" component="p" style={{ textAlign: "center" }}>
				{product.description}
			</Typography>

			{/* TODO: add real add to cart logic */}
			<Button
				variant="contained"
				color="primary"
				style={{ display: "block", margin: "auto" }}
				disabled={!isLoggedIn}
				onClick={handleAddToCart}
			>
				{isLoggedIn ? "Add to cart" : "Log in to add to cart"}
			</Button>
		</div>
	);
};

export default ProductPage;
