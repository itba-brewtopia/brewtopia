import {
	Button,
	CircularProgress,
	List,
	MenuItem,
	TextField,
	Typography,
} from "@mui/material";
import CartRow from "../../components/CartRow/CartRow";
import { FormikProvider, useFormik } from "formik";
import * as Yup from "yup";
import React, { useCallback, useContext, useEffect } from "react";
import CartsService from "../../api/services/CartsService";
import GlobalContext from "../../context/GlobalContext";
import ProductsService from "../../api/services/ProductsService";
import StocksService from "../../api/services/StocksService";
import OrdersService from "../../api/services/OrdersService";
import { useNavigate } from "react-router-dom";
import routes from "../../router/routes";
import { printPrice } from "../../utils/price";

type CartForm = {
	pickupStoreId: string;
};

const validationSchema = Yup.object({
	pickupStoreId: Yup.string().required("Required"),
});

const CartPage = () => {
	const navigate = useNavigate();

	const { cart, stores, setCart, customer } = useContext(
		GlobalContext
	) as GlobalContextType;
	const [cartProducts, setCartProducts] = React.useState<Product[] | null>(
		null
	);
	const [pickupReadyDate, setPickupReadyDate] = React.useState<string | null>(
		null
	);
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const fetched = React.useRef<boolean>(false);

	const formik = useFormik<CartForm>({
		initialValues: {
			pickupStoreId: "",
		},
		onSubmit: async (values) => {
			// TODO: Connect to backend
			console.log(values);
		},
		validationSchema,
		validateOnMount: true,
	});

	const handleCartItemIncrease = useCallback(
		async (cartItem: FullCartItem) => {
			const res = await CartsService.incrementCartItem(
				customer!.id,
				cartItem.id
			);
			setCart(res.data);
		},
		[customer, setCart]
	);

	const handleCartItemDecrease = useCallback(
		async (cartItem: FullCartItem) => {
			const res = await CartsService.decrementCartItem(
				customer!.id,
				cartItem.id
			);
			setCart(res.data);
		},
		[customer, setCart]
	);

	const handleCartItemRemove = useCallback(
		async (cartItem: FullCartItem) => {
			const res = await CartsService.removeCartItem(customer!.id, cartItem.id);
			setCart(res.data);
		},
		[customer, setCart]
	);

	const handleCartItemChangeQuantity = useCallback(
		async (cartItem: FullCartItem, quantity: number) => {
			const res = await CartsService.changeCartItem(
				customer!.id,
				cartItem.id,
				quantity
			);
			setCart(res.data);
		},
		[customer, setCart]
	);

	const fetchCartAndItems = useCallback(async () => {
		const cartRes = await CartsService.getCartByCustomerId(customer!.id);
		const fetchedCart = cartRes.data;
		setCart(fetchedCart);

		const cartItems = fetchedCart.items.map((item) =>
			ProductsService.getProductById(item.productId)
		);

		const cartItemsRes = await Promise.all(cartItems);

		setCartProducts(
			cartItemsRes.map((item, i) => ({
				...item.data,
				quantity: fetchedCart.items[i].quantity,
			}))
		);
	}, [customer, setCart]);

	const handleBuy = useCallback(async () => {
		if (!formik.isValid) return;

		const res = await OrdersService.buyCart(
			customer!.id,
			formik.values.pickupStoreId
		);
		setCart(res.data);

		navigate(routes.Orders.path);
	}, [customer, formik, setCart, navigate]);

	useEffect(() => {
		if (fetched.current) return;

		fetched.current = true;

		setIsLoading(true);
		fetchCartAndItems()
			.catch(console.error)
			.finally(() => setIsLoading(false));
	}, [cart, cartProducts, fetchCartAndItems]);

	useEffect(() => {
		if (!cart || !cart.items.length || !formik.values.pickupStoreId) return;

		StocksService.getStockAvailableDate(
			formik.values.pickupStoreId,
			cart.items
		).then((res) => {
			setPickupReadyDate(res.data);
		});
	}, [cart, formik.values.pickupStoreId]);

	if (isLoading || !cart || !cartProducts || !stores)
		return (
			<div className="h-screen w-screen flex justify-center items-center">
				<CircularProgress />
			</div>
		);

	if (cart.items.length === 0)
		return (
			<div className="h-screen w-screen flex justify-center items-center">
				<Typography variant="h3" component="h1">
					No items in cart
				</Typography>
			</div>
		);

	return (
		<div>
			<Typography
				variant="h3"
				component="h1"
				sx={{ textAlign: "center", marginTop: 2 }}
			>
				Cart
			</Typography>

			<List sx={{ width: "100%", bgcolor: "background.paper" }}>
				{cart.items.map((item) => (
					<CartRow
						product={cartProducts.find((p) => p.id === item.productId)!}
						quantity={item.quantity}
						key={item.productId}
						onIncrease={handleCartItemIncrease}
						onDecrease={handleCartItemDecrease}
						onRemove={handleCartItemRemove}
						onChangeQuantity={handleCartItemChangeQuantity}
					/>
				))}
			</List>

			<Typography
				variant="h4"
				component="h2"
				sx={{ textAlign: "center", marginTop: 2 }}
			>
				Total: {printPrice(cart.totalPrice)}
			</Typography>

			<FormikProvider value={formik}>
				<TextField
					select
					name="pickupStoreId"
					id="pickupStoreId"
					label="Pickup store"
					value={formik.values.pickupStoreId}
					onChange={formik.handleChange}
					error={
						formik.touched.pickupStoreId && Boolean(formik.errors.pickupStoreId)
					}
					helperText={
						formik.touched.pickupStoreId && formik.errors.pickupStoreId
					}
					sx={{
						width: "60vw",
					}}
				>
					{stores?.map((store) => (
						<MenuItem key={store.id} value={store.id}>
							{store.alias}
						</MenuItem>
					))}
				</TextField>

				{pickupReadyDate && (
					<Typography
						variant="h5"
						component="h3"
						sx={{ textAlign: "center", marginTop: 2 }}
					>
						Pickup ready date:{" "}
						{
							// if pickupReadyDate is today, show 'Today', else show date
							new Date(pickupReadyDate)?.toLocaleDateString() ===
							new Date()?.toLocaleDateString()
								? "Today"
								: new Date(pickupReadyDate)?.toLocaleString()
						}
					</Typography>
				)}

				<Button
					variant="contained"
					sx={{ marginTop: 2 }}
					disabled={formik.isSubmitting || !formik.isValid}
					onClick={handleBuy}
				>
					Buy
				</Button>
			</FormikProvider>
		</div>
	);
};

export default CartPage;
