import { FilterAlt, Search } from "@mui/icons-material";
import { Button, Container, MenuItem, TextField } from "@mui/material";
import { FormikProvider, useFormik } from "formik";
import * as Yup from "yup";
import { useContext, useState } from "react";
import GlobalContext from "../../context/GlobalContext";
import ProductsService from "../../api/services/ProductsService";
import ProductCard from "../../components/ProductCard";

type SearchFormValues = {
  search: string | undefined;
  minPrice: number | undefined;
  maxPrice: number | undefined;
  categoryId: string | undefined;
};

const validationSchema = Yup.object({
  search: Yup.string().nullable(),
  minPrice: Yup.number().min(0, "Must be greater than 0"),
  maxPrice: Yup.number().min(0, "Must be greater than 0"),
  categoryId: Yup.string().nullable(),
});

const SearchPage = () => {
  const { categories } = useContext(GlobalContext) as GlobalContextType;
  const [products, setProducts] = useState<GetProductResponseDto[]>([]);
  const formik = useFormik<SearchFormValues>({
    initialValues: {
      search: undefined,
      minPrice: undefined,
      maxPrice: undefined,
      categoryId: undefined,
    },
    validationSchema,
    onSubmit: (values) => {
      console.log(values);
      ProductsService.searchProducts({
        searchTerm: values.search ? values.search : undefined,
        minPrice: values.minPrice ? values.minPrice : undefined,
        maxPrice: values.maxPrice ? values.maxPrice : undefined,
        category: values.categoryId ? values.categoryId : undefined,
      }).then((res) => setProducts(res.data));
    },
  });

  return (
    <Container className="mt-3">
      <FormikProvider value={formik}>
        <TextField
          fullWidth
          id="search"
          name="search"
          placeholder="Search by product name"
          label="Search"
          value={formik.values.search}
          onChange={formik.handleChange}
          error={formik.touched.search && Boolean(formik.errors.search)}
          helperText={formik.touched.search && formik.errors.search}
          InputProps={{
            startAdornment: <Search />,
          }}
        />
        <div className="mb-3" />

        <TextField
          id="minPrice"
          name="minPrice"
          placeholder="Min price"
          label="Min price"
          type="number"
          value={formik.values.minPrice}
          onChange={formik.handleChange}
          error={formik.touched.minPrice && Boolean(formik.errors.minPrice)}
          helperText={formik.touched.minPrice && formik.errors.minPrice}
        />
        <TextField
          id="maxPrice"
          name="maxPrice"
          placeholder="Max price"
          label="Max price"
          type="number"
          value={formik.values.maxPrice}
          onChange={formik.handleChange}
          error={formik.touched.maxPrice && Boolean(formik.errors.maxPrice)}
          helperText={formik.touched.maxPrice && formik.errors.maxPrice}
        />

        <TextField
          sx={{
            minWidth: 200,
          }}
          select
          label="Filter by category"
          placeholder="Filter by category"
          defaultValue="EUR"
          InputProps={{
            startAdornment: <FilterAlt />,
          }}
          value={formik.values.categoryId}
          onChange={(e) =>
            formik.setFieldValue(
              "categoryId",
              e.target.value ? e.target.value : ""
            )
          }
        >
          {categories?.map((option: Category) => (
            <MenuItem key={option.id} value={option.id}>
              {option.name}
            </MenuItem>
          ))}
        </TextField>

        <div className="mb-3" />

        <Button
          variant="contained"
          color="primary"
          onClick={formik.handleSubmit as any}
        >
          Search
        </Button>
      </FormikProvider>
      <div>
        {products.map((product) => {
          return <ProductCard product={product} key={product.id} />;
        })}
      </div>
    </Container>
  );
};

export default SearchPage;
