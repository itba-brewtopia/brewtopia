import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import routes from "./routes";
import HomePage from "../views/HomePage";
import LoginPage from "../views/LoginPage";
import CartPage from "../views/CartPage";
import OrdersPage from "../views/OrdersPage";
import ProductPage from "../views/ProductPage";
import ProfilePage from "../views/ProfilePage";
import RegisterPage from "../views/RegisterPage";
import SearchPage from "../views/SearchPage";
import StoresPage from "../views/StoresPage";
import Navbar from "../components/Navbar";
import React from "react";
import GlobalContext from "../context/GlobalContext";
const Router = () => {
	const { isLoggedIn } = React.useContext(GlobalContext) as GlobalContextType;
	return (
		<BrowserRouter>
			<Navbar />
			<Routes>
				<Route path={routes.Home.path} element={<HomePage />} />
				{!isLoggedIn && (
					<>
						<Route path={routes.Login.path} element={<LoginPage />} />
						<Route path={routes.Register.path} element={<RegisterPage />} />
					</>
				)}
				<Route path={routes.Product.path} element={<ProductPage />} />
				{isLoggedIn && (
					<>
						<Route path={routes.Cart.path} element={<CartPage />} />
						<Route path={routes.Orders.path} element={<OrdersPage />} />
						<Route path={routes.Profile.path} element={<ProfilePage />} />
					</>
				)}
				<Route path={routes.Search.path} element={<SearchPage />} />
				<Route path={routes.Stores.path} element={<StoresPage />} />
				<Route path="*" element={<Navigate to="/" replace />} />
			</Routes>
		</BrowserRouter>
	);
};

export default Router;
