const routes = {
	Home: {
		path: "/",
		name: "Home",
	},
	Login: {
		path: "/login",
		name: "Login",
	},
	Register: {
		path: "/register",
		name: "Register",
	},
	Product: {
		path: "/products/:id",
		name: "Product",
	},
	Cart: {
		path: "/cart",
		name: "Cart",
	},
	Orders: {
		path: "/orders",
		name: "Orders",
	},
	Profile: {
		path: "/profile",
		name: "Profile",
	},
	Search: {
		path: "/search",
		name: "Search",
	},
	Stores: {
		path: "/stores",
		name: "Stores",
	},
};

export default routes;
