import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { Coffee, Person, ShoppingCart } from "@mui/icons-material";
import { Link, useNavigate } from "react-router-dom";
import routes from "../../router/routes";
import { Badge } from "@mui/material";
import GlobalContext from "../../context/GlobalContext";
import CartsService from "../../api/services/CartsService";
import { useCallback } from "react";

const generalLinks = [routes.Search, routes.Stores];

const loggedInUserLinks = [routes.Profile, routes.Orders];

const loggedOutUserLinks = [routes.Login, routes.Register];

function Navbar() {
	const navigate = useNavigate();
	const { isLoggedIn, logout, cart, customer } = React.useContext(
		GlobalContext
	) as GlobalContextType;
	const [cartSize, setCartSize] = React.useState<number | null>(null);

	const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
		null
	);
	const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
		null
	);

	const loggedInUserActions = [
		{
			name: "Log out",
			onClick: () => {
				logout();
				handleCloseUserMenu();
				navigate(routes.Home.path);
			},
		},
	];

	const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorElNav(event.currentTarget);
	};
	const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorElUser(event.currentTarget);
	};

	const handleCloseNavMenu = () => {
		setAnchorElNav(null);
	};

	const handleCloseUserMenu = () => {
		setAnchorElUser(null);
	};

	const fetchCartSize = useCallback(async () => {
		const res = await CartsService.getCartSizeByCustomerId(customer!.id);
		setCartSize(res.data);
	}, [customer]);

	React.useEffect(() => {
		if (!isLoggedIn) return;

		if (cart) {
			setCartSize(cart?.items?.length);
			return;
		}

		fetchCartSize();
	}, [cart, isLoggedIn, fetchCartSize]);

	return (
		<AppBar position="static">
			<Container maxWidth="xl">
				<Toolbar disableGutters>
					<Coffee sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
					<Typography
						variant="h6"
						noWrap
						component={Link}
						to={routes.Home.path}
						sx={{
							mr: 2,
							display: { xs: "none", md: "flex" },
							fontFamily: "monospace",
							fontWeight: 700,
							letterSpacing: ".3rem",
							color: "inherit",
							textDecoration: "none",
						}}
					>
						Brewtopia
					</Typography>

					<Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
						<IconButton
							size="large"
							aria-label="account of current user"
							aria-controls="menu-appbar"
							aria-haspopup="true"
							onClick={handleOpenNavMenu}
							color="inherit"
						>
							<MenuIcon />
						</IconButton>
						<Menu
							id="menu-appbar"
							anchorEl={anchorElNav}
							anchorOrigin={{
								vertical: "bottom",
								horizontal: "left",
							}}
							keepMounted
							transformOrigin={{
								vertical: "top",
								horizontal: "left",
							}}
							open={Boolean(anchorElNav)}
							onClose={handleCloseNavMenu}
							sx={{
								display: { xs: "block", md: "none" },
							}}
						>
							{generalLinks.map((link) => (
								<MenuItem key={link.path} onClick={handleCloseNavMenu}>
									<Typography
										textAlign="center"
										component={Link}
										to={link.path}
									>
										{link.name}
									</Typography>
								</MenuItem>
							))}
						</Menu>
					</Box>
					<Coffee sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} />
					<Typography
						variant="h5"
						noWrap
						component={Link}
						to={routes.Home.path}
						sx={{
							mr: 2,
							display: { xs: "flex", md: "none" },
							flexGrow: 1,
							fontFamily: "monospace",
							fontWeight: 700,
							letterSpacing: ".3rem",
							color: "inherit",
							textDecoration: "none",
						}}
					>
						Brewtopia
					</Typography>
					<Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
						{generalLinks.map((link) => (
							<Button
								key={link.path}
								onClick={handleCloseNavMenu}
								component={Link}
								to={link.path}
								sx={{ my: 2, color: "white", display: "block" }}
							>
								{link.name}
							</Button>
						))}
					</Box>

					{isLoggedIn && (
						<Box sx={{ flexGrow: 0 }} className="me-4">
							<Tooltip title="View Cart">
								<IconButton
									sx={{ p: 0 }}
									component={Link}
									to={routes.Cart.path}
								>
									{/* TODO: REPLACE MOCKED BADGE CONTENT WITH REAL SHOPPING CART SIZE */}
									<Badge
										badgeContent={cartSize ? cartSize : null}
										color="error"
									>
										<ShoppingCart />
									</Badge>
								</IconButton>
							</Tooltip>
						</Box>
					)}

					<Box sx={{ flexGrow: 0 }}>
						<Tooltip title="User Links">
							<IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
								<Avatar alt="User">
									<Person />
								</Avatar>
							</IconButton>
						</Tooltip>
						<Menu
							sx={{ mt: "45px" }}
							id="menu-appbar"
							anchorEl={anchorElUser}
							anchorOrigin={{
								vertical: "top",
								horizontal: "right",
							}}
							keepMounted
							transformOrigin={{
								vertical: "top",
								horizontal: "right",
							}}
							open={Boolean(anchorElUser)}
							onClose={handleCloseUserMenu}
						>
							{isLoggedIn
								? [
										...loggedInUserLinks.map((link) => (
											<MenuItem key={link.path} onClick={handleCloseUserMenu}>
												<Typography
													textAlign="center"
													component={Link}
													to={link.path}
												>
													{link.name}
												</Typography>
											</MenuItem>
										)),

										...loggedInUserActions.map((action) => (
											<MenuItem key={action.name} onClick={handleCloseUserMenu}>
												<Typography textAlign="center" onClick={action.onClick}>
													{action.name}
												</Typography>
											</MenuItem>
										)),
								  ]
								: loggedOutUserLinks.map((link) => (
										<MenuItem key={link.path} onClick={handleCloseUserMenu}>
											<Typography
												textAlign="center"
												component={Link}
												to={link.path}
											>
												{link.name}
											</Typography>
										</MenuItem>
								  ))}
						</Menu>
					</Box>
				</Toolbar>
			</Container>
		</AppBar>
	);
}
export default Navbar;
