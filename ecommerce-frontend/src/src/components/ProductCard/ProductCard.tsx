import {
	Card,
	CardActionArea,
	CardContent,
	CardMedia,
	Typography,
} from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { printPrice } from "../../utils/price";

type ProductCardProps = {
	product: Product;
};

const ProductCard = ({ product }: ProductCardProps) => {
	return (
		<Card
			sx={{
				maxWidth: 345,
				margin: "auto",
				marginTop: 2,
				marginBottom: 2,
			}}
		>
			<CardActionArea component={Link} to={`/products/${product.id}`}>
				<CardMedia
					component="img"
					style={{
						height: 140,
					}}
					image={product.images[0]}
					title={product.name}
				/>
				<CardContent>
					<Typography gutterBottom variant="h5" component="div">
						{product.name}
					</Typography>

					{/* <Typography variant="body2" color="text.secondary">
                        {product.description}
                    </Typography> */}

					<Typography variant="body2" color="text.secondary">
						{printPrice(product.price)}
					</Typography>
				</CardContent>
			</CardActionArea>
		</Card>
	);
};

export default ProductCard;
