import { Receipt } from "@mui/icons-material";
import { printPrice } from "../../utils/price";
import {
	Card,
	CardContent,
	Chip,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
} from "@mui/material";

type OrderCardProps = {
	order: Order;
};

const OrderCard = ({ order }: OrderCardProps) => {
	return (
		<Card
			sx={{
				maxWidth: 600,
			}}
		>
			<CardContent>
				<Typography variant="h6" textAlign="left">
					<Receipt /> Order {order.id}
				</Typography>
				<div
					style={{
						textAlign: "left",
					}}
				>
					Status: <Chip label={order.status} />
				</div>

				<div
					style={{
						textAlign: "left",
					}}
				>
					Created: {new Date(order.createdAt).toLocaleString()}
				</div>
				<Typography variant="h6">Items</Typography>
				<TableContainer component={Paper}>
					<Table aria-label="order item list">
						<TableHead>
							<TableRow>
								<TableCell>Product ID</TableCell>
								<TableCell>Product</TableCell>
								<TableCell align="right">Quantity</TableCell>
								<TableCell align="right">Price per unit</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{order.items.map((item) => (
								<TableRow
									key={item.productId}
									sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
								>
									<TableCell component="th" scope="row">
										{item.productId}
									</TableCell>
									<TableCell align="right">{item.name}</TableCell>
									<TableCell align="right">{item.quantity}</TableCell>
									<TableCell align="right">{printPrice(item.price)}</TableCell>
								</TableRow>
							))}
							<TableRow>
								<TableCell colSpan={3}>Total</TableCell>
								<TableCell align="right">
									{printPrice(order.totalPrice)}
								</TableCell>
							</TableRow>
						</TableBody>
					</Table>
				</TableContainer>
			</CardContent>
		</Card>
	);
};

export default OrderCard;
