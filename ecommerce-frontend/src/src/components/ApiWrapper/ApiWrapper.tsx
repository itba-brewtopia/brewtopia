import React from "react";
import GlobalContext from "../../context/GlobalContext";
import { CircularProgress } from "@mui/material";

type Props = {
	children: React.ReactNode;
};

const ApiWrapper = ({ children }: Props) => {
	const { isLoading, authComplete, globalError } = React.useContext(
		GlobalContext
	) as GlobalContextType;

	if (globalError) {
		return (
			<div className="flex items-center justify-center w-screen h-screen">
				<h1>{globalError}</h1>
			</div>
		);
	}

	if (isLoading || !authComplete) {
		return (
			<div className="flex items-center justify-center w-screen h-screen">
				<CircularProgress />
			</div>
		);
	}

	return <div>{children}</div>;
};

export default ApiWrapper;
