import { Card, CardContent, Typography } from "@mui/material";

type StoreCardProps = {
	store: Store;
};

const StoreCard = ({ store }: StoreCardProps) => {
	return (
		<Card
			sx={{
				maxWidth: 345,
				margin: "auto",
				marginTop: 2,
				marginBottom: 2,
			}}
		>
			<CardContent>
				<Typography gutterBottom variant="h5" component="div">
					{store.alias}
				</Typography>

				{/* <Typography variant="body2" color="text.secondary">
                        {product.description}
                    </Typography> */}

				<Typography variant="body2" color="text.secondary">
					{store.address.street + " " + store.address.number},{" "}
					{store.address.city}, {store.address.province},{" "}
					{store.address.postalCode}
				</Typography>

				<Typography variant="body2" color="text.secondary">
					{store.phone}
				</Typography>
			</CardContent>
		</Card>
	);
};

export default StoreCard;
