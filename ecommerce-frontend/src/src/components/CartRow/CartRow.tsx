import { Remove, Add, Delete } from "@mui/icons-material";
import {
	ListItem,
	TextField,
	IconButton,
	ListItemAvatar,
	Avatar,
	ListItemText,
} from "@mui/material";
import { useFormik } from "formik";
import React, { useEffect, useMemo } from "react";
import * as Yup from "yup";
import { printPrice } from "../../utils/price";

type CartRowProps = {
	quantity: number;
	product: Product;
	onRemove: (cartItem: FullCartItem) => Promise<void>;
	onIncrease: (cartItem: FullCartItem) => Promise<void>;
	onDecrease: (cartItem: FullCartItem) => Promise<void>;
	onChangeQuantity: (cartItem: FullCartItem, quantity: number) => Promise<void>;
};

type CartRowFormValues = {
	quantity: number;
};

const validationSchema = Yup.object({
	quantity: Yup.number()
		.min(1, "Quantity must be greater than 0")
		.required("Quantity is required"),
});

const CartRow = ({
	quantity,
	product,
	onRemove,
	onIncrease,
	onDecrease,
	onChangeQuantity,
}: CartRowProps) => {
	const formik = useFormik<CartRowFormValues>({
		initialValues: {
			quantity,
		},
		validationSchema,
		onSubmit: (values) => {},
		enableReinitialize: true,
	});

	const fullCartItem = useMemo(
		() => ({
			...product,
			quantity: formik.values.quantity,
		}),
		[product, formik.values.quantity]
	);

	useEffect(() => {
		// TODO: UPDATE QUANTITY WTIH API
		if (
			formik.values.quantity !== quantity &&
			formik.isValid &&
			formik.dirty &&
			!formik.isSubmitting &&
			formik.values.quantity > 0
		) {
			onChangeQuantity(fullCartItem, formik.values.quantity);
		}
	}, [
		formik.values.quantity,
		onChangeQuantity,
		product,
		formik.isValid,
		formik.dirty,
		formik.isSubmitting,
		quantity,
		fullCartItem,
	]);
	return (
		<ListItem
			secondaryAction={
				<div>
					<TextField
						label="Quantity"
						name="quantity"
						type="number"
						InputLabelProps={{
							shrink: true,
						}}
						variant="outlined"
						value={formik.values.quantity}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						error={formik.touched.quantity && Boolean(formik.errors.quantity)}
					/>
					<IconButton
						edge="end"
						aria-label="decrement"
						disabled={formik.values.quantity === 1}
						onClick={() => onDecrease(fullCartItem)}
					>
						<Remove />
					</IconButton>
					<IconButton
						edge="end"
						aria-label="increment"
						onClick={() => onIncrease(fullCartItem)}
					>
						<Add />
					</IconButton>
					<IconButton
						edge="end"
						aria-label="delete"
						onClick={() => onRemove(fullCartItem)}
					>
						<Delete />
					</IconButton>
				</div>
			}
		>
			<ListItemAvatar>
				<Avatar
					alt={product.name}
					src={product.images[0]}
					sx={{ width: "5rem", height: "5rem" }}
				/>
			</ListItemAvatar>
			<ListItemText
				primary={product.name}
				secondary={`${printPrice(product.price)} each`}
			/>
		</ListItem>
	);
};

export default CartRow;
