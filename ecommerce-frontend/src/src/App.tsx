import { ThemeProvider } from "@emotion/react";
import "./App.css";
import Router from "./router/Router";
import theme from "./theme";
import { GlobalContextProvider } from "./context/GlobalContext";
import ApiWrapper from "./components/ApiWrapper";

function App() {
	return (
		<div className="App">
			<ThemeProvider theme={theme}>
				<GlobalContextProvider>
					<ApiWrapper>
						<Router />
					</ApiWrapper>
				</GlobalContextProvider>
			</ThemeProvider>
		</div>
	);
}

export default App;
