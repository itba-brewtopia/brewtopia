import { createTheme } from "@mui/material/styles";

const theme = createTheme({
	palette: {
		primary: {
			main: "#B8621B",
		},
		secondary: {
			main: "#262A56",
		},
	},
});

export default theme;
