import { createContext, useCallback, useEffect, useState } from "react";
import CustomersService from "../api/services/CustomersService";
import ProductsService from "../api/services/ProductsService";
import StoresService from "../api/services/StoresService";

const GlobalContext = createContext<GlobalContextType | null>(null);

export default GlobalContext;

export const GlobalContextProvider = ({
	children,
}: {
	children: React.ReactNode;
}) => {
	const [categories, setCategories] = useState<Category[] | null>(null);
	const [cart, setCart] = useState<Cart | null>(null);
	const [stores, setStores] = useState<Store[] | null>(null);
	const [customer, setCustomer] = useState<Customer | null>(null);
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const [authComplete, setAuthComplete] = useState<boolean>(false);
	const [globalError, setGlobalError] = useState<string | null>(null);
	const customerId = window.localStorage.getItem("customerId");

	const isLoggedIn = authComplete && !!customer;

	const fetchCustomer = useCallback(async () => {
		if (!customerId) return;

		if (customerId && !customer && !isLoading && !authComplete) {
			setIsLoading(true);
			try {
				const res = await CustomersService.getCustomerById(customerId);

				setCustomer(res.data);
			} catch (e: any) {
				window.localStorage.removeItem("customerId");
				throw e;
			} finally {
				setIsLoading(false);
			}
		}
	}, [customerId, customer, isLoading, authComplete]);

	const login = useCallback(async (customer: Customer) => {
		window.localStorage.setItem("customerId", customer.id);

		setCustomer(customer);
	}, []);

	const fetchCategories = async () => {
		const categoriesRes = await ProductsService.getAllCategories();
		setCategories(categoriesRes.data);
	};

	const fetchStores = async () => {
		const storesRes = await StoresService.getStores();
		setStores(storesRes.data);
	};

	const logout = useCallback(async () => {
		window.localStorage.removeItem("customerId");

		setCustomer(null);
	}, []);

	const fetchAll = useCallback(async () => {
		await fetchCategories();
		await fetchStores();
	}, []);

	useEffect(() => {
		fetchCustomer()
			.then(async () => {
				await fetchAll();
				setAuthComplete(true);
			})
			.catch((e) => {
				setGlobalError(e.message);
			});
	}, [fetchCustomer, fetchAll]);

	return (
		<GlobalContext.Provider
			value={{
				customer,
				categories,
				authComplete,
				isLoading,
				isLoggedIn,
				login,
				logout,
				setCustomer,
				setCart,
				cart,
				stores,
				globalError,
			}}
		>
			{children}
		</GlobalContext.Provider>
	);
};
