enum UpdateCartAction {
	INCREMENT = "increment",
	DECREMENT = "decrement",
	REMOVE = "remove",
	CHANGE = "change",
}

export default UpdateCartAction;
