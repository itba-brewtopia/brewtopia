#!/bin/bash -e 

echo "Running tests for service"
## pytest
python -m pytest "./tests" --junitxml=report.xml

echo "Running coverage command for service"
## Coverage
python -m pytest "./tests" -p no:warnings --cov="./" --cov-report html
