import uuid
import unittest
from unittest.mock import MagicMock, patch

from models.shipment_status import ShipmentStatus
from services.shipments_service import ShipmentsService
from api.dto.shipment_full_dto import ShipmentFullDto
from api.dto.item_full_dto import ItemFullDto
from tests.mocks.db_mocks import mock_db_shipments


class TestShipmentsService(unittest.TestCase):

    @patch('persistence.shipments_persistence.ShipmentsPersistence.get_instance', return_value=MagicMock())
    def test_get_shipments(self, mock_get_instance):
        mock_get_instance.return_value.get_shipments.return_value = mock_db_shipments
        expected = [ShipmentFullDto.from_model(
            shipment) for shipment in mock_db_shipments]

        shipments = ShipmentsService().get_instance().get_shipments_with_items()

        self.assertEqual(len(shipments), len(expected))
        self.assertEqual(shipments, expected)

    @patch('persistence.shipments_persistence.ShipmentsPersistence.get_instance', return_value=MagicMock())
    def test_get_shipment_by_id(self, mock_get_instance):
        mock_get_instance.return_value.get_shipment_by_id.return_value = mock_db_shipments[0]
        expected = ShipmentFullDto.from_model(mock_db_shipments[0])

        shipments = ShipmentsService().get_instance().get_shipment_with_items_by_id(
            mock_db_shipments[0].id)

        self.assertIsNotNone(shipments)
        self.assertEqual(shipments, expected)

    @patch('requests.get', return_value=MagicMock(status_code=200))
    @patch('persistence.shipments_persistence.ShipmentsPersistence.get_instance', return_value=MagicMock())
    def test_create_shipment(self, mock_get_instance, mock_requests):
        mock_get_instance.return_value.create_shipment.return_value = mock_db_shipments[0]
        expected = ShipmentFullDto.from_model(mock_db_shipments[0])

        shipments = ShipmentsService().get_instance().create_shipment(
            destination_store_id="2"*24,
            source_store_id="3"*24,
            estimated_delivery_date="2020-01-01",
            estimated_ship_date="2020-01-01",
            items=[ItemFullDto.from_model(item)
                   for item in mock_db_shipments[0].items]
        )

        self.assertIsNotNone(shipments)
        self.assertEqual(shipments, expected)

    @patch('requests.patch', return_value=MagicMock(status_code=200))
    @patch('persistence.shipments_persistence.ShipmentsPersistence.get_instance', return_value=MagicMock())
    def test_update_shipment(self, mock_get_instance, mock_requests):
        shipment = mock_db_shipments[0]
        shipment.status = ShipmentStatus.DELIVERED.value
        mock_get_instance.return_value.update_shipment.return_value = shipment
        expected = ShipmentFullDto.from_model(shipment)

        shipment_with_items = ShipmentsService().get_instance().update_shipment(
            shipment_id=shipment.id,
            status=ShipmentStatus.DELIVERED.value,
        )

        self.assertIsNotNone(shipment_with_items)
        self.assertEqual(shipment_with_items, expected)
