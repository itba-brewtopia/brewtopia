import unittest
from mock_alchemy.mocking import UnifiedAlchemyMagicMock, AlchemyMagicMock
from unittest.mock import MagicMock, patch
from sqlalchemy.orm import Session

from persistence.shipments_persistence import ShipmentsPersistence
from models.shipment_status import ShipmentStatus
from tests.mocks.db_mocks import mock_db_shipments


class TestShipmentsPersistence(unittest.TestCase):

    def test_get_shipments(self):
        session = UnifiedAlchemyMagicMock(
        )
        for shipment in mock_db_shipments:
            session.add(shipment)

        persistence = ShipmentsPersistence(session)
        shipments = persistence.get_shipments()

        self.assertEqual(len(shipments), len(mock_db_shipments))

    def test_get_shipments_with_params(self):
        # UnifiedAlchemyMagicMock() does not support complex queries, therefore we just check if filter was called
        session = UnifiedAlchemyMagicMock()

        persistence = ShipmentsPersistence(session)
        persistence.get_shipments(status=ShipmentStatus.PENDING)

        session.filter.assert_called_once()

    def test_get_shipment_by_id(self):
        # UnifiedAlchemyMagicMock() does not support get which is used in tested method,
        # so we use AlchemyMagicMock that returns random data
        session = AlchemyMagicMock()

        persistence = ShipmentsPersistence(session)

        shipment = persistence.get_shipment_by_id(mock_db_shipments[0].id)
        self.assertIsNotNone(shipment)

    def test_create_shipment(self):
        session = UnifiedAlchemyMagicMock()

        persistence = ShipmentsPersistence(session)
        mock_shipment = mock_db_shipments[0]
        shipment = persistence.create_shipment(
            destination_store_id=mock_shipment.destination_store_id,
            source_store_id=mock_shipment.source_store_id,
            estimated_delivery_date=mock_shipment.estimated_delivery_date,
            estimated_ship_date=mock_shipment.estimated_ship_date,
            items=[]
        )
        self.assertIsNotNone(shipment)
        self.assertEqual(shipment.destination_store_id,
                         mock_shipment.destination_store_id)
        self.assertEqual(shipment.source_store_id,
                         mock_shipment.source_store_id)
        self.assertEqual(shipment.estimated_delivery_date,
                         mock_shipment.estimated_delivery_date)
        self.assertEqual(shipment.estimated_ship_date,
                         mock_shipment.estimated_ship_date)

    @patch.object(Session, 'refresh')
    @patch.object(Session, 'get')
    def test_update_shipment(self, mock_get, mock_refresh):
        persistence = ShipmentsPersistence(Session())
        mock_shipment = mock_db_shipments[0]
        mock_get.return_value = mock_shipment

        new_shipment = persistence.update_shipment(
            shipment_id=mock_shipment.id,
            status=ShipmentStatus.SHIPPING.value
        )

        self.assertEqual(new_shipment.status, ShipmentStatus.SHIPPING.value)
