import uuid
from fastapi.testclient import TestClient
from api.shipments_api import app
from api.dto.shipment_post_dto import ShipmentPostDto
from api.dto.shipment_patch_dto import ShipmentPatchDto
from unittest.mock import MagicMock, patch
from models.shipment_status import ShipmentStatus

from api.dto.shipment_full_dto import ShipmentFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_shipments


client = TestClient(app)


@patch('services.shipments_service.ShipmentsService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/store-shipments/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('services.shipments_service.ShipmentsService.get_instance')
def test_get_shipments(mock_get_instance):
    mock_response = [ShipmentFullDto.from_model(
        shipment) for shipment in mock_db_shipments]
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_shipments_with_items.return_value = mock_response
    expected_response = [shipment.dict() for shipment in mock_response]

    response = client.get("/store-shipments")

    assert response.status_code == 200
    assert_equal_list_of_dicts(response.json(), expected_response)


def test_get_invalid_status():
    response = client.get(f"/store-shipments", params={"status": "INVALID"})

    assert response.status_code == 422


@ patch('services.shipments_service.ShipmentsService.get_instance')
def test_get_shipment_by_id(mock_get_instance):
    mock_response = ShipmentFullDto.from_model(
        mock_db_shipments[0])
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_shipment_with_items_by_id.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.get(f"/store-shipments/{mock_db_shipments[0].id}")

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


@ patch('services.shipments_service.ShipmentsService.get_instance')
def test_get_shipment_by_id_not_found(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_service.get_shipment_with_items_by_id.return_value = None

    response = client.get(f"/store-shipments/1")

    assert response.status_code == 404


@ patch('services.shipments_service.ShipmentsService.get_instance')
def test_create_shipment(mock_get_instance):
    mock_get_instance.return_value = mock_service = MagicMock()
    shipment = mock_db_shipments[0]
    mock_response = ShipmentFullDto.from_model(
        shipment)
    mock_service.create_shipment.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.post("/store-shipments", json=ShipmentPostDto(
        destination_store_id=shipment.destination_store_id,
        source_store_id=shipment.source_store_id,
        estimated_ship_date=str(shipment.estimated_ship_date),
        estimated_delivery_date=str(shipment.estimated_delivery_date),
        items=[],
    ).dict())

    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


@patch('services.shipments_service.ShipmentsService.get_instance')
def test_update_shipment(mock_get_instance):
    shipment = mock_db_shipments[0]
    mock_get_instance.return_value = mock_service = MagicMock()
    mock_response = ShipmentFullDto.from_model(
        shipment)
    mock_service.update_shipment.return_value = mock_response
    expected_response = mock_response.dict()

    response = client.patch(f"/store-shipments/{shipment.id}", json=ShipmentPatchDto(
        status=ShipmentStatus.PENDING,
    ).dict())

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)
