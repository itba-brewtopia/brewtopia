import os
from models.shipment import Shipment
from models.item import Item
from api.dto.shipment_full_dto import ShipmentFullDto
from tests.mocks.db_mocks import mock_db_shipments
from persistence.shipments_persistence import ShipmentsPersistence
import copy


def setup_db():
    session = ShipmentsPersistence().get_instance().session
    session.query(Item).delete()
    session.query(Shipment).delete()
    session.commit()
    session.close()


def seed_db():
    session = ShipmentsPersistence().get_instance().session
    for shipment in mock_db_shipments:
        insert_shipment = copy.deepcopy(shipment)
        insert_shipment.id = None
        session.add(insert_shipment)  # adds shipment and its items
        session.commit()

    session.commit()
    session.close()


def get_first_shipment():
    session = ShipmentsPersistence().get_instance().session
    shipment = session.query(Shipment).first()
    shipment_dto = ShipmentFullDto.from_model(shipment)
    session.close()
    return shipment_dto


def drop_db():
    session = ShipmentsPersistence().get_instance().session
    # rollback any pending transaction, like errors, to avoid locking for future tests
    session.rollback()
    session.query(Item).delete()
    session.query(Shipment).delete()
    session.commit()
    session.close()
