import datetime
from fastapi.testclient import TestClient
import pytest

from api.shipments_api import app
from api.dto.shipment_post_dto import ShipmentPostDto
from api.dto.shipment_patch_dto import ShipmentPatchDto
from api.dto.shipment_full_dto import ShipmentFullDto
from persistence.shipments_persistence import ShipmentsPersistence
from models.shipment_status import ShipmentStatus
from services.shipments_service import ShipmentsService
from api.dto.item_full_dto import ItemFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_shipments
from tests.utils.db_utils import setup_db, seed_db, drop_db, get_first_shipment

from unittest.mock import MagicMock, patch


client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown_function():
    yield

    drop_db()


def test_health_check():
    response = client.get("/store-shipments/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_get_shipments():
    seed_db()
    expected_response = [ShipmentFullDto.from_model(
        shipment).dict() for shipment in mock_db_shipments]

    response = client.get("/store-shipments", params={})

    assert response.status_code == 200
    assert_equal_list_of_dicts(expected_response, response.json())


def test_get_shipment_by_id():
    seed_db()
    shipment_dto = get_first_shipment()

    response = client.get(f'/store-shipments/{shipment_dto.id}')

    assert response.status_code == 200
    assert_equal_dicts(response.json(), shipment_dto.dict())


@patch('requests.get', return_value=MagicMock(status_code=200))
def test_create_shipment(mock_requests):
    shipment1 = mock_db_shipments[0]

    response = client.post("/store-shipments", json=ShipmentPostDto(destination_store_id=shipment1.destination_store_id,
                                                                    source_store_id=shipment1.source_store_id,
                                                                    estimated_ship_date=str(
                                                                        shipment1.estimated_ship_date),
                                                                    estimated_delivery_date=str(
                                                                        shipment1.estimated_delivery_date),
                                                                    items=[
                                                                        ItemFullDto.from_model(item).dict() for item in shipment1.items
                                                                    ]).dict())

    expected_response = ShipmentFullDto.from_model(
        shipment1).dict()
    print(response.json())
    expected_response["id"] = response.json()["id"]
    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


@patch('requests.patch', return_value=MagicMock(status_code=200))
def test_update_shipment(mock_requests):
    seed_db()
    shipment_dto = get_first_shipment()
    expected_response = shipment_dto.dict()
    expected_response["status"] = ShipmentStatus.DELIVERED.value
    expected_response["delivery_date"] = datetime.datetime.now().strftime(
        "%Y-%m-%d")
    expected_response["ship_date"] = datetime.datetime.now().strftime(
        "%Y-%m-%d")

    response = client.patch(f'/store-shipments/{shipment_dto.id}', json=ShipmentPatchDto(
        status=ShipmentStatus.DELIVERED.value
    ).dict())

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)
