import datetime
import uuid
from models.shipment import Shipment
from models.item import Item
from models.shipment_status import ShipmentStatus

ids = [uuid.uuid4() for _ in range(3)]
mock_db_shipments = [
    Shipment(id=ids[0],
             destination_store_id='5f4f3f2f1e0d0c0b0a090808',
             source_store_id='5f4f3f2f1e0d0c0b0a090809',
             status=ShipmentStatus.PENDING.value,
             estimated_ship_date=datetime.datetime.utcnow().isoformat()[:10],
             estimated_delivery_date=datetime.datetime.utcnow().isoformat()[
        :10],
        is_deleted=False,
        restock=False,
        items=[
                 Item(
                     product_id='5f4f3f2f1e0d0c0b0a090801',
                     quantity=1,
                     shipment_id=ids[0],
                 )]
    ),
    Shipment(id=ids[1],
             destination_store_id='5f4f3f2f1e0d0c0b0a090811',
             source_store_id='5f4f3f2f1e0d0c0b0a090812',
             status=ShipmentStatus.PENDING.value,
             estimated_ship_date=datetime.datetime.utcnow().isoformat()[:10],
             estimated_delivery_date=datetime.datetime.utcnow().isoformat()[
        :10],
        is_deleted=False,
        restock=False,
        items=[
                 Item(
                     product_id='5f4f3f2f1e0d0c0b0a090801',
                     quantity=1,
                     shipment_id=ids[1],
                 )]
    ),
    Shipment(id=ids[2],
             destination_store_id='5f4f3f2f1e0d0c0b0a090814',
             source_store_id='5f4f3f2f1e0d0c0b0a090815',
             status=ShipmentStatus.DELIVERED.value,
             estimated_ship_date=datetime.datetime.utcnow().isoformat()[:10],
             estimated_delivery_date=datetime.datetime.utcnow().isoformat()[
        :10],
        is_deleted=False,
        restock=False,
        items=[
                 Item(
                     product_id='5f4f3f2f1e0d0c0b0a090801',
                     quantity=1,
                     shipment_id=ids[2],
                 )]
    ),
]
