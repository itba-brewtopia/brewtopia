from datetime import datetime, timedelta
import os
from typing import Optional
from func_timeout import FunctionTimedOut, func_timeout
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from uuid import UUID

from models.base import Base
from models.shipment import Shipment
from models.item import Item
from models.shipment_status import ShipmentStatus
from api.dto.item_full_dto import ItemFullDto


def _get_day_filter(date: str):
    return Shipment.estimated_ship_date.between(datetime.strptime(
        date, '%Y-%m-%d'),
        datetime.strptime(date, '%Y-%m-%d') + timedelta(days=1))


class ShipmentsPersistence:

    _instance = None

    def __init__(self, session=None):
        if session is not None:
            self.session = session
            return
        database_name = os.environ.get('POSTGRES_DB')
        database_user = os.environ.get('POSTGRES_USER')
        database_password = os.environ.get('POSTGRES_PASSWORD')
        engine = create_engine(
            f'postgresql://{database_user}:{database_password}@shipments-psql:5432/{database_name}')
        self.session = sessionmaker(
            autocommit=False, autoflush=False, bind=engine)()
        # Creates the tables if they don't exist
        Base.metadata.create_all(bind=engine)

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: self.session.query(Shipment).first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def get_shipments(self, status: Optional[str] = None,
                      estimated_ship_date: Optional[str] = None, estimated_delivery_date: Optional[str] = None,
                      ship_date: Optional[str] = None, delivery_date: Optional[str] = None,
                      source_store_id: Optional[str] = None, destination_store_id: Optional[str] = None,
                      restock: Optional[bool] = None):
        query = self.session.query(Shipment)
        filter_args = []
        if status is not None:
            filter_args.append(Shipment.status == status)
        if estimated_ship_date is not None and estimated_ship_date != '':
            filter_args.append(_get_day_filter(estimated_ship_date))
        if estimated_delivery_date is not None and estimated_delivery_date != '':
            filter_args.append(_get_day_filter(estimated_delivery_date))
        if ship_date is not None and ship_date != '':
            filter_args.append(_get_day_filter(ship_date))
        if delivery_date is not None and delivery_date != '':
            filter_args.append(_get_day_filter(delivery_date))
        if source_store_id is not None:
            filter_args.append(Shipment.source_store_id == source_store_id)
        if destination_store_id is not None:
            filter_args.append(
                Shipment.destination_store_id == destination_store_id)
        if restock is not None:
            filter_args.append(Shipment.restock == restock)

        if filter_args:
            return query.filter(*filter_args)
        else:
            return query.all()

    def get_shipment_by_id(self, shipment_id: UUID):
        return self.session.get(Shipment, shipment_id)

    def create_shipment(self,
                        destination_store_id: str,
                        source_store_id: str,
                        estimated_ship_date: str,
                        estimated_delivery_date: str,
                        items: list[ItemFullDto],
                        restock: Optional[bool] = False):
        shipment = Shipment(
            destination_store_id=destination_store_id,
            source_store_id=source_store_id,
            estimated_ship_date=estimated_ship_date,
            estimated_delivery_date=estimated_delivery_date,
            restock=restock
        )

        self.session.add(shipment)
        self.session.commit()
        self.session.refresh(shipment)  # Refresh to get the id afterwards
        items = [Item(product_id=item.product_id, shipment_id=shipment.id,
                      quantity=item.quantity) for item in items]
        self.session.add_all(items)
        self.session.commit()
        self.session.refresh(shipment)

        return shipment

    def update_shipment(self, shipment_id: UUID, status: str):
        shipment = self.session.get(Shipment, shipment_id)
        if shipment is None:
            return None
        if shipment.status == status:
            raise ValueError("Shipment already has status " + status)
        shipment.status = status
        if status == ShipmentStatus.SHIPPING.value:
            shipment.ship_date = datetime.now()
        elif status == ShipmentStatus.DELIVERED.value:
            shipment.delivery_date = datetime.now()
            if shipment.ship_date is None:
                shipment.ship_date = datetime.now()

        self.session.commit()
        self.session.refresh(shipment)
        return shipment
