from sqlalchemy import Column, String, DateTime, Boolean
from models.base import Base
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
import uuid

from models.shipment_status import ShipmentStatus
from sqlalchemy.dialects.postgresql import UUID


class Shipment(Base):
    __tablename__ = "shipments"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    destination_store_id = Column(String)
    source_store_id = Column(String)
    status = Column(String, default=ShipmentStatus.PENDING.value)
    estimated_ship_date = Column(DateTime)
    estimated_delivery_date = Column(DateTime)
    ship_date = Column(DateTime, nullable=True)
    delivery_date = Column(DateTime, nullable=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=func.now())
    modified_at = Column(DateTime, default=func.now(), onupdate=func.now())
    restock = Column(Boolean, default=False)

    items = relationship("Item")  # one to many relationship with items
