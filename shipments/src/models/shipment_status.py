from enum import Enum


class ShipmentStatus(str, Enum):
    PENDING = 'PENDING'
    SHIPPING = 'SHIPPING'
    DELIVERED = 'DELIVERED'
    CANCELLED = 'CANCELLED'

    @classmethod
    def is_valid(cls, status: str):
        status = status.upper() if status is not None else None
        return status in cls.__members__
