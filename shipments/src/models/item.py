from sqlalchemy import Column, BigInteger, ForeignKey, String
from models.base import Base
from sqlalchemy.orm import Mapped, mapped_column


class Item(Base):
    __tablename__ = "items"

    # primary key is a composite of product_id and shipment_id
    product_id = Column(String, primary_key=True)
    shipment_id: Mapped[int] = mapped_column(
        ForeignKey("shipments.id"), primary_key=True)
    quantity = Column(BigInteger)
