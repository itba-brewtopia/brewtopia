from typing import Optional
from persistence.shipments_persistence import ShipmentsPersistence
import requests

from api.dto.shipment_full_dto import ShipmentFullDto
from api.dto.item_full_dto import ItemFullDto
from models.shipment_status import ShipmentStatus


def _check_store_id(store_id: str):
    response = requests.get(
        f'http://stores-service:8080/stores/{store_id}', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            raise ValueError("Invalid store id: " + store_id)
        else:
            raise Exception("Shipments server internal error")


def _check_items(items: list[ItemFullDto]):
    if len(items) == 0:
        raise ValueError("Items list is empty")

    for item in items:
        if item.quantity <= 0:
            raise ValueError("Invalid item quantity: " +
                             str(item.quantity))

        response = requests.get(
            f'http://products-service:8080/products/{item.product_id}', timeout=10)
        if response.status_code != 200:
            if response.status_code == 404:
                raise ValueError("Invalid product id: " + item.product_id)
            else:
                raise Exception("Shipments server internal error")


def _post_stock_increase(shipment: ShipmentFullDto):
    for item in shipment.items:
        # TODO: rollback on failure?¿
        request = {
            'store_id': shipment.destination_store_id,
            'product_id': item.product_id,
            'quantity': item.quantity,
            'action': 'increment'
        }
        response = requests.patch(
            url=f'http://stock-service:8080/stock', timeout=10,
            json=request)
        if response.status_code != 200:
            if response.status_code == 404 or response.status_code == 422:
                raise ValueError(
                    "Invalid stock increment request: " + str(request))
            else:
                raise Exception(
                    "Stock server internal error: " + str(response))


class ShipmentsService:

    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return ShipmentsPersistence.get_instance().health_check()

    def get_shipments_with_items(self, status: Optional[str] = None,
                                 estimated_ship_date: Optional[str] = None, estimated_delivery_date: Optional[str] = None,
                                 ship_date: Optional[str] = None, delivery_date: Optional[str] = None,
                                 source_store_id: Optional[str] = None, destination_store_id: Optional[str] = None,
                                 restock: Optional[bool] = None):
        shipments = ShipmentsPersistence.get_instance().get_shipments(
            status=status,
            estimated_ship_date=estimated_ship_date, estimated_delivery_date=estimated_delivery_date,
            ship_date=ship_date, delivery_date=delivery_date,
            source_store_id=source_store_id, destination_store_id=destination_store_id, restock=restock)

        response = []
        for shipment in shipments:
            response.append(ShipmentFullDto.from_model(
                shipment))

        return response

    def get_shipment_with_items_by_id(self, shipment_id: str):
        shipment = ShipmentsPersistence.get_instance().get_shipment_by_id(shipment_id)
        if shipment is None:
            return None

        return ShipmentFullDto.from_model(shipment)

    def create_shipment(self,
                        destination_store_id: str,
                        source_store_id: str,
                        estimated_ship_date: str,
                        estimated_delivery_date: str,
                        items: list[ItemFullDto],
                        restock: Optional[bool] = False):
        _check_store_id(destination_store_id)
        _check_store_id(source_store_id)
        _check_items(items)

        shipment = ShipmentsPersistence.get_instance().create_shipment(
            destination_store_id, source_store_id, estimated_ship_date, estimated_delivery_date, items, restock
        )
        return ShipmentFullDto.from_model(shipment)

    def update_shipment(self, shipment_id: str, status: str):
        shipment = ShipmentsPersistence.get_instance(
        ).update_shipment(shipment_id, status)
        shipment_dto = ShipmentFullDto.from_model(shipment)

        if status == ShipmentStatus.DELIVERED.value:
            _post_stock_increase(shipment_dto)

        return shipment_dto
