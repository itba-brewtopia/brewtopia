from fastapi import FastAPI, HTTPException
from typing import List, Optional

from api.dto.shipment_full_dto import ShipmentFullDto
from api.dto.shipment_post_dto import ShipmentPostDto
from api.dto.shipment_patch_dto import ShipmentPatchDto
from services.shipments_service import ShipmentsService
from models.shipment_status import ShipmentStatus

app = FastAPI(openapi_url="/store-shipments/openapi.json",
              docs_url="/store-shipments/docs", redoc_url="/store-shipments/redoc")


@app.get("/store-shipments/health")
async def health_check():
    service_status = ShipmentsService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@app.get("/store-shipments", response_model=List[ShipmentFullDto])
async def get_shipments(status: Optional[str] = None,
                        estimated_ship_date: Optional[str] = None, estimated_delivery_date: Optional[str] = None,
                        ship_date: Optional[str] = None, delivery_date: Optional[str] = None,
                        source_store_id: Optional[str] = None, destination_store_id: Optional[str] = None,
                        restock: Optional[bool] = None):
    try:
        if status == '':
            status = None
        elif status is not None and not ShipmentStatus.is_valid(status):
            raise ValueError("Invalid status: " + status)
        shipments_with_items = ShipmentsService.get_instance().get_shipments_with_items(
            status=status,
            estimated_ship_date=estimated_ship_date, estimated_delivery_date=estimated_delivery_date,
            ship_date=ship_date, delivery_date=delivery_date,
            source_store_id=source_store_id, destination_store_id=destination_store_id, restock=restock
        )
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))
    return shipments_with_items


@app.get("/store-shipments/{shipment_id}", response_model=ShipmentFullDto)
async def get_shipment_by_id(shipment_id: str):
    shipment_with_items = ShipmentsService.get_instance().\
        get_shipment_with_items_by_id(shipment_id)
    if shipment_with_items is None:
        raise HTTPException(status_code=404, detail="Shipment not found")
    return shipment_with_items


@app.post("/store-shipments", response_model=ShipmentFullDto, status_code=201)
async def create_shipment(shipment: ShipmentPostDto):
    try:
        shipment_with_items = ShipmentsService.get_instance().create_shipment(destination_store_id=shipment.destination_store_id,
                                                                              source_store_id=shipment.source_store_id,
                                                                              estimated_ship_date=shipment.estimated_ship_date,
                                                                              estimated_delivery_date=shipment.estimated_delivery_date,
                                                                              items=shipment.items, restock=shipment.restock)
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    return shipment_with_items


@app.patch("/store-shipments/{shipment_id}", response_model=ShipmentFullDto)
async def update_shipment(shipment_id: str, patch: ShipmentPatchDto):
    try:
        shipment_with_items = ShipmentsService.get_instance().update_shipment(shipment_id=shipment_id,
                                                                              status=patch.status)
    except ValueError as e:
        raise HTTPException(
            status_code=422, detail="Invalid parameters: " + str(e))

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

    if shipment_with_items is None:
        raise HTTPException(status_code=404, detail="Shipment not found")
    return shipment_with_items
