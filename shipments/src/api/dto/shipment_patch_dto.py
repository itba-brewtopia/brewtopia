from pydantic import BaseModel

from models.shipment_status import ShipmentStatus


class ShipmentPatchDto(BaseModel):
    status: ShipmentStatus
