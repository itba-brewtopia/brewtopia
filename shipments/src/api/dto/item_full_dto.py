from pydantic import BaseModel


class ItemFullDto(BaseModel):
    product_id: str
    quantity: float

    def from_model(item):
        return ItemFullDto(
            product_id=str(item.product_id),
            quantity=item.quantity
        )
