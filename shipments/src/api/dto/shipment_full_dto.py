from typing import Union
from pydantic import BaseModel
from models.shipment import Shipment
from api.dto.item_full_dto import ItemFullDto


class ShipmentFullDto(BaseModel):
    id: str
    destination_store_id: str
    source_store_id: str
    status: str
    estimated_ship_date: str
    estimated_delivery_date: str
    ship_date: Union[str, None] = None
    delivery_date: Union[str, None] = None
    is_deleted: bool
    created_at: str
    modified_at: str
    items: list[ItemFullDto]
    restock: bool

    @staticmethod
    def from_model(shipment: Shipment):
        if shipment is None:
            return None
        items_parsed = [ItemFullDto.from_model(
            item) for item in shipment.items]
        return ShipmentFullDto(
            id=str(shipment.id),
            destination_store_id=str(shipment.destination_store_id),
            source_store_id=str(shipment.source_store_id),
            status=shipment.status,
            ship_date=str(shipment.ship_date) if shipment.ship_date else None,
            estimated_ship_date=str(
                shipment.estimated_ship_date) if shipment.estimated_ship_date else None,
            estimated_delivery_date=str(
                shipment.estimated_delivery_date) if shipment.estimated_delivery_date else None,
            delivery_date=str(
                shipment.delivery_date) if shipment.delivery_date else None,
            is_deleted=shipment.is_deleted,
            created_at=str(shipment.created_at),
            modified_at=str(shipment.modified_at),
            items=items_parsed,
            restock=shipment.restock
        )
