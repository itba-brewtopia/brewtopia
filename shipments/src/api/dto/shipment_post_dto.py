from typing import Optional
from pydantic import BaseModel

from api.dto.item_full_dto import ItemFullDto


class ShipmentPostDto(BaseModel):
    destination_store_id: str
    source_store_id: str
    estimated_ship_date: str
    estimated_delivery_date: str
    items: list[ItemFullDto]
    restock: Optional[bool] = False
