#DOMAIN_NAME array
DOMAIN_NAMES=("carts-and-orders" "customers" "products-and-stock", "shipments" "stores")

#for loop
export TEST_TARGET=INTEGRATION
for DOMAIN_NAME in ${DOMAIN_NAMES[@]}
do
    echo "Starting $DOMAIN_NAME"
    export $(cat context.env | xargs)
    docker login -u $CI_REGISTRY_USER --password $CI_JOB_TOKEN $CI_REGISTRY
    docker compose -f ${DOMAIN_NAME}/docker-compose.yml --env-file ${DOMAIN_NAME}/env-files/.env.test --profile all pull
    docker compose -f ${DOMAIN_NAME}/docker-compose.yml --env-file ${DOMAIN_NAME}/env-files/.env.test --profile all up -d --wait
done

# Run integration tests
echo "Running integration tests"

# TODO
