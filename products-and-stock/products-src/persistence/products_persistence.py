

import datetime
import os
import unittest
from bson import ObjectId
from func_timeout import FunctionTimedOut, func_timeout
import mongoengine

from api.dto.input.product_patch_dto import ProductPatchDto
from models.product import Product
from models.product_category import ProductCategory
from api.dto.input.product_post_dto import ProductPostDto


class ProductsPersistence(unittest.TestCase):
    _instance = None

    def __init__(self):
        database_name = os.environ.get("MONGO_INITDB_DATABASE")
        database_user = os.environ.get("MONGO_INITDB_ROOT_USERNAME")
        database_password = os.environ.get("MONGO_INITDB_ROOT_PASSWORD")
        mongoengine.connect(
            uuidRepresentation="standard",
            db=database_name,
            username=database_user,
            password=database_password,
            host="products-and-stock-mongodb",
        )

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: Product.objects().first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def create_product(self, product: ProductPostDto, category: ProductCategory):
        now = datetime.datetime.utcnow()
        new_product = Product(
            name=product.name,
            description=product.description,
            price=product.price,
            images=product.images,
            category=category,
            created_at=now,
            modified_at=now,
        )
        return new_product.save()

    def get_product_by_id(self, product_id: str):
        if not ObjectId.is_valid(product_id):
            return None
        return Product.objects(id=ObjectId(product_id)).first()

    def get_products(self, category_id: str = None, query: str = None, min_price: float = None, max_price: float = None, is_deleted: bool = False):
        query_params = {}
        query_params["is_deleted"] = is_deleted
        if category_id:
            if not ObjectId.is_valid(category_id):
                return []
            query_params["category__id"] = ObjectId(category_id)
        if query:
            query_params["name__icontains"] = query
        if min_price:
            query_params["price__gte"] = min_price
        if max_price:
            query_params["price__lte"] = max_price
        return Product.objects(**query_params).all()

    def update_product(self, product_id: str, product_patch_dto: ProductPatchDto):
        if not ObjectId.is_valid(product_id):
            return None
        product = Product.objects(id=ObjectId(product_id)).first()
        if product is None:
            return None
        if product_patch_dto.name:
            product.name = product_patch_dto.name
        if product_patch_dto.description:
            product.description = product_patch_dto.description
        if product_patch_dto.price:
            product.price = product_patch_dto.price
        if product_patch_dto.images:
            product.images = product_patch_dto.images
        if product_patch_dto.category:
            product.category = ProductCategory(
                id=product_patch_dto.category.id,
                name=product_patch_dto.category.name,
                image=product_patch_dto.category.image,
            )
        if product_patch_dto.is_deleted:
            product.is_deleted = product_patch_dto.is_deleted
        product.modified_at = datetime.datetime.utcnow()
        return product.save()
