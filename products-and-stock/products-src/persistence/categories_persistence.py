import datetime
import os
from bson import ObjectId
from func_timeout import FunctionTimedOut, func_timeout
import mongoengine

from api.dto.input.category_patch_dto import CategoryPatchDto
from api.dto.input.category_post_dto import CategoryPostDto
from models.category import Category


class CategoriesPersistence:
    _instance = None

    def __init__(self):
        database_name = os.environ.get('MONGO_INITDB_DATABASE')
        database_user = os.environ.get('MONGO_INITDB_ROOT_USERNAME')
        database_password = os.environ.get('MONGO_INITDB_ROOT_PASSWORD')
        mongoengine.connect(
            db=database_name,
            username=database_user,
            password=database_password,
            host="products-and-stock-mongodb",
            uuidRepresentation="standard"

        )

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: Category.objects().first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def get_category_by_id(self, category_id: str):
        if not ObjectId.is_valid(category_id):
            return None
        return Category.objects(id=ObjectId(category_id)).first()

    def create_category(self, category: CategoryPostDto):
        now = datetime.datetime.utcnow()
        new_category = Category(
            name=category.name,
            image=category.image,
            created_at=now,
            modified_at=now,
        )
        return new_category.save()

    def get_categories(self):
        return Category.objects(is_deleted=False).all()

    def update_category(self, category_id: str, category_patch_dto: CategoryPatchDto):
        if not ObjectId.is_valid(category_id):
            return None
        category = Category.objects(id=ObjectId(category_id)).first()
        if category is None:
            return None
        if category_patch_dto.name is not None:
            category.name = category_patch_dto.name
        if category_patch_dto.image is not None:
            category.image = category_patch_dto.image
        if category_patch_dto.is_deleted is not None:
            category.is_deleted = category_patch_dto.is_deleted
        category.modified_at = datetime.datetime.utcnow()
        return category.save()
