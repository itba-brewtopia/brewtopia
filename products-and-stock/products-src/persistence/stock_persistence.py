import os

from models.stock import Stock
from models.base import Base

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker



class StockPersistence:

    _instance = None

    def __init__(self, session=None):
        if session is not None:
            self.session = session
            return
        database_name = os.environ.get('POSTGRES_DB')
        database_user = os.environ.get('POSTGRES_USER')
        database_password = os.environ.get('POSTGRES_PASSWORD')
        self.engine = create_engine(
            f'postgresql://{database_user}:{database_password}@products-and-stock-psql:5432/{database_name}')
        self.session = sessionmaker(
            autocommit=False, autoflush=False, bind=self.engine)()

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance
    
    def create_stock(self, store_id: str, product_id: str, quantity: int, minimum_quantity: int):
        stock = Stock(
            store_id=store_id,
            product_id=product_id,
            quantity=quantity,
            minimum_quantity=minimum_quantity
        )
        self.session.add(stock)
        self.session.commit()
        self.session.refresh(stock)
        return stock