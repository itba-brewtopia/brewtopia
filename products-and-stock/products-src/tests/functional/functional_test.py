from unittest.mock import MagicMock, patch
from services.categories_service import CategoriesService
from persistence.categories_persistence import CategoriesPersistence
from services.products_service import ProductsService
from persistence.products_persistence import ProductsPersistence
import pytest

from fastapi.testclient import TestClient
from api.api import app

from tests.utils.db_utils import seed_category_db, seed_product_db, setup_db, drop_db
from tests.mocks.db_mocks import mock_post_category, mock_db_category, mock_post_product, mock_db_product, mock_db_products, mock_db_categories, mock_central_store


client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown():
    ProductsPersistence._instance = None
    ProductsService._instance = None
    CategoriesService._instance = None
    CategoriesPersistence._instance = None

    yield

    drop_db()


def test_health_check():
    response = client.get("/products/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}

    response = client.get("/categories/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_create_category():
    category = mock_post_category
    response = client.post(f"/categories", json=category.dict())
    json_response = response.json()
    assert response.status_code == 201
    assert json_response["id"] != None
    assert json_response['created_at'] == json_response['modified_at']
    assert json_response["name"] == category.name
    assert json_response["image"] == category.image


def test_create_category_missing_name():
    category_json = mock_post_category.dict()
    del category_json["name"]
    response = client.post("/categories", json=category_json)
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "name"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test_create_category_missing_image():
    category_json = mock_post_category.dict()
    del category_json["image"]
    response = client.post("/categories", json=category_json)
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "image"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test_create_category_invalid_image():
    category_json = mock_post_category.dict()
    category_json["image"] = "invalid_url"
    response = client.post("/categories", json=category_json)
    response_json = response.json()
    assert response.status_code == 422
    assert response_json['detail'][0]['loc'] == ['body', 'image']


def test_get_category_by_id():
    seed_category_db()

    response = client.get(f"/categories/{mock_db_category.id}")
    json_response = response.json()

    assert response.status_code == 200
    assert json_response["id"] == str(mock_db_category.id)


def test_get_category_by_id_not_found():
    seed_category_db()
    response = client.get("/categories/123")
    assert response.status_code == 404
    assert response.json() == {"detail": "Category not found"}


@patch("services.utils._get_central_store", return_value=mock_central_store)
def test_create_product(mock_get_central_store):
    seed_category_db()

    product = mock_post_product.dict()

    response = client.post("/products", json=product)
    json_response = response.json()

    mock_get_central_store.assert_called()
    assert response.status_code == 201
    assert json_response["id"] != None
    assert json_response["name"] == product["name"]
    assert json_response["category"] is not None
    assert json_response["created_at"] == json_response["modified_at"]


def test_create_product_missing_name():
    seed_category_db()

    product = mock_post_product.dict()
    del product["name"]

    response = client.post("/products", json=product)
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "name"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test_create_product_missing_category():
    seed_category_db()

    product = mock_post_product.dict()
    del product["category_id"]

    response = client.post("/products", json=product)
    assert response.status_code == 422
    assert response.json() == {
        "detail": [
            {
                "loc": ["body", "category_id"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test_create_product_invalid_category():
    seed_category_db()

    product = mock_post_product.dict()
    product["category_id"] = "invalid_id"

    response = client.post("/products", json=product)
    assert response.status_code == 422
    assert response.json() == {
        "detail": "Invalid category id"
    }


@patch("services.utils._get_central_store", return_value=mock_central_store)
def test_create_product_after_category_insert(mock_get_central_store):
    category = mock_post_category.dict()
    response = client.post("/categories", json=category)
    category_id = response.json()["id"]

    product = mock_post_product.dict()
    product["category_id"] = category_id

    response = client.post("/products", json=product)
    json_response = response.json()

    mock_get_central_store.assert_called()
    assert response.status_code == 201
    assert json_response["id"] != None
    assert json_response["name"] == product["name"]
    assert json_response["category"] is not None
    assert json_response['category']['id'] == category_id
    assert json_response['category']['name'] == category['name']


def test_get_product_by_id():
    seed_category_db()
    seed_product_db()

    response = client.get(f"/products/{mock_db_product.id}")
    json_response = response.json()

    assert response.status_code == 200
    assert json_response["id"] == str(mock_db_product.id)


def test_get_product_by_id_not_found():
    seed_category_db()
    seed_product_db()
    response = client.get("/products/123")
    assert response.status_code == 404
    assert response.json() == {"detail": "Product not found"}


@patch("services.utils._get_central_store", return_value=mock_central_store)
def test_get_product_after_insert(mock_get_central_store):
    seed_category_db()
    product_dto = mock_post_product.dict()

    response = client.post("/products", json=product_dto)

    json_post_response = response.json()
    mock_get_central_store.assert_called()
    assert response.status_code == 201

    get_response = client.get(f"/products/{json_post_response['id']}")
    json_get_response = get_response.json()

    assert get_response.status_code == 200
    del json_get_response["created_at"]
    del json_get_response["modified_at"]
    del json_post_response["created_at"]
    del json_post_response["modified_at"]
    assert json_get_response == json_post_response


def test_get_categories():
    seed_category_db()

    response = client.get("/categories")
    json_response = response.json()

    assert response.status_code == 200
    assert len(json_response) == 2


def test_get_categories():

    response = client.get("/categories")
    json_response = response.json()

    assert response.status_code == 200
    assert len(json_response) == 0


def test_get_categories_after_insert():
    category_dto = mock_post_category.dict()

    response = client.post("/categories", json=category_dto)

    json_post_response = response.json()

    assert response.status_code == 201

    get_response = client.get("/categories")
    json_get_response = get_response.json()

    assert get_response.status_code == 200
    assert len(json_get_response) == 1
    assert json_get_response[0]['id'] == json_post_response['id']


def test_get_products():
    seed_category_db()
    seed_product_db()

    response = client.get("/products")
    json_response = response.json()

    assert response.status_code == 200
    assert len(json_response) == len(mock_db_products)


def test_get_products_by_category():
    seed_category_db()
    seed_product_db()

    category_id = str(mock_db_category.id)

    response = client.get("/products?category_id=" + category_id)
    json_response = response.json()

    assert response.status_code == 200
    assert all(product['category']['id'] ==
               category_id for product in json_response)


def test_get_products_by_category_not_found():
    seed_category_db()
    seed_product_db()

    category_id = "123"

    response = client.get("/products?category_id=" + category_id)
    json_response = response.json()

    assert response.status_code == 200
    assert len(json_response) == 0


def test_get_products_by_price_range():
    seed_category_db()
    seed_product_db()

    max_price = 150
    min_price = 0

    response = client.get(
        f"/products?min_price={min_price}&max_price={max_price}")
    json_response = response.json()

    assert response.status_code == 200
    assert all(min_price <= float(
        product['price']) <= max_price for product in json_response)


def test_get_products_by_price_range_and_category():
    seed_category_db()
    seed_product_db()

    max_price = 300
    min_price = 110
    category_id = str(mock_db_category.id)

    response = client.get(
        f"/products?min_price={min_price}&max_price={max_price}&category_id={category_id}")
    json_response = response.json()

    assert response.status_code == 200
    assert len(json_response) == 0


def test_get_products_by_query():
    seed_category_db()
    seed_product_db()

    query = "1"

    response = client.get(f"/products?query={query}")
    json_response = response.json()

    assert response.status_code == 200
    assert all(query in product['name'] for product in json_response)


def test_update_category():
    seed_category_db()

    new_name = "new name"

    response = client.patch(
        f"/categories/{str(mock_db_category.id)}", json={"name": new_name})
    json_response = response.json()

    assert response.status_code == 200
    assert json_response['name'] == new_name
    assert json_response['created_at'] != json_response['modified_at']


def test_update_category_not_found():

    new_name = "new name"

    response = client.patch(f"/categories/123", json={"name": new_name})
    json_response = response.json()

    assert response.status_code == 404
    assert json_response == {"detail": "Category not found"}


def test_delete_category():
    seed_category_db()

    length_1 = len(mock_db_categories)
    response = client.patch(f"/categories/{str(mock_db_category.id)}", json={
        "is_deleted": True
    })
    json_response = response.json()

    get_res = client.get("/categories")
    json_get_response = get_res.json()
    length_2 = len(json_get_response)

    assert response.status_code == 200
    assert length_1 != length_2


def test_update_product():
    seed_category_db()
    seed_product_db()

    new_name = "new name"

    response = client.patch(
        f"/products/{str(mock_db_product.id)}", json={"name": new_name})
    json_response = response.json()

    assert response.status_code == 200
    assert json_response['name'] == new_name
    assert json_response['created_at'] != json_response['modified_at']


def test_delete_product():
    seed_category_db()
    seed_product_db()

    length_1 = len(mock_db_products)
    response = client.patch(f"/products/{str(mock_db_product.id)}", json={
        "is_deleted": True
    })
    json_response = response.json()

    get_res = client.get("/products")
    json_get_response = get_res.json()
    length_2 = len(json_get_response)

    assert json_response['is_deleted'] == True
    assert response.status_code == 200
    assert get_res.status_code == 200
    assert length_1 != length_2


def test_delete_product_not_found():

    response = client.patch("/products/12345565767", json={
        "is_deleted": True
    })
    json_response = response.json()

    assert response.status_code == 404
    assert json_response == {"detail": "Product not found"}


def test_update_product_category():
    seed_category_db()
    seed_product_db()

    new_category_id = str(mock_db_categories[1].id)
    categories_res = client.get(f"/categories/{new_category_id}")
    assert new_category_id != mock_db_categories[1].id
    assert categories_res.status_code == 200

    response = client.patch(f"/products/{str(mock_db_product.id)}", json={
        "category_id": new_category_id
    })
    json_response = response.json()

    assert json_response['category']['id'] == new_category_id
    assert str(mock_db_product.category.id) != json_response['category']['id']
    assert response.status_code == 200
