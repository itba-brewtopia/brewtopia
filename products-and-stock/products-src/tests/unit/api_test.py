from fastapi.testclient import TestClient
from api.dto.input.category_patch_dto import CategoryPatchDto
from models.category import Category
from api.dto.category_full_dto import CategoryFullDto
from api.dto.input.category_post_dto import CategoryPostDto
from api.api import app
from unittest.mock import patch, MagicMock
from tests.mocks.db_mocks import mock_db_product, mock_db_category, mock_db_categories, mock_db_products
from api.dto.input.product_post_dto import ProductPostDto

client = TestClient(app)


@patch('services.categories_service.CategoriesService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/categories/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('services.products_service.ProductsService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/products/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


# Tests for POST /categories
@patch("services.categories_service.CategoriesService.get_instance")
def test_create_category(mock_get_instance):
    category1 = mock_db_category
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.create_category.return_value = category1

    response = client.post("/categories", json=CategoryPostDto(
        name="Category 1",
        image="https://picsum.photos/200/300"
    ).dict())

    expected_response = category1.to_dict()

    assert response.status_code == 201
    assert response.json() == expected_response


@patch("services.categories_service.CategoriesService.get_instance")
def test_create_category_error(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    response = client.post("/categories", json={
        "image": "https://picsum.photos/200/300",
    })
    assert response.status_code == 422
    assert response.json() == {"detail": [
        {'loc': ['body', 'name'], 'msg': 'field required',
            'type': 'value_error.missing'}
    ]}

# Tests for GET /categories/{category_id}


@patch("services.categories_service.CategoriesService.get_instance")
def test_get_category_by_id(mock_get_instance):
    category1 = mock_db_category
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_category_by_id.return_value = category1

    response = client.get(f"/categories/{category1.id}")

    expected_response = category1.to_dict()

    assert response.status_code == 200
    assert response.json() == expected_response


@patch("services.categories_service.CategoriesService.get_instance")
def test_get_category_by_id_not_found(mock_get_instance):
    category1 = mock_db_category
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_category_by_id.return_value = None

    response = client.get(f"/categories/{category1.id}")

    expected_response = {
        "detail": "Category not found"
    }

    assert response.status_code == 404
    assert response.json() == expected_response


# Tests for POST /products
@patch("services.products_service.ProductsService.get_instance")
def test_create_product(mock_get_instance):
    product1 = mock_db_product
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.create_product.return_value = product1

    response = client.post("/products", json=ProductPostDto(
        name=mock_db_product.name,
        price=mock_db_product.price,
        description=mock_db_product.description,
        category_id=str(mock_db_product.category.id),
        images=mock_db_product.images
    ).dict())

    expected_response = product1.to_dict()

    assert response.status_code == 201
    assert response.json() == expected_response


@patch("services.products_service.ProductsService.get_instance")
def test_create_product_missing_name(mock_get_instance):
    product1 = mock_db_product
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    response = client.post("/products", json={
        "description": mock_db_product.description,
        "price": str(mock_db_product.price),
        "category_id": str(mock_db_product.category.id),
        "images": mock_db_product.images
    })

    assert response.status_code == 422
    assert response.json() == {"detail": [
        {'loc': ['body', 'name'], 'msg': 'field required',
            'type': 'value_error.missing'}
    ]}


@patch("services.products_service.ProductsService.get_instance")
def test_create_product_missing_category_id(mock_get_instance):
    product1 = mock_db_product
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    response = client.post("/products", json={
        "name": mock_db_product.name,
        "price": str(mock_db_product.price),
        "description": mock_db_product.description,
        "images": mock_db_product.images
    })

    assert response.status_code == 422
    assert response.json() == {"detail": [
        {'loc': ['body', 'category_id'], 'msg': 'field required',
            'type': 'value_error.missing'}
    ]}


@patch("services.products_service.ProductsService.get_instance")
def test_create_product_missing_description(mock_get_instance):
    product1 = mock_db_product

    product1.description = None
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.create_product.return_value = product1

    response = client.post("/products", json=ProductPostDto(
        name=mock_db_product.name,
        price=str(mock_db_product.price),
        category_id=str(mock_db_product.category.id),
        images=mock_db_product.images
    ).dict())

    assert response.status_code == 201
    assert response.json() == product1.to_dict()


# Tests for GET /products/{product_id}
@patch("services.products_service.ProductsService.get_instance")
def test_get_product_by_id(mock_get_instance):
    product1 = mock_db_product
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_product_by_id.return_value = product1

    response = client.get(f"/products/{product1.id}")

    mock_service.get_product_by_id.assert_called_once_with(str(product1.id))
    assert response.status_code == 200
    assert response.json()['id'] == str(product1.id)


@patch("services.products_service.ProductsService.get_instance")
def test_get_product_by_id_not_found(mock_get_instance):
    product1 = mock_db_product
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_product_by_id.return_value = None

    response = client.get(f"/products/{product1.id}")

    expected_response = {
        "detail": "Product not found"
    }

    assert response.status_code == 404
    assert response.json() == expected_response


@patch("services.categories_service.CategoriesService.get_instance")
def test_get_categories(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_categories.return_value = mock_db_categories

    response = client.get("/categories")

    assert response.status_code == 200
    assert len(response.json()) == 2


@patch("services.categories_service.CategoriesService.get_instance")
def test_get_categories_empty(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_categories.return_value = []

    response = client.get("/categories")

    assert response.status_code == 200
    assert len(response.json()) == 0


@patch("services.products_service.ProductsService.get_instance")
def test_get_products_no_filters(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_products.return_value = mock_db_products

    response = client.get("/products")

    assert response.status_code == 200
    assert len(response.json()) == 2


@patch("services.products_service.ProductsService.get_instance")
def test_get_products_with_filters(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_products.return_value = mock_db_products
    category_id = "1233435"
    query = "bolsa"
    min_price = 100
    max_price = 200

    response = client.get(
        f"/products?category_id={category_id}&query={query}&min_price={min_price}&max_price={max_price}")

    mock_service.get_products.assert_called_once_with(
        category_id, query, min_price, max_price)

    assert response.status_code == 200
    assert len(response.json()) == len(mock_db_products)


@patch("services.categories_service.CategoriesService.get_instance")
def test_update_category(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    updated_category = Category(
        id=mock_db_category.id,
        name="Bolsas",
        image=mock_db_category.image
    )

    mock_service.update_category.return_value = updated_category

    response = client.patch(f"/categories/{mock_db_category.id}", json={
        "name": "Bolsas"
    })

    mock_service.update_category.assert_called_once_with(category_id=str(
        mock_db_category.id), category_patch_dto=CategoryPatchDto(name="Bolsas"))
    assert response.status_code == 200
    assert response.json() == CategoryFullDto.from_model(updated_category).dict()
