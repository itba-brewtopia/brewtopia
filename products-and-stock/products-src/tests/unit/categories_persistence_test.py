import unittest
from unittest.mock import MagicMock, patch
from tests.mocks.db_mocks import assign_mock_id, mock_db_category, mock_category, mock_post_category, mock_db_categories
from persistence.categories_persistence import CategoriesPersistence

from models.category import Category

@patch("mongoengine.connect")
class TestCategoriesPersistence(unittest.TestCase):
    @patch.object(Category, 'objects')
    def test_get_category_by_id(self, mock_objects, _):
        expected = MagicMock(id=mock_db_category.id)
        mock_objects.return_value.first.return_value = expected
        persistence = CategoriesPersistence()
        category = persistence.get_category_by_id(expected.id)

        self.assertEqual(category.id, expected.id)

    @patch.object(Category, 'objects')
    def test_get_category_by_id_not_found(self, mock_objects, _):
        expected = None
        mock_objects.return_value.first.return_value = expected
        persistence = CategoriesPersistence()
        category = persistence.get_category_by_id(mock_category.id)

        self.assertEqual(category, expected)
    

    @patch.object(Category, 'objects')
    def test_get_category_by_id_invalid(self, mock_objects, _):
        expected = None
        persistence = CategoriesPersistence()
        category = persistence.get_category_by_id(mock_category.id[0:9])

        self.assertEqual(category, expected)

    @patch.object(Category, "save", autospec=True )
    def test_create_category(self, mock_save, _):
        expected = mock_db_category
        params = mock_post_category
        mock_save.side_effect = assign_mock_id
        # mock save method return value with instance of Category
        persistence = CategoriesPersistence()

        category = persistence.create_category(params)
        mock_save.assert_called_once()
        self.assertNotEqual(category.id, None)
        self.assertEqual(category.name, params.name)
        self.assertEqual(category.image, params.image)
        self.assertNotEqual(category.created_at, None)
        self.assertNotEqual(category.modified_at, None)
        self.assertEqual(category.is_deleted, False)

    @patch.object(Category, 'objects')
    def test_get_all_categories(self, mock_objects, _):
        expected = mock_db_categories
        mock_objects.return_value.all.return_value = expected
        persistence = CategoriesPersistence()
        categories = persistence.get_categories()

        mock_objects.return_value.all.assert_called_once()
        self.assertEqual(len(categories), len(expected))
        self.assertEqual(categories[0].id, expected[0].id)

    @patch.object(Category, 'objects')
    def test_get_all_categories(self, mock_objects, _):
        expected = []
        mock_objects.return_value.all.return_value = expected
        persistence = CategoriesPersistence()
        categories = persistence.get_categories()

        mock_objects.return_value.all.assert_called_once()
        self.assertEqual(len(categories), len(expected))
    




