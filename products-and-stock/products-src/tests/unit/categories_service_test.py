import unittest
from unittest.mock import MagicMock, patch

from services.categories_service import CategoriesService
from tests.mocks.db_mocks import mock_db_category, mock_post_category, mock_category, mock_db_categories


class TestCategoriesService(unittest.TestCase):
    @patch("persistence.categories_persistence.CategoriesPersistence.get_instance", return_value=MagicMock())
    def test_get_category_by_id(self, mock_get_instance):
        mock_get_instance.return_value.get_category_by_id.return_value = mock_db_category

        categories_service = CategoriesService()

        category = categories_service.get_category_by_id(mock_db_category.id)

        self.assertEqual(category, mock_db_category)

    @patch("persistence.categories_persistence.CategoriesPersistence.get_instance", return_value=MagicMock())
    def test_get_category_by_id_not_found(self, mock_get_instance):
        mock_get_instance.return_value.get_category_by_id.return_value = None

        categories_service = CategoriesService()

        category = categories_service.get_category_by_id(mock_db_category.id)

        self.assertEqual(category, None)

    @patch("persistence.categories_persistence.CategoriesPersistence.get_instance", return_value=MagicMock())
    def test_create_category(self, mock_get_instance):
        category_post = mock_post_category
        mock_get_instance.return_value.create_category.return_value = mock_db_category
        categories_service = CategoriesService()

        category = categories_service.create_category(category_post)

        self.assertNotEqual(category.id, None)
        self.assertEqual(category.name, category_post.name)
        self.assertEqual(category.image, category_post.image)
        self.assertNotEqual(category.created_at, None)
        self.assertNotEqual(category.modified_at, None)
        self.assertEqual(category.is_deleted, False)

    @patch("persistence.categories_persistence.CategoriesPersistence.get_instance", return_value=MagicMock())
    def test_get_categories(self, mock_get_instance):
        mock_get_instance.return_value.get_categories.return_value = mock_db_categories
        categories_service = CategoriesService()

        categories = categories_service.get_categories()

        mock_get_instance.return_value.get_categories.assert_called_once()
        self.assertEqual(len(categories), 2)
        self.assertEqual(categories[0], mock_db_category)

    @patch("persistence.categories_persistence.CategoriesPersistence.get_instance", return_value=MagicMock())
    def test_get_categories_empty(self, mock_get_instance):
        mock_get_instance.return_value.get_categories.return_value = []
        categories_service = CategoriesService()

        categories = categories_service.get_categories()

        mock_get_instance.return_value.get_categories.assert_called_once()
        self.assertEqual(len(categories), 0)

        