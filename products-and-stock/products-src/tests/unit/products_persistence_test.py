

import unittest
from unittest.mock import MagicMock, patch

from models.category import Category

from persistence.products_persistence import ProductsPersistence

from tests.mocks.db_mocks import assign_mock_id, mock_post_product, mock_category, mock_db_product, mock_db_products

from models.product import Product

@patch("mongoengine.connect")
class TestProductsPersistence(unittest.TestCase):

    @patch.object(Product, "save", autospec=True)
    def test_create_product(self, mock_save, _):
        mock_save.side_effect = assign_mock_id
        post_product = mock_post_product
        product_category = mock_category

        product_persistence = ProductsPersistence()

        product = product_persistence.create_product(post_product, product_category)

        mock_save.assert_called_once()
        mock_save.call_args[0] == mock_db_product
        self.assertNotEqual(product.created_at, None)
        self.assertNotEqual(product.modified_at, None)
        self.assertEqual(product.created_at, product.modified_at)
        self.assertEqual(product.is_deleted, False)

    @patch.object(Product, 'objects')
    def test_get_product_by_id(self, mock_objects, _):
        expected = mock_db_product
        mock_objects.return_value.first.return_value = expected
        persistence = ProductsPersistence()
        product = persistence.get_product_by_id(expected.id)

        self.assertEqual(product.id, expected.id)
        mock_objects.assert_called_once()

    @patch.object(Product, 'objects')
    def test_get_products_no_filters(self, mock_objects, _):
        expected = mock_db_products
        mock_objects.return_value.all.return_value = expected
        persistence = ProductsPersistence()
        products = persistence.get_products()

        self.assertEqual(products, expected)
        mock_objects.assert_called_once()

    @patch.object(Product, 'objects')
    def test_get_products_with_invalid_category(self, mock_objects, _):

        persistence = ProductsPersistence()
        products = persistence.get_products(category_id="invalid")

        self.assertEqual(products, [])
        mock_objects.return_value.all.assert_not_called()
    
    @patch.object(Product, 'objects')
    def test_get_products_with_invalid_category_and_other_filters(self, mock_objects, _):

        persistence = ProductsPersistence()
        products = persistence.get_products(category_id="invalid", query="query", min_price=0)

        self.assertEqual(products, [])
        mock_objects.return_value.all.assert_not_called()

    @patch.object(Product, 'objects')
    def test_get_products_with_query_params(self, mock_objects, _):

        persistence = ProductsPersistence()
        products = persistence.get_products( query="query", min_price=100)

        mock_objects.assert_called_with(name__icontains="query", price__gte=100, is_deleted=False)
