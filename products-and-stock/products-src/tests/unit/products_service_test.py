

import unittest
from unittest.mock import MagicMock, patch
from services.products_service import ProductsService
from tests.mocks.db_mocks import mock_db_product, mock_post_product, mock_db_category, mock_category, mock_db_products, mock_central_store

class TestProductsService(unittest.TestCase):
    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    @patch("services.utils._get_central_store", return_value=MagicMock())
    @patch("services.categories_service.CategoriesService.get_instance", return_value=MagicMock())
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_create_product(self, mock_get_persistence_instance, mock_get_categories_service_instance, mock_get_central_store, mock_get_stock_persistence_instance):
        params = mock_post_product
        mock_get_persistence_instance.return_value.create_product.return_value = mock_db_product
        mock_get_categories_service_instance.return_value.get_category_by_id.return_value = mock_db_category
        mock_get_central_store.return_value = mock_central_store
        mock_get_stock_persistence_instance.return_value.create_stock.return_value = None
        
        products_service = ProductsService()

        response = products_service.create_product(params)

        if mock_get_central_store.return_value is not None:
            mock_get_stock_persistence_instance.return_value.create_stock.assert_called_with(mock_central_store['id'], str(mock_db_product.id), 2000, 1000)
        mock_get_persistence_instance.return_value.create_product.assert_called()
        mock_get_categories_service_instance.return_value.get_category_by_id.assert_called_with(params.category_id)
        mock_get_persistence_instance.return_value.create_product.call_args[0] == [mock_post_product, mock_category]


    @patch("services.categories_service.CategoriesService.get_instance", return_value=MagicMock())
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_create_product_invalid_category(self, mock_get_persistence_instance, mock_get_categories_service_instance):
        params = mock_post_product
        mock_get_persistence_instance.return_value.create_product.return_value = mock_db_product
        mock_get_categories_service_instance.return_value.get_category_by_id.return_value = None
        products_service = ProductsService()
        response = products_service.create_product(params)

        mock_get_persistence_instance.return_value.create_product.assert_not_called()
        mock_get_categories_service_instance.return_value.get_category_by_id.assert_called_with(params.category_id)
        self.assertEqual(response, None)
    
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_get_product_by_id(self, mock_get_persistence_instance):
        product_id = str(mock_db_product.id)
        mock_get_persistence_instance.return_value.get_product_by_id.return_value = mock_db_product

        products_service = ProductsService()
        response = products_service.get_product_by_id(product_id)
        mock_get_persistence_instance.return_value.get_product_by_id.assert_called_with(product_id)
        self.assertEqual(response, mock_db_product)

    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_get_product_by_id_not_found(self, mock_get_persistence_instance):
        product_id = str(mock_db_product.id)
        mock_get_persistence_instance.return_value.get_product_by_id.return_value = None

        products_service = ProductsService()
        response = products_service.get_product_by_id(product_id)
        mock_get_persistence_instance.return_value.get_product_by_id.assert_called_with(product_id)
        self.assertEqual(response, None)

    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_get_products(self, mock_get_persistence_instance):
        mock_get_persistence_instance.return_value.get_products.return_value = mock_db_products

        products_service = ProductsService()
        response = products_service.get_products()
        mock_get_persistence_instance.return_value.get_products.assert_called_with(None, None, None, None, False)
        self.assertEqual(len(response), 2)

    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_get_products_with_category_id(self, mock_get_persistence_instance):
        category_id = str(mock_db_category.id)
        mock_get_persistence_instance.return_value.get_products.return_value = mock_db_products

        products_service = ProductsService()
        response = products_service.get_products(category_id)
        mock_get_persistence_instance.return_value.get_products.assert_called_with(category_id, None, None, None, False)
        self.assertEqual(response, mock_db_products)



        
        