import os

from models.stock import Stock

from models.base import Base

from persistence.stock_persistence import StockPersistence

from models.category import Category

from models.product import Product
from mongoengine import connect, disconnect
from tests.mocks.db_mocks import mock_db_category, mock_db_categories, mock_db_products
import copy

database_name = os.environ.get('MONGO_INITDB_DATABASE')
database_user = os.environ.get('MONGO_INITDB_ROOT_USERNAME')
database_password = os.environ.get('MONGO_INITDB_ROOT_PASSWORD')


def setup_db():
    session = StockPersistence.get_instance().session
    engine = StockPersistence.get_instance().engine
    Base.metadata.create_all(bind=engine)
    session.close()
    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    Product.objects().delete()
    Category.objects().delete()
    disconnect()


def seed_product_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    
    products = [copy.deepcopy(prod) for prod in mock_db_products]
    
    Product.objects().delete()

    for prod in products:
        prod.save()
    disconnect()

def seed_category_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    
    categories = [copy.deepcopy(cat) for cat in mock_db_categories]
    
    Category.objects().delete()
    for cat in categories:
        cat.save()
    disconnect()


def drop_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    Product.objects().delete()
    Category.objects().delete()
    disconnect()
    session = StockPersistence.get_instance().session
    session.rollback()
    session.query(Stock).delete()
    session.commit()
   
    session.close()