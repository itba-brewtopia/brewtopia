from bson import ObjectId
from api.dto.input.product_post_dto import ProductPostDto
from api.dto.input.category_post_dto import CategoryPostDto
from models.product_category import ProductCategory
from models.product import Product
from models.category import Category
import datetime


mock_central_store = {
    "id": "5f4f3f2f1e0d0c0b0a090800",
    "name": "Central",

}

mock_category = ProductCategory(
    id='5f4f3f2f1e0d0c0b0a090806',
    name="Category 1",
    image="https://picsum.photos/200/300",
)


mock_db_product = Product(
    id=ObjectId('5f4f3f2f1e0d0c0b0a090807'),
    name="Product 1",
    description="Product 1 description",
    price=100.00,
    created_at=datetime.datetime.utcnow(),
    modified_at=datetime.datetime.utcnow(),
    category=mock_category,
    images=["https://picsum.photos/200/300"]
)

mock_db_products = [
    mock_db_product,
    Product(
        id=ObjectId('5f4f3f2f1e0d0c0b0a090808'),
        name="Product 2",
        description="Product 2 description",
        price=200.00,
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        category=mock_category,
        images=["https://picsum.photos/200/300"]
    )
]

mock_post_product = ProductPostDto(
    name="Product 1",
    description="Product 1 description",
    price=100.00,
    category_id='5f4f3f2f1e0d0c0b0a090806',
    images=["https://picsum.photos/200/300"],
)

mock_db_category = Category(
    id=ObjectId('5f4f3f2f1e0d0c0b0a090806'),
    name="Category 1",
    image="https://picsum.photos/200/300",
    created_at=datetime.datetime.utcnow(),
    modified_at=datetime.datetime.utcnow(),)

mock_db_categories = [
    mock_db_category,
    Category(
        id=ObjectId('5f4f3f2f1e0d0c0b0a090807'),
        name="Category 2",
        image="https://picsum.photos/200/300",
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),),
]


mock_post_category = CategoryPostDto(
   name="Category 1",
    image="https://picsum.photos/200/300",
)


def assign_mock_id(self):
    self.id = mock_db_category.id
    return self

# invalid_post_category = CategoryPostDto(
#     image="https://picsum.photos/200/300",
# )