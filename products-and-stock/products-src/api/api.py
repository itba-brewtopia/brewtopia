from fastapi import FastAPI
from .routers import products, categories

import uvicorn

app = FastAPI(openapi_url="/products/openapi.json",
                   docs_url="/products/docs", redoc_url="/products/redoc")

app.include_router(products.router)
app.include_router(categories.router)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
