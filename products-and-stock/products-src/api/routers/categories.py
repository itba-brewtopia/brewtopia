from fastapi import APIRouter, HTTPException
from api.dto.input.category_patch_dto import CategoryPatchDto
from services.categories_service import CategoriesService
from api.dto.category_full_dto import CategoryFullDto
from api.dto.input.category_post_dto import CategoryPostDto
from mongoengine.errors import ValidationError

router = APIRouter()


@router.get("/categories/health")
async def health_check():
    service_status = CategoriesService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@router.post("/categories", response_model=CategoryFullDto, status_code=201)
async def create_category(category: CategoryPostDto):
    try:
        category_model = CategoriesService.get_instance().create_category(category)
        response = CategoryFullDto.from_model(category_model)
        return response
    except ValidationError as e:
        exc = e.to_dict()
        k = exc.keys()
        detail = []
        for key in k:
            detail.append({
                "loc": ["body", key],
                "msg": exc[key],
                "type": "value_error.invalid"
            })
        raise HTTPException(status_code=422, detail=detail)


@router.get("/categories", response_model=list[CategoryFullDto])
async def get_categories():
    categories = CategoriesService.get_instance().get_categories()
    response = list(
        map(lambda category: CategoryFullDto.from_model(category), categories))
    return response


@router.get("/categories/{category_id}", response_model=CategoryFullDto)
async def get_category_by_id(category_id: str):
    category_model = CategoriesService.get_instance().get_category_by_id(category_id)
    if category_model is None:
        raise HTTPException(status_code=404, detail="Category not found")
    response = CategoryFullDto.from_model(category_model)
    return response


@router.patch("/categories/{category_id}", response_model=CategoryFullDto)
async def update_category(category_id: str, category_patch_dto: CategoryPatchDto):
    category_model = CategoriesService.get_instance().update_category(
        category_id=category_id, category_patch_dto=category_patch_dto)
    if category_model is None:
        raise HTTPException(status_code=404, detail="Category not found")
    response = CategoryFullDto.from_model(category_model)
    return response
