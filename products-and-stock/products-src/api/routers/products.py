from fastapi import APIRouter, HTTPException
from api.dto.input.product_patch_dto import ProductPatchDto
from services.products_service import ProductsService
from api.dto.product_full_dto import ProductFullDto
from api.dto.input.product_post_dto import ProductPostDto

router = APIRouter()


@router.get("/products/health")
async def health_check():
    service_status = ProductsService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@router.post("/products", response_model=ProductFullDto, status_code=201)
async def create_product(product: ProductPostDto):
    product_model = ProductsService.get_instance().create_product(product)
    if product_model:
        response = ProductFullDto.from_model(product_model)
        return response
    else:
        raise HTTPException(status_code=422, detail="Invalid category id")


@router.get("/products/{product_id}", response_model=ProductFullDto)
async def get_product_by_id(product_id: str):
    product_model = ProductsService.get_instance().get_product_by_id(product_id)
    if product_model:
        response = ProductFullDto.from_model(product_model)
        return response
    else:
        raise HTTPException(status_code=404, detail="Product not found")


@router.get("/products", response_model=list[ProductFullDto])
async def get_products(category_id: str = None, query: str = None, min_price: float = None, max_price: float = None):
    products = ProductsService.get_instance().get_products(
        category_id, query, min_price, max_price)
    response = [ProductFullDto.from_model(product) for product in products]
    return response


@router.patch("/products/{product_id}", response_model=ProductFullDto)
async def update_product(product_id: str, product_patch_dto: ProductPatchDto):
    try:
        product_model = ProductsService.get_instance().update_product(
            product_id=product_id, product_patch_dto=product_patch_dto)
    except Exception as e:
        raise HTTPException(status_code=422, detail=str(e))
    if product_model:
        response = ProductFullDto.from_model(product_model)
        return response
    else:
        raise HTTPException(status_code=404, detail="Product not found")
