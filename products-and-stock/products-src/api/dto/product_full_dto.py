

from models.product import Product
from api.dto.product_category_dto import ProductCategoryDto
from pydantic import BaseModel


class ProductFullDto(BaseModel):
    id: str
    name: str
    description: str = None
    price: str
    created_at: str
    modified_at: str
    is_deleted: bool = False
    category: ProductCategoryDto
    images: list[str]

    @staticmethod
    def from_model(product: Product):
        return ProductFullDto(
            id=str(product.id),
            name=product.name,
            description=product.description,
            price=str(product.price),
            created_at=str(product.created_at),
            modified_at=str(product.modified_at),
            category=ProductCategoryDto.from_model(product.category),
            is_deleted=product.is_deleted,
            images=product.images
        )

   