from typing import Optional

from pydantic import BaseModel


class CategoryPatchDto(BaseModel):
    name: Optional[str]
    image: Optional[str]
    is_deleted: Optional[bool]