from pydantic import BaseModel


class CategoryPostDto(BaseModel):
    name: str
    image: str