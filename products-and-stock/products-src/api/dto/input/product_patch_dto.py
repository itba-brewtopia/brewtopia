from typing import List, Optional

from api.dto.product_category_dto import ProductCategoryDto
from pydantic import BaseModel


class ProductPatchDto(BaseModel):
    name: Optional[str]
    description: Optional[str]
    price: Optional[str]
    category_id: Optional[str]
    images: Optional[List[str]]
    is_deleted: Optional[bool]
    category: Optional[ProductCategoryDto]
