

from typing import List
from pydantic import BaseModel


class ProductPostDto(BaseModel):
    name:str
    description:str | None
    category_id:str
    price: float
    images:List[str]

    def __eq__(self, __value: object) -> bool:
        return self.name == __value.name and self.description == __value.description and self.category_id == __value.category_id and self.images == __value.images