

from pydantic import BaseModel


class ProductCategoryDto(BaseModel):
    id: str
    name: str
    image:str

    @staticmethod
    def from_model(category):
        return ProductCategoryDto(
            id=str(category.id),
            name=category.name,
            image=category.image
        )
