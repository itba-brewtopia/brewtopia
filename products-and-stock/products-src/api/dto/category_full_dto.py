from pydantic import BaseModel
from models.category import Category

class CategoryFullDto(BaseModel):
    id: str
    name: str
    image: str
    created_at: str
    modified_at: str
    is_deleted: bool = False

    @staticmethod
    def from_model(category:Category):
        return CategoryFullDto(
            id=str(category.id),
            name=category.name,
            image=category.image,
            created_at=str(category.created_at),
            modified_at=str(category.modified_at),
            is_deleted=category.is_deleted
        )

