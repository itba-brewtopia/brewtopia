from api.dto.input.category_patch_dto import CategoryPatchDto
from models.category import Category
from api.dto.input.category_post_dto import CategoryPostDto
from persistence.categories_persistence import CategoriesPersistence


class CategoriesService:
    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return CategoriesPersistence.get_instance().health_check()

    def create_category(self, category: CategoryPostDto) -> Category:
        return CategoriesPersistence.get_instance().create_category(category)

    def get_category_by_id(self, category_id: str) -> Category:
        return CategoriesPersistence.get_instance().get_category_by_id(category_id)

    def get_categories(self) -> list[Category]:
        return CategoriesPersistence.get_instance().get_categories()

    def update_category(self, category_id: str, category_patch_dto: CategoryPatchDto) -> Category:
        return CategoriesPersistence.get_instance().update_category(category_id=category_id, category_patch_dto=category_patch_dto)
