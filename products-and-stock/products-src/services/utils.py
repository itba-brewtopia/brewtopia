import requests


def _get_central_store():
    try: 
        response = requests.get(
            f'http://stores-service:8080/stores?alias=central')
        if response.status_code != 200:
            if response.status_code == 404:
                return None
            else:
                raise ValueError("Store server internal error")
        response_json = response.json()
        if len(response_json) == 0 or len(response_json) >1:
            return None
        return response_json[0]
    except ValueError:
        raise
    except Exception:
        raise Exception(
            'Stock server not available')