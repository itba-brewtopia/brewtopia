# from pydantic import ValidationError
from persistence.stock_persistence import StockPersistence
import services.utils as utils
from api.dto.product_category_dto import ProductCategoryDto
from api.dto.input.product_patch_dto import ProductPatchDto
from persistence.products_persistence import ProductsPersistence
from models.product_category import ProductCategory
from services.categories_service import CategoriesService


class ProductsService:
    _instance = None

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = ProductsService()
        return cls._instance

    def health_check(self):
        return ProductsPersistence.get_instance().health_check()

    def create_product(self, product):
        category = CategoriesService.get_instance().get_category_by_id(product.category_id)
        if (category):
            product_category = ProductCategory.from_model(category)
            db_product = ProductsPersistence.get_instance().create_product(product, product_category)
            # Create stock for product in central store
            central_store = utils._get_central_store()
            if central_store is not None:
                StockPersistence.get_instance().create_stock(
                    central_store['id'], str(db_product.id), 2000, 1000)
            return db_product
        else:
            return None

    def get_product_by_id(self, product_id):
        return ProductsPersistence.get_instance().get_product_by_id(product_id)

    def get_products(self, category_id: str = None, query: str = None, min_price: str = None, max_price: str = None, is_deleted: bool = False):
        return ProductsPersistence.get_instance().get_products(category_id, query, min_price, max_price, is_deleted)

    def update_product(self, product_id: str, product_patch_dto: ProductPatchDto):
        if product_patch_dto.category_id is not None:
            category = CategoriesService.get_instance(
            ).get_category_by_id(product_patch_dto.category_id)
            if category is None:
                raise Exception("invalid.category_id")
            product_patch_dto.category = ProductCategoryDto.from_model(
                category)
        return ProductsPersistence.get_instance().update_product(product_id, product_patch_dto)
