import os
from mongoengine import Document, StringField, URLField, BooleanField, DateTimeField
class Category(Document):
    meta = {
        'collection': 'category'+(os.environ.get("TABLE_SUFFIX")
                               if os.environ.get("TABLE_SUFFIX") is not None else "")}
    name = StringField(required=True, max_length=50)
    image = URLField(required=True, max_length=200)
    is_deleted = BooleanField(default=False)
    created_at = DateTimeField()
    modified_at = DateTimeField()

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "image": self.image,
            "is_deleted": self.is_deleted,
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.created_at else None,
            "modified_at": self.modified_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.modified_at else None,
        }


