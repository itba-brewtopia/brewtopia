
from mongoengine import EmbeddedDocument, StringField, URLField
from models.category import Category


class ProductCategory(EmbeddedDocument):
    id = StringField(required=True)
    name = StringField(required=True, max_length=50)
    image = URLField( max_length=200)

    @staticmethod
    def from_model(model: Category):
        return ProductCategory(
            id=str(model.id),
            name=model.name,
            image=model.image,
        )

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "image": self.image,
        }
    
    def __eq__(self, other):
        return self.id == other.id and self.name == other.name and self.image == other.image

