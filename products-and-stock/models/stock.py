import os
from models.base import Base
from sqlalchemy import Boolean, Column, String, Numeric, DateTime, Integer
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


class Stock(Base):
    __tablename__ = "stock" + (os.environ.get("TABLE_SUFFIX")
                               if os.environ.get("TABLE_SUFFIX") is not None else "")

    product_id = Column(String, primary_key=True)
    store_id = Column(String, primary_key=True)
    created_at = Column(DateTime, default=func.now())
    modified_at = Column(DateTime, default=func.now(), onupdate=func.now())
    quantity = Column(Integer, default=0)
    minimum_quantity = Column(Integer, default=0)
    is_deleted = Column(Boolean, default=False)
