import os
from mongoengine import Document, EmbeddedDocumentField, StringField, BooleanField, DateTimeField, ListField, URLField, Decimal128Field
from .category import Category
from .product_category import ProductCategory

class Product(Document):
    meta={
        'collection': 'products' + (os.environ.get("TABLE_SUFFIX")
                               if os.environ.get("TABLE_SUFFIX") is not None else "")
    }
    name = StringField(required=True, max_length=50)
    description = StringField(max_length=200)
    price = Decimal128Field(required=True, precision=2)
    is_deleted = BooleanField(default=False)
    created_at = DateTimeField()
    modified_at = DateTimeField()
    category = EmbeddedDocumentField(ProductCategory)
    images = ListField(URLField())
    # tags = 

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "description": self.description,
            "price": str(self.price),
            "is_deleted": self.is_deleted,
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.created_at else None,
            "modified_at": self.modified_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.modified_at else None,
            "category": self.category.to_dict(),
            "images": self.images
        }


