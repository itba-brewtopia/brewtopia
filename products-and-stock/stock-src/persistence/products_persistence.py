import os
import unittest
from bson import ObjectId

import mongoengine
from models.product import Product


class ProductsPersistence(unittest.TestCase):
    _instance = None

    def __init__(self):
        database_name = os.environ.get("MONGO_INITDB_DATABASE")
        database_user = os.environ.get("MONGO_INITDB_ROOT_USERNAME")
        database_password = os.environ.get("MONGO_INITDB_ROOT_PASSWORD")
        mongoengine.connect(
            uuidRepresentation="standard",
            db=database_name,
            username=database_user,
            password=database_password,
            host="products-and-stock-mongodb",
        )

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = cls()
        return cls._instance
    
    def get_product_by_id(self, product_id: str):
        if not ObjectId.is_valid(product_id):
            return None
        return Product.objects(id=ObjectId(product_id)).first()