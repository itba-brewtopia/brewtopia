import os
from typing import List, Tuple

from func_timeout import FunctionTimedOut, func_timeout

from exceptions.insufficient_stock_error import InsufficientStockError

from models.stock import Stock
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models.base import Base


class StockPersistence:

    _instance = None

    def __init__(self, session=None):
        if session is not None:
            self.session = session
            return
        database_name = os.environ.get('POSTGRES_DB')
        database_user = os.environ.get('POSTGRES_USER')
        database_password = os.environ.get('POSTGRES_PASSWORD')
        engine = create_engine(
            f'postgresql://{database_user}:{database_password}@products-and-stock-psql:5432/{database_name}')
        self.session = sessionmaker(
            autocommit=False, autoflush=False, bind=engine)()
        # Creates the tables if they don't exist
        Base.metadata.create_all(bind=engine)

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: self.session.query(Stock).first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def edit_stock_item(self, patch: StockPatchDto):
        modified = False
        stock = self.session.get(Stock, (patch.product_id, patch.store_id))
        if stock is None:
            if patch.action == StockPatchDtoAction.DECREMENT.value:
                stock = Stock(
                    product_id=patch.product_id,
                    store_id=patch.store_id,
                    quantity=0,
                    minimum_quantity=0,
                )
                self.session.add(stock)
                self.session.commit()
                self.session.refresh(stock)
            else: 
                # Crear stock
                stock = Stock(
                    product_id=patch.product_id,
                    store_id=patch.store_id,
                    quantity=patch.quantity,
                    minimum_quantity=patch.minimum_quantity if patch.minimum_quantity is not None else patch.quantity,
                )
                self.session.add(stock)
                self.session.commit()
                self.session.refresh(stock)
                return stock
        if patch.is_deleted == True:
            stock.is_deleted = True
        else:
            if patch.minimum_quantity is not None:
                stock.minimum_quantity = patch.minimum_quantity
                modified = True
            if patch.is_deleted == False:
                stock.is_deleted = patch.is_deleted
            if patch.action is not None:
                # switch case
                if patch.action == StockPatchDtoAction.CHANGE.value:
                    modified = True
                    stock.quantity = patch.quantity
                elif patch.action == StockPatchDtoAction.INCREMENT.value:
                    modified = True
                    stock.quantity += patch.quantity
                elif patch.action == StockPatchDtoAction.DECREMENT.value:
                    if stock.quantity - patch.quantity < 0:
                        raise InsufficientStockError("Insufficient stock")
                    modified = True
                    stock.quantity -= patch.quantity
            # Si se modifica el stock y estaba marcado como deleted lo marcamos como que no esta deleted
            if modified and stock.is_deleted == True:
                stock.is_deleted = False
        self.session.commit()
        self.session.refresh(stock)
        return stock

    def get_stock_item(self, product_id: str, store_id: str):
        stock = self.session.get(Stock, (product_id, store_id))
        return stock

    def get_all_stock(self):
        return self.session.query(Stock).all()

    def get_store_stock(self, store_id: str):
        return self.session.query(Stock).filter(Stock.store_id == store_id).all()

    def get_product_stock(self, product_id: str):
        return self.session.query(Stock).filter(Stock.product_id == product_id).all()
