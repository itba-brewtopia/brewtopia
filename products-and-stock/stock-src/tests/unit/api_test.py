import datetime
from unittest.mock import MagicMock, patch

from api.dto.input.stock_patch_dto import StockPatchDtoAction
from api.dto.stock_get_dto import StockGetDto
from api.dto.stock_item_dto import StockItemDto
from api.stock_api import app
from fastapi.testclient import TestClient
from exceptions.insufficient_stock_error import InsufficientStockError
from exceptions.not_found_error import NotFoundError
from tests.mocks.db_mocks import mock_patch_stock, mock_db_stock, mock_db_stocks


client = TestClient(app)


@patch('services.stock_service.StockService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/stock/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


# Tests for PATCH /stock
@patch("services.stock_service.StockService.get_instance")
def test_edit_stock_item_quantity(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    params = mock_patch_stock
    params.quantity = mock_db_stock.quantity
    mock_service.edit_stock_item.return_value = mock_db_stock
    response = client.patch("/stock", json=params.dict())
    mock_service.edit_stock_item.assert_called_once_with(params)
    assert response.json()['quantity'] == params.quantity
    assert response.status_code == 200
    assert response.json()['product_id'] == str(params.product_id)
    assert response.json()['store_id'] == str(params.store_id)


@patch("services.stock_service.StockService.get_instance")
def test_edit_stock_item_invalid_params(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    params = {
        "product_id": "5f4f3f2f1e0d0c0b0a090807",
        "store_id": "5a4f3b2f1e0d0c0b0a090900",
        "action": StockPatchDtoAction.CHANGE.value
    }
    mock_service.edit_stock_item.return_value = mock_db_stock
    response = client.patch("/stock", json=params)
    assert response.status_code == 422
    assert response.json()[
        'detail'] == "Invalid parameter: quantity is required when action is CHANGE"

@patch("services.stock_service.StockService.get_instance")
def test_get_all_stock(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.get_stock_items.return_value = mock_db_stocks
    response = client.get("/stock")
    assert response.status_code == 200
    assert len(response.json()) == len(mock_db_stocks)
    mock_service.get_stock_items.assert_called_once_with(None, None)


@patch("services.stock_service.StockService.get_instance")
def test_get_empty_stock(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.get_stock_items.return_value = []
    response = client.get("/stock")
    assert response.status_code == 200
    assert len(response.json()) == 0
    mock_service.get_stock_items.assert_called_once_with(None, None)

@patch("services.stock_service.StockService.get_instance")
def test_get_stock_by_store_id(mock_get_instance):
    mock_service = MagicMock()
    store_id = mock_db_stock.store_id
    mock_get_instance.return_value = mock_service
    mock_service.get_stock_items.return_value = [item for item in mock_db_stocks if item.store_id == store_id]
    response = client.get(f"/stock?store_id={store_id}")
    assert response.status_code == 200
    assert len(response.json()) != len(mock_db_stocks)
    assert all(item['store_id'] == store_id for item in response.json())
    mock_service.get_stock_items.assert_called_once_with(store_id, None)

@patch("services.stock_service.StockService.get_instance")
def test_get_stock_by_product_id(mock_get_instance):
    mock_service = MagicMock()
    product_id = mock_db_stock.product_id
    mock_get_instance.return_value = mock_service
    mock_service.get_stock_items.return_value = [item for item in mock_db_stocks if item.product_id == product_id]
    response = client.get(f"/stock?product_id={product_id}")
    assert response.status_code == 200
    assert len(response.json()) != len(mock_db_stocks)
    assert all(item['product_id'] == product_id for item in response.json())
    mock_service.get_stock_items.assert_called_once_with(None, product_id)

@patch("services.stock_service.StockService.get_instance")
def test_get_available_date(mock_get_instance):
    get_dto = StockGetDto(
        store_id=mock_db_stock.store_id,
        items=[
            StockItemDto(
                product_id=mock_db_stock.product_id,
                quantity=mock_db_stock.quantity-1
            )
        ]
    )
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.get_date_for_stock.return_value = datetime.datetime.now()
    response = client.post("/stock/available", json=get_dto.dict())

    assert response.status_code == 200
    mock_service.get_date_for_stock.assert_called_once_with(get_dto)

@patch("services.stock_service.StockService.get_instance")
def test_not_found_error(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.edit_stock_item.side_effect = NotFoundError("Product not found")
    response = client.patch("/stock",  json=mock_patch_stock.dict())
    assert response.status_code == 404
    assert response.json()['detail'] == "Product not found"

@patch("services.stock_service.StockService.get_instance")
def test_insufficient_stock_error(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.edit_stock_item.side_effect = InsufficientStockError("Insufficient stock")
    response = client.patch("/stock",  json=mock_patch_stock.dict())
    assert response.status_code == 409


@patch("services.stock_service.StockService.get_instance")
def test_insufficient_stock_error(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.edit_stock_item.side_effect = ValueError("Value error")
    response = client.patch("/stock", json=mock_patch_stock.dict())
    assert response.status_code == 503

@patch("services.stock_service.StockService.get_instance")
def test_restock_success(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.restock_items.return_value = None
    response = client.post("/stock/restock", json={})
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}