from copy import deepcopy
import unittest
from mock_alchemy.mocking import UnifiedAlchemyMagicMock, AlchemyMagicMock
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction
from exceptions.insufficient_stock_error import InsufficientStockError
from models.stock import Stock

from persistence.stock_persistence import StockPersistence
from tests.mocks.db_mocks import mock_db_stock, mock_db_stocks


class TestStockPersistence(unittest.TestCase):

    def test_get_stock_item(self):
        session = AlchemyMagicMock()
        session.get.return_value = mock_db_stock
        persistence = StockPersistence(session)
        stock = persistence.get_stock_item(mock_db_stock.product_id, mock_db_stock.store_id)
        
        self.assertEqual(stock, mock_db_stock)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))

    def test_get_stock_item_not_found(self):
        session = AlchemyMagicMock()
        session.get.return_value = None
        persistence = StockPersistence(session)
        stock = persistence.get_stock_item(mock_db_stock.product_id, mock_db_stock.store_id)
        
        self.assertIsNone(stock)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))

    def test_get_all_stock(self):
        session = AlchemyMagicMock()
        session.query.return_value.all.return_value = mock_db_stocks
        persistence = StockPersistence(session)
        stock = persistence.get_all_stock()
        
        self.assertEqual(stock, mock_db_stocks)
        session.query.assert_called_once_with(Stock)
        session.query.return_value.all.assert_called_once()
    
    def test_get_empty_stock(self):
        session = AlchemyMagicMock()
        session.query.return_value.all.return_value = []
        persistence = StockPersistence(session)
        stock = persistence.get_all_stock()
        
        self.assertEqual(stock, [])
        session.query.assert_called_once_with(Stock)
        session.query.return_value.all.assert_called_once()

    def test_get_store_stock(self):
        session = AlchemyMagicMock()
        store_id = mock_db_stock.store_id
        session.query.return_value.filter.return_value.all.return_value =  [item for item in mock_db_stocks if item.store_id == store_id]
        persistence = StockPersistence(session)
        stock = persistence.get_store_stock(mock_db_stock.store_id)
        self.assertNotEqual(stock, mock_db_stocks)
        assert all([item.store_id == mock_db_stock.store_id for item in stock])
        session.query.return_value.filter.assert_called_once_with(Stock.store_id == store_id)
    
    def test_get_empty_store_stock(self):
        session = AlchemyMagicMock()
        store_id = mock_db_stock.store_id
        session.query.return_value.filter.return_value.all.return_value =  []
        persistence = StockPersistence(session)
        stock = persistence.get_store_stock(store_id)
        self.assertEqual(stock, [])
        session.query.return_value.filter.assert_called_once_with(Stock.store_id == store_id)

    def test_get_product_stock(self):
        session = AlchemyMagicMock()
        product_id = mock_db_stock.product_id
        session.query.return_value.filter.return_value.all.return_value =  [item for item in mock_db_stocks if item.product_id == product_id]
        persistence = StockPersistence(session)
        stock = persistence.get_product_stock(mock_db_stock.product_id)
        self.assertNotEqual(stock, mock_db_stocks)
        assert all([item.product_id == mock_db_stock.product_id for item in stock])
        session.query.return_value.filter.assert_called_once_with(Stock.product_id == product_id)

    def test_get_empty_product_stock(self):
        session = AlchemyMagicMock()
        product_id = mock_db_stock.product_id
        session.query.return_value.filter.return_value.all.return_value =  []
        persistence = StockPersistence(session)
        stock = persistence.get_product_stock(product_id)
        self.assertEqual(stock, [])
        session.query.return_value.filter.assert_called_once_with(Stock.product_id == product_id)
    
    def test_edit_stock_item_decrement(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.DECREMENT.value,
            quantity=2
        )
        session = AlchemyMagicMock()
        session.get.return_value = mock_db_stock
        initial_quantity = mock_db_stock.quantity
        mock_db_stock_copy = deepcopy(mock_db_stock)
        persistence = StockPersistence(session)
        stock = persistence.edit_stock_item(params)

        self.assertNotEqual(stock, mock_db_stock_copy)
        self.assertEqual(stock.quantity, initial_quantity - params.quantity)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.commit.assert_called_once()

    def test_edit_stock_item_increment(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.INCREMENT.value,
            quantity=2
        )
        session = AlchemyMagicMock()
        session.get.return_value = mock_db_stock
        initial_quantity = mock_db_stock.quantity
        mock_db_stock_copy = deepcopy(mock_db_stock)
        persistence = StockPersistence(session)
        stock = persistence.edit_stock_item(params)

        self.assertNotEqual(stock, mock_db_stock_copy)
        self.assertEqual(stock.quantity, initial_quantity + params.quantity)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.commit.assert_called_once()

    def test_edit_stock_item_change(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.CHANGE.value,
            quantity=2
        )
        session = AlchemyMagicMock()
        session.get.return_value = mock_db_stock
        mock_db_stock_copy = deepcopy(mock_db_stock)
        persistence = StockPersistence(session)
        stock = persistence.edit_stock_item(params)

        self.assertNotEqual(stock, mock_db_stock_copy)
        self.assertEqual(stock.quantity, params.quantity)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.commit.assert_called_once()

    def test_edit_stock_item_insufficient_stock(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.DECREMENT.value,
            quantity=mock_db_stock.quantity + 1
        )
        session = AlchemyMagicMock()
        session.get.return_value = mock_db_stock
        persistence = StockPersistence(session)
        with self.assertRaises(InsufficientStockError):
            persistence.edit_stock_item(params)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.commit.assert_not_called()

    def test_edit_stock_item_create(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.CHANGE.value,
            quantity=mock_db_stock.quantity,
            minimum_quantity=mock_db_stock.minimum_quantity
        )
        session = AlchemyMagicMock()
        session.get.return_value = None
        persistence = StockPersistence(session)
        stock = persistence.edit_stock_item(params)

        self.assertEqual(stock.product_id, params.product_id)
        self.assertEqual(stock.store_id, params.store_id)
        self.assertEqual(stock.quantity, params.quantity)
        self.assertEqual(stock.minimum_quantity, params.minimum_quantity)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.add.assert_called_once_with(stock)
        session.commit.assert_called_once()

    def test_edit_stock_item_create_decrement(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.DECREMENT.value,
            quantity=mock_db_stock.quantity,
            minimum_quantity=mock_db_stock.minimum_quantity
        )
        session = AlchemyMagicMock()
        session.get.return_value = None
        persistence = StockPersistence(session)
        with self.assertRaises(InsufficientStockError):
            persistence.edit_stock_item(params)

        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.add.assert_called_once()
        session.commit.assert_called_once()

    def test_edit_deleted_stock(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.CHANGE.value,
            quantity=mock_db_stock.quantity +1,
            minimum_quantity=mock_db_stock.minimum_quantity + 1,
        )
        session = AlchemyMagicMock()
        mock_db_stock.is_deleted = True
        session.get.return_value = mock_db_stock
        mock_db_stock_copy = deepcopy(mock_db_stock)
        persistence = StockPersistence(session)
        stock = persistence.edit_stock_item(params)

        self.assertNotEqual(stock, mock_db_stock_copy)
        self.assertEqual(stock.is_deleted, False)
        self.assertEqual(stock.quantity, params.quantity)
        self.assertEqual(stock.minimum_quantity, params.minimum_quantity)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.commit.assert_called_once()

    def test_delete_stock(self):
        params = StockPatchDto(
            product_id=mock_db_stock.product_id,
            store_id=mock_db_stock.store_id,
            action=StockPatchDtoAction.CHANGE.value,
            quantity=mock_db_stock.quantity +1,
            minimum_quantity=mock_db_stock.minimum_quantity + 1,
            is_deleted=True
        )
        session = AlchemyMagicMock()
        session.get.return_value = mock_db_stock
        persistence = StockPersistence(session)
        stock = persistence.edit_stock_item(params)

        self.assertEqual(stock, mock_db_stock)
        self.assertEqual(stock.is_deleted, True)
        self.assertNotEqual(stock.quantity, params.quantity)
        self.assertNotEqual(stock.minimum_quantity, params.minimum_quantity)
        session.get.assert_called_once_with(Stock, (mock_db_stock.product_id, mock_db_stock.store_id))
        session.commit.assert_called_once()


        




 

