import datetime
import json
import unittest
from unittest.mock import MagicMock, Mock, call, patch
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction
from api.dto.stock_get_dto import StockGetDto
from exceptions.insufficient_stock_error import InsufficientStockError
from exceptions.not_found_error import NotFoundError
from services.stock_service import StockService
from services.utils import on_day
from tests.mocks.db_mocks import mock_db_stock, mock_patch_stock, mock_db_product, mock_db_stocks


class TestStockService(unittest.TestCase):
    @patch("services.utils.check_store_id", return_value=MagicMock())
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_edit_stock_item(self, mock_get_persistence_instance, mock_get_products_persistence_instance, mock_check_store_id):
        mock_get_persistence_instance.return_value.edit_stock_item.return_value = mock_db_stock
        mock_get_products_persistence_instance.return_value.get_product_by_id.return_value = mock_db_product
        stock_service = StockService()
        response = stock_service.edit_stock_item(mock_patch_stock)
        mock_check_store_id.assert_called_once_with(mock_patch_stock.store_id)
        mock_get_persistence_instance.return_value.edit_stock_item.assert_called_once_with(
            mock_patch_stock)
        mock_get_products_persistence_instance.return_value.get_product_by_id.assert_called_once_with(
            mock_patch_stock.product_id)
        self.assertEqual(response, mock_db_stock)

    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_edit_stock_item_product_not_found(self, mock_get_products_persistence_instance):
        # Prepare test data
        patch = mock_patch_stock

        stock_service = StockService()
        # Mock the get_product_by_id method to return None (product not found)
        mock_get_products_persistence_instance.return_value.get_product_by_id.return_value = None

        # Execute the function and verify that it raises a NotFoundError
        with self.assertRaises(NotFoundError):
            stock_service.edit_stock_item(patch)

        mock_get_products_persistence_instance.return_value.get_product_by_id.assert_called_once_with(
            patch.product_id)

    @patch("services.utils.check_store_id", return_value=MagicMock())
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_edit_stock_item_invalid_store_id(self, mock_get_products_persistence_instance, mock_check_store_id):
        # Prepare test data
        patch = mock_patch_stock

        mock_check_store_id.side_effect = NotFoundError("Store not found")
        mock_get_products_persistence_instance.return_value.get_product_by_id.return_value = mock_db_product

        stock_service = StockService()
        # Execute the function and verify that it raises an exception for an invalid store_id
        with self.assertRaises(NotFoundError):
            stock_service.edit_stock_item(patch)

        mock_get_products_persistence_instance.return_value.get_product_by_id.assert_called_once_with(
            patch.product_id)
        mock_check_store_id.assert_called_once_with(patch.store_id)

    @patch("services.utils.check_store_id", return_value=MagicMock())
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    def test_edit_stock_item_internal_store_error(self, mock_get_products_persistence_instance, mock_check_store_id):
        # Prepare test data
        patch = mock_patch_stock

        mock_check_store_id.side_effect = ValueError(
            "Store server internal error")
        mock_get_products_persistence_instance.return_value.get_product_by_id.return_value = mock_db_product

        stock_service = StockService()
        # Execute the function and verify that it raises an exception for an invalid store_id
        with self.assertRaises(ValueError):
            stock_service.edit_stock_item(patch)

        mock_get_products_persistence_instance.return_value.get_product_by_id.assert_called_once_with(
            patch.product_id)
        mock_check_store_id.assert_called_once_with(patch.store_id)

    @patch("services.utils.check_store_id", return_value=MagicMock())
    @patch("services.utils.get_central_store", return_value=json.loads('{"id": 1}'))
    @patch("persistence.products_persistence.ProductsPersistence.get_instance", return_value=MagicMock())
    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_edit_stock_item_insufficient_stock(self, mock_get_persistence_instance, mock_get_products_persistence_instance, mock_get_central_store, mock_check_store_id):
        # Prepare test data
        patch = mock_patch_stock
        mock_get_products_persistence_instance.return_value.get_product_by_id.return_value = mock_db_product

        # Mock the edit_stock_item method to raise an InsufficientStockError
        mock_get_persistence_instance.return_value.edit_stock_item.side_effect = InsufficientStockError(
            "Insufficient stock")

        # Execute the function and verify that it raises an InsufficientStockError
        with self.assertRaises(InsufficientStockError):
            stock_service = StockService()
            stock_service.edit_stock_item(patch)
        mock_get_products_persistence_instance.return_value.get_product_by_id.assert_called_once_with(
            patch.product_id)
        mock_check_store_id.assert_called_once_with(patch.store_id)
        # assert edit_stock_item was called twice, once for the store and once for the central store
        expected_calls = [call(patch), call(StockPatchDto(
            product_id=patch.product_id,
            store_id="1",  # Central store id
            action=StockPatchDtoAction.DECREMENT.value,
            quantity=patch.quantity
        ))]
        mock_get_persistence_instance.return_value.edit_stock_item.assert_has_calls(
            expected_calls)
        mock_get_central_store.assert_called_once()

    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_get_stock_items(self, mock_stock_persistence_instance):
        mock_stock_persistence_instance.return_value.get_all_stock.return_value = mock_db_stocks
        stock_service = StockService()
        response = stock_service.get_stock_items(None, None)
        mock_stock_persistence_instance.return_value.get_all_stock.assert_called_once()
        self.assertEqual(response, mock_db_stocks)

    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_get_stock_items_by_store(self, mock_stock_persistence_instance):
        store_id = mock_db_stock.store_id
        mock_stock_persistence_instance.return_value.get_store_stock.return_value = [item for item in mock_db_stocks if item.store_id == store_id]
        stock_service = StockService()
        response = stock_service.get_stock_items(store_id, None)
        mock_stock_persistence_instance.return_value.get_store_stock.assert_called_once_with(
            store_id)
        self.assertNotEqual(response, mock_db_stocks)
        assert all(item.store_id == store_id for item in response)

    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_get_stock_items_by_product(self, mock_stock_persistence_instance):
        product_id = mock_db_stock.product_id
        mock_stock_persistence_instance.return_value.get_product_stock.return_value = [item for item in mock_db_stocks if item.product_id == product_id]
        stock_service = StockService()
        response = stock_service.get_stock_items(None, product_id)
        mock_stock_persistence_instance.return_value.get_product_stock.assert_called_once_with(
            product_id)
        self.assertNotEqual(response, mock_db_stocks)
        assert all(item.product_id == product_id for item in response)

    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_get_empty_stock(self, mock_stock_persistence_instance):
        mock_stock_persistence_instance.return_value.get_all_stock.return_value = []
        stock_service = StockService()
        response = stock_service.get_stock_items(None, None)
        self.assertEqual(response, [])

    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_get_date_for_stock(self, mock_stock_persistence_instance):
        get_params = StockGetDto(
            store_id=mock_db_stock.store_id,
            items=[
                {
                    "product_id": mock_db_stock.product_id,
                    "quantity": mock_db_stock.quantity-1
                }
              
            ]
        )

        mock_stock_persistence_instance.return_value.get_stock_item.return_value = mock_db_stock

        stock_service = StockService()
        response = stock_service.get_date_for_stock(get_params)
        mock_stock_persistence_instance.return_value.get_stock_item.assert_called_once_with(get_params.items[0].product_id,
            get_params.store_id)
        self.assertEqual(response.date(), datetime.datetime.now().date())
    
    @patch("persistence.stock_persistence.StockPersistence.get_instance", return_value=MagicMock())
    def test_get_date_for_stock_insufficient(self, mock_stock_persistence_instance):
        get_params = StockGetDto(
            store_id=mock_db_stock.store_id,
            items=[
                {
                    "product_id": mock_db_stock.product_id,
                    "quantity": mock_db_stock.quantity-1
                }, 
                {
                    "product_id": "5f4f3f2f1e0d0c0b0a090801",
                    "quantity": 1
                }
            ]
        )

        mock_stock_persistence_instance.return_value.get_stock_item.side_effect = [mock_db_stock, None]

        stock_service = StockService()
        response = stock_service.get_date_for_stock(get_params)
        mock_stock_persistence_instance.return_value.get_stock_item.assert_has_calls([
            call(get_params.items[0].product_id, get_params.store_id),
            call(get_params.items[1].product_id, get_params.store_id)
        ])
        self.assertEqual(response.date(), on_day(datetime.datetime.now(), 1).date())        


        

    
