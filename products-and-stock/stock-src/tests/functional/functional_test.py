import pytest
from fastapi.testclient import TestClient

from unittest.mock import MagicMock, patch
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction
from api.dto.stock_full_dto import StockFullDto
from api.stock_api import app
from persistence.products_persistence import ProductsPersistence

from tests.utils.db_utils import drop_db, seed_db, seed_product_db, setup_db, drop_product_db
from tests.mocks.db_mocks import mock_db_stocks, mock_db_stock, mock_db_product


client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()
    drop_product_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown_function():
    ProductsPersistence._instance = None
    
    yield

    drop_db()
    drop_product_db()


def test_health_check():
    response = client.get("/stock/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_get_stock():
    seed_db()

    expected_response = [StockFullDto.from_model(stock) for stock in mock_db_stocks]

    response = client.get("/stock")
    assert response.status_code == 200
    assert response.json() == expected_response

def test_get_stock_by_store():
    seed_db()
    store_id = mock_db_stock.store_id
    expected_response = [StockFullDto.from_model(stock) for stock in mock_db_stocks if stock.store_id == store_id]

    response = client.get(f"/stock?store_id={store_id}")
    assert response.status_code == 200
    assert response.json() == expected_response

def test_get_stock_by_product():
    seed_db()
    product_id = mock_db_stock.product_id
    expected_response = [StockFullDto.from_model(stock) for stock in mock_db_stocks if stock.product_id == product_id]

    response = client.get(f"/stock?product_id={product_id}")
    assert response.status_code == 200
    assert response.json() == expected_response

def test_get_stock_by_store_and_product():
    seed_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id
    expected_response = [StockFullDto.from_model(stock) for stock in mock_db_stocks if stock.store_id == store_id and stock.product_id == product_id]

    response = client.get(f"/stock?store_id={store_id}&product_id={product_id}")
    assert response.status_code == 200
    assert response.json() == expected_response

@patch("requests.get", return_value=MagicMock(status_code=404))
def test_get_stock_store_not_found(mock_get_store_by_id):
    seed_db()
    seed_product_db()
    #van al revés para que no lo encuentre
    store_id = "aaaaaaaaaaaaaaaaaaaaaaaaa"
    product_id = mock_db_stock.product_id

    response = client.get(f"/stock?store_id={store_id}&product_id={product_id}")
    assert response.status_code == 404
    assert response.json() == {"detail": "Store not found"}

@patch("services.utils.check_store_id", return_value=MagicMock())
def test_get_stock_product_not_found(mock_get_store_by_id):
    seed_db()
    mock_get_store_by_id.return_value = None
    store_id = mock_db_stock.store_id
    product_id = "aaaaaaaaaaaaaaaaa"

    response = client.get(f"/stock?store_id={store_id}&product_id={product_id}")
    mock_get_store_by_id.assert_not_called()
    assert response.status_code == 404
    assert response.json() == {"detail": "Product not found"}

@patch("services.utils.check_store_id", return_value=MagicMock())
def test_edit_stock_item_increment(mock_check_store_id):
    seed_db()
    seed_product_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id

    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        action=StockPatchDtoAction.INCREMENT.value,
        quantity=5
    )

    response = client.patch("/stock", json=patch.dict())
    mock_check_store_id.assert_called_once_with(store_id)
    assert response.status_code == 200
    json = response.json()
    assert json["store_id"] == store_id
    assert json["product_id"] == product_id
    assert json["quantity"] == mock_db_stock.quantity + patch.quantity
  

@patch("services.utils.check_store_id", return_value=MagicMock())
def test_edit_stock_item_change(mock_check_store_id):
    seed_db()
    seed_product_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id

    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        action=StockPatchDtoAction.CHANGE.value,
        quantity=5
    )

    response = client.patch("/stock", json=patch.dict())
    mock_check_store_id.assert_called_once_with(store_id)
    assert response.status_code == 200
    json = response.json()
    assert json["store_id"] == store_id
    assert json["product_id"] == product_id
    assert json["quantity"] == patch.quantity

@patch("services.utils.check_store_id", return_value=MagicMock())
def test_edit_stock_item_decrement(mock_check_store_id):
    seed_db()
    seed_product_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id

    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        action=StockPatchDtoAction.DECREMENT.value,
        quantity=1
    )

    response = client.patch("/stock", json=patch.dict())
    mock_check_store_id.assert_called_once_with(store_id)
    assert response.status_code == 200
    json = response.json()
    assert json["store_id"] == store_id
    assert json["product_id"] == product_id
    assert json["quantity"] == mock_db_stock.quantity - patch.quantity

@patch("services.utils.create_stock_shipment", return_value=MagicMock())
@patch("services.utils.get_central_store", return_value={"id": '5a4f3b2f1e0d0c0b0a090901'})
@patch("services.utils.check_store_id", return_value=MagicMock())
def test_edit_stock_item_decrement_overflow(mock_check_store_id, mock_get_central_store, mock_create_stock_shipment):
    seed_db()
    seed_product_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id

    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        action=StockPatchDtoAction.DECREMENT.value,
        quantity=mock_db_stock.quantity + 1
    )

    response = client.patch("/stock", json=patch.dict())

    mock_check_store_id.assert_called_once_with(store_id)
    mock_get_central_store.assert_called_once()
    mock_create_stock_shipment.assert_called_once()
    assert response.status_code == 200


@patch("services.utils.check_store_id", return_value=MagicMock())
def test_edit_stock_item_invalid_operation(mock_check_store_id):
    seed_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id

    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        action=StockPatchDtoAction.CHANGE.value,
    )

    response = client.patch("/stock", json=patch.dict())
    mock_check_store_id.assert_not_called()
    assert response.status_code == 422
    

@patch("services.utils.check_store_id", return_value=MagicMock())
def test_edit_stock_item_minimum_quantity(mock_check_store_id):
    seed_db()
    seed_product_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id
    new_min_q = 50
    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        minimum_quantity=new_min_q
    )

    response = client.patch("/stock", json=patch.dict())
    mock_check_store_id.assert_called_once_with(store_id)
    assert response.status_code == 200
    json = response.json()
    assert json["store_id"] == store_id
    assert json["product_id"] == product_id
    assert json["minimum_quantity"] == new_min_q
    
@patch("services.utils.check_store_id", return_value=MagicMock())
def test_delete_stock_item(mock_check_store_id):
    seed_db()
    seed_product_db()
    store_id = mock_db_stock.store_id
    product_id = mock_db_stock.product_id
    new_min_q = 50
    patch = StockPatchDto(
        store_id=store_id,
        product_id=product_id,
        minimum_quantity=new_min_q,
        is_deleted=True
    )

    response = client.patch("/stock", json=patch.dict())
    mock_check_store_id.assert_called_once_with(store_id)
    assert response.status_code == 200
    json = response.json()
    assert json["store_id"] == store_id
    assert json["product_id"] == product_id
    assert json["is_deleted"] == True
    assert json["minimum_quantity"] != new_min_q