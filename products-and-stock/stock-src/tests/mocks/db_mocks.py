import datetime

from bson import ObjectId
from models.product_category import ProductCategory

from models.product import Product
from models.stock import Stock
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction

mock_db_stocks = [
    Stock(
        product_id='5f4f3f2f1e0d0c0b0a090807',
        store_id='5a4f3b2f1e0d0c0b0a090900',
        quantity=10,
        minimum_quantity=3,
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        is_deleted=False
    ),
    Stock(
        product_id='5f4f3f2f1e0d0c0b0a090808',
        store_id='5a4f3b2f1e0d0c0b0a090900',
        quantity=10,
        minimum_quantity=3,
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        is_deleted=False
    ),
    Stock(
        product_id='5f4f3f2f1e0d0c0b0a090807',
        store_id='5a4f3b2f1e0d0c0b0a090901',
        quantity=20,
        minimum_quantity=11,
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        is_deleted=False
    ),
    Stock(
        product_id='5f4f3f2f1e0d0c0b0a090801',
        store_id='5a4f3b2f1e0d0c0b0a090901',
        quantity=30,
        minimum_quantity=11,
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        is_deleted=False
    ),

]

mock_patch_stock = StockPatchDto(
    product_id='5f4f3f2f1e0d0c0b0a090807',
    store_id='5a4f3b2f1e0d0c0b0a090900',
    action=StockPatchDtoAction.CHANGE.value,
    quantity=6
)

mock_db_stock = Stock(
    product_id='5f4f3f2f1e0d0c0b0a090807',
    store_id='5a4f3b2f1e0d0c0b0a090900',
    quantity=10,
    minimum_quantity=3,
    created_at=datetime.datetime.utcnow(),
    modified_at=datetime.datetime.utcnow(),
    is_deleted=False
)

mock_category = ProductCategory(
    id='5f4f3f2f1e0d0c0b0a090806',
    name="Category 1",
    image="https://picsum.photos/200/300",
)

mock_db_product = Product(
    id=ObjectId('5f4f3f2f1e0d0c0b0a090807'),
    name="Product 1",
    description="Product 1 description",
    price=100.00,
    created_at=datetime.datetime.utcnow(),
    modified_at=datetime.datetime.utcnow(),
    category=mock_category,
    images=["https://picsum.photos/200/300"]
)


mock_db_products = [
    mock_db_product,
    Product(
        id=ObjectId('5f4f3f2f1e0d0c0b0a090808'),
        name="Product 2",
        description="Product 2 description",
        price=200.00,
        created_at=datetime.datetime.utcnow(),
        modified_at=datetime.datetime.utcnow(),
        category=mock_category,
        images=["https://picsum.photos/200/300"]
    )
]