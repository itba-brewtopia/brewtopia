import copy
import os
from models.product import Product
from models.stock import Stock
from persistence.stock_persistence import StockPersistence
from tests.mocks.db_mocks import mock_db_stocks, mock_db_stock, mock_db_products
from api.dto.stock_full_dto import StockFullDto
from mongoengine import connect, disconnect

database_name = os.environ.get('MONGO_INITDB_DATABASE')
database_user = os.environ.get('MONGO_INITDB_ROOT_USERNAME')
database_password = os.environ.get('MONGO_INITDB_ROOT_PASSWORD')

def setup_db():
    session = StockPersistence().get_instance().session
    session.query(Stock).delete()
    session.commit()
    session.close()

    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    Product.objects().delete()
    disconnect()


def seed_db():
    session = StockPersistence().get_instance().session
    for stock in mock_db_stocks:
        insert_stock = copy.deepcopy(stock)
        session.add(insert_stock)  # adds shipment and its items
        session.commit()

    session.commit()
    session.close()

def get_first_stock():
    session = StockPersistence().get_instance().session
    stock = session.query(Stock).first()
    stock_dto = StockFullDto.from_model(stock)
    session.close()
    return stock_dto

def seed_product_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    
    products = [copy.deepcopy(prod) for prod in mock_db_products]
    
    Product.objects().delete()

    for prod in products:
        prod.save()
    disconnect()

def drop_product_db():
    connect(db=database_name, username=database_user,
            password=database_password, host='products-and-stock-mongodb', uuidRepresentation="standard")
    Product.objects().delete()
    disconnect()


def drop_db():
    session = StockPersistence().get_instance().session
    # rollback any pending transaction, like errors, to avoid locking for future tests
    session.rollback()
    session.query(Stock).delete()
    session.commit()
    session.close()