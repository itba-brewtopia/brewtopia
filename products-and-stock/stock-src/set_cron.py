from crontab import CronTab

# Cron job to restock items every friday at 12:00
cron = CronTab(user="python")
job = cron.new(command='curl -X POST "http://localhost:8080/stock/restock"')
job.dow.on('FRI')
job.hour.on(12)
job.minute.on(0)
# job.minute.every(1)
job.enable()
cron.write()
