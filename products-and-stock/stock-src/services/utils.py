import datetime
from exceptions.not_found_error import NotFoundError
import requests

# Get date time for next monday


def on_day(date, day):
    """
    Returns the date of the next given weekday after
    the given date. For example, the date of next Monday.

    NB: if it IS the day we're looking for, this returns 0.
    consider then doing onDay(foo, day + 1).
    """
    days = (day - date.weekday() + 7) % 7
    return date + datetime.timedelta(days=days)


def check_store_id(store_id: str):
    response = requests.get(
        f'http://stores-service:8080/stores/{store_id}', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            raise NotFoundError("Store not found")
        else:
            raise Exception("Store server internal error")


def get_central_store():
    response = requests.get(
        f'http://stores-service:8080/stores?alias=CENTRAL', timeout=10)
    if response.status_code != 200:
        if response.status_code == 404:
            return None
        else:
            raise Exception("Store server internal error")
    response_json = response.json()
    if len(response_json) == 0 or len(response_json) > 1:
        return None
    return response_json[0]


def create_stock_shipment(store_id: str, central_store_id: str, items: list, restock: bool = False):
    response = requests.post(
        f'http://shipments-service:8080/store-shipments', timeout=10,
        json={
            "destination_store_id": store_id,
            "source_store_id": central_store_id,
            "estimated_ship_date": on_day(datetime.datetime.now(), 0).strftime("%Y-%m-%d"),
            "estimated_delivery_date": on_day(datetime.datetime.now(), 1).strftime("%Y-%m-%d"),
            "items": items,
            "restock": restock  # indicates if the shipment is for restocking or from a customer order
        }
    )
    if response.status_code != 201:
        raise Exception("Shipment server internal error " +
                        str(response.json()))
