import datetime
from typing import Optional
import services.utils as utils
from exceptions.insufficient_stock_error import InsufficientStockError
from persistence.stock_persistence import StockPersistence
import requests
from exceptions.not_found_error import NotFoundError
from persistence.products_persistence import ProductsPersistence
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction
from models.stock import Stock
import requests


class StockService():
    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return StockPersistence.get_instance().health_check()

    def edit_stock_item(self, patch: StockPatchDto):
        # Check if product exists
        product = ProductsPersistence.get_instance().get_product_by_id(patch.product_id)
        if product is None:
            raise NotFoundError("Product not found")
        
        # Check if store exists
        utils.check_store_id(patch.store_id)

        try:
            # Update or create stock item
            stock_item = StockPersistence.get_instance().edit_stock_item(patch)
            return stock_item
        except InsufficientStockError as e:
            # Get central store id
            central_store = utils.get_central_store()
            if central_store is None:
                raise NotFoundError("Central store not found, cannot restock")
            central_store_id = central_store['id']
            # Decrement central store stock
            central_store_stock_patch = StockPatchDto(
                product_id=patch.product_id,
                store_id=central_store_id,
                action=StockPatchDtoAction.DECREMENT.value,
                quantity=patch.quantity
            )
            try:
                central_stock_item = StockPersistence.get_instance().edit_stock_item(
                    central_store_stock_patch)
            except InsufficientStockError as e:
                raise InsufficientStockError(
                    "Insufficient stock in central store")
            # Create shipment
            utils.create_stock_shipment(
                patch.store_id, central_store_id, [{
                    "product_id": patch.product_id,
                    "quantity": patch.quantity
                }])
            stock_items = self.get_stock_items(
                patch.store_id, patch.product_id)
            if len(stock_items) == 0:
                raise NotFoundError("Stock item not found")
            return stock_items[0]
        except Exception as e:
            raise e

    def get_stock_items(self, store_id: Optional[str], product_id: Optional[str]):
        if store_id is not None:
            if product_id is not None:
                stock = StockPersistence.get_instance().get_stock_item(product_id, store_id)
                if stock is None:
                    product = ProductsPersistence.get_instance().get_product_by_id(product_id)
                    if product is None:
                        raise NotFoundError("Product not found")
                    utils.check_store_id(store_id)
                    
                    # create stock item if it doesn't exist
                    stock = StockPersistence.get_instance().edit_stock_item(
                        StockPatchDto(
                            product_id=product_id,
                            store_id=store_id,
                            quantity=0,
                            minimum_quantity=0
                        ))
                return [stock]
            else:
                return StockPersistence.get_instance().get_store_stock(store_id)
        else:
            if product_id is not None:
                return StockPersistence.get_instance().get_product_stock(product_id)
            else:
                return StockPersistence.get_instance().get_all_stock()

    def get_date_for_stock(self, get_dto):
        # map get_dto.items into list of tuples
        stock_qs = [item.quantity for item in get_dto.items]
        stock_keys = [(get_dto.store_id, item.product_id)
                      for item in get_dto.items]

        # get stock for all items
        stocks = [StockPersistence.get_instance().get_stock_item(key[1], key[0])
                  for key in stock_keys]
        stock_quantities = [
            stock.quantity if stock is not None else 0 for stock in stocks]

        diff = [stock_quantity - req_quantity for stock_quantity,
                req_quantity in zip(stock_quantities, stock_qs)]

        date = datetime.datetime.now() if all(
            [d >= 0 for d in diff]) else utils.on_day(datetime.datetime.now(), 1)
        return date

    def restock_items(self):
        stock: list[Stock] = StockPersistence.get_instance().get_all_stock()

        central_store_id = utils.get_central_store()["id"]

        # get current shipments
        pending_shipments = requests.get(
            f'http://shipments-service:8080/store-shipments?restock=true&status=PENDING').json()
        shipping_shipments = requests.get(
            f'http://shipments-service:8080/store-shipments?restock=true&status=SHIPPING').json()
        shipments = pending_shipments + shipping_shipments

        shipments_by_store = {}
        for s in shipments:
            if s["destination_store_id"] not in shipments_by_store:
                shipments_by_store[s["destination_store_id"]] = {}
            quantity_by_product = shipments_by_store[s["destination_store_id"]]
            for item in s["items"]:
                if item["product_id"] not in quantity_by_product:
                    quantity_by_product[item["product_id"]] = 0
                quantity_by_product[item["product_id"]] += item["quantity"]

        restock_by_store = {}
        for s in stock:
            if s.store_id == central_store_id:
                continue

            delta = s.minimum_quantity - s.quantity - \
                shipments_by_store.get(s.store_id, {}).get(s.product_id, 0)
            if delta <= 0:
                continue

            if s.store_id not in restock_by_store:
                restock_by_store[s.store_id] = []

            restock_by_store[s.store_id].append(
                {"product_id": s.product_id, "quantity": delta})

        for store_id, items in restock_by_store.items():

            # check if sufficient stock in central store
            _items = []
            for item in items:
                stock_item = StockPersistence.get_instance().get_stock_item(
                    item["product_id"], central_store_id)
                if stock_item is None or stock_item.quantity < item["quantity"]:
                    continue
                _items.append(item)
            if len(_items) == 0:
                continue

            utils.create_stock_shipment(
                store_id, central_store_id, _items, restock=True)

            # decrement central store stock
            for item in _items:
                central_store_stock_patch = StockPatchDto(
                    product_id=item["product_id"],
                    store_id=central_store_id,
                    action=StockPatchDtoAction.DECREMENT.value,
                    quantity=item["quantity"]
                )
                StockPersistence.get_instance().edit_stock_item(
                    central_store_stock_patch)
