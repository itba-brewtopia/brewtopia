from fastapi import FastAPI, HTTPException

from api.dto.stock_get_dto import StockGetDto
from exceptions.insufficient_stock_error import InsufficientStockError
from exceptions.not_found_error import NotFoundError
from api.dto.stock_full_dto import StockFullDto
from api.dto.input.stock_patch_dto import StockPatchDto, StockPatchDtoAction
from services.stock_service import StockService

app = FastAPI(openapi_url="/stock/openapi.json",
              docs_url="/stock/docs", redoc_url="/stock/redoc")


@app.get("/stock/health")
async def health_check():
    service_status = StockService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@app.patch("/stock", response_model=StockFullDto)
async def edit_stock_item(patch: StockPatchDto):
    if StockPatchDtoAction.CHANGE == patch.action and patch.quantity is None:
        raise HTTPException(
            status_code=422,
            detail="Invalid parameter: quantity is required when action is CHANGE")
    try:
        stock = StockService.get_instance().edit_stock_item(patch)
        return StockFullDto.from_model(stock)
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except InsufficientStockError as e:
        raise HTTPException(status_code=409, detail=str(e))
    except ValueError as e:
        raise HTTPException(
            status_code=503, detail="Service Unavailable: " + str(e))
    except Exception as e:
        raise HTTPException(
            status_code=500, detail="Internal Server Error: " + str(e))


@app.get("/stock")
async def get_stock_items(store_id: str = None, product_id: str = None):
    try:
        stock = StockService.get_instance().get_stock_items(store_id, product_id)
        return [StockFullDto.from_model(s) for s in stock]
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(
            status_code=500, detail="Internal Server Error: " + str(e))


@app.post("/stock/available")
async def get_available_date_stock_items(get_dto: StockGetDto):
    try:
        available_date = StockService.get_instance().get_date_for_stock(get_dto)
        return available_date
    except NotFoundError as e:
        raise HTTPException(status_code=404, detail=str(e))
    except Exception as e:
        raise HTTPException(
            status_code=500, detail="Internal Server Error: " + str(e))


@app.post("/stock/restock")
async def restock_items():
    try:
        StockService.get_instance().restock_items()
        return {"status": "OK"}
    except Exception as e:
        raise HTTPException(
            status_code=500, detail="Internal Server Error: " + str(e))
