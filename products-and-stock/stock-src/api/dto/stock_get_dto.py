
from typing import List
from pydantic import BaseModel

from api.dto.stock_item_dto import StockItemDto


class StockGetDto(BaseModel):
    store_id: str
    items: List[StockItemDto]
