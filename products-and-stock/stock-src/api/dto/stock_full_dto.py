from models.stock import Stock
from pydantic import BaseModel


class StockFullDto(BaseModel):
    store_id: str
    product_id: str
    quantity: int
    modified_at: str
    created_at: str
    minimum_quantity: int
    is_deleted: bool

    @staticmethod
    def from_model(stock:Stock):
        return StockFullDto(
            store_id=str(stock.store_id),
            product_id=str(stock.product_id),
            quantity=stock.quantity,
            modified_at=str(stock.modified_at),
            created_at=str(stock.created_at),
            minimum_quantity=stock.minimum_quantity,
            is_deleted=stock.is_deleted,
        )    

