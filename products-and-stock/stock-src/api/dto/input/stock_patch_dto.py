from enum import Enum
from typing import Optional
from pydantic import BaseModel


class StockPatchDtoAction(str, Enum):
    INCREMENT = 'increment'
    DECREMENT = 'decrement'
    CHANGE = 'change'


class StockPatchDto(BaseModel):
    product_id:str
    store_id:str
    quantity:Optional[int]
    minimum_quantity:Optional[int]
    is_deleted:Optional[bool]
    action: Optional[StockPatchDtoAction]