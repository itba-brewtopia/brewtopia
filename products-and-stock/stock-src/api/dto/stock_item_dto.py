

from pydantic import BaseModel


class StockItemDto(BaseModel):
    product_id: str
    quantity: int