from typing import List
from fastapi import FastAPI, HTTPException

from services.stores_service import StoresService
from api.dto.input.store_post_dto import StorePostDto
from api.dto.store_full_dto import StoreFullDto
from api.dto.input.store_patch_dto import StorePatchDto

app = FastAPI(openapi_url="/stores/openapi.json",
              docs_url="/stores/docs", redoc_url="/stores/redoc")


@app.get("/stores/health")
async def health_check():
    service_status = StoresService.get_instance().health_check()
    if "OK" in service_status:
        return {"status": "OK"}
    else:
        return {"status": "NOT OK", "details": service_status}


@app.get("/stores", response_model=List[StoreFullDto])
async def get_stores(alias: str = '', city: str = ''):
    stores = StoresService.get_instance().get_stores(alias=alias, city=city)
    response = []
    for store in stores:
        response.append(StoreFullDto.from_model(store))
    return response


@app.get("/stores/{id}", response_model=StoreFullDto)
async def get_store_by_id(id: str):
    store_model = StoresService.get_instance().get_store_by_id(id=id)
    if store_model is None:
        raise HTTPException(status_code=404, detail="Store not found")
    response = StoreFullDto.from_model(store_model)
    return response


@app.post("/stores", response_model=StoreFullDto, status_code=201)
async def create_store(storeDto: StorePostDto):
    store_model = StoresService.get_instance().create_store(alias=storeDto.alias, phone=storeDto.phone, address=storeDto.address,
                                                            open_hours=storeDto.open_hours)
    response = StoreFullDto.from_model(store_model)
    return response


@app.patch("/stores/{id}", response_model=StoreFullDto)
async def update_store(id: str, storePatchDto: StorePatchDto):
    store_model = StoresService.get_instance().update_store(id=id, alias=storePatchDto.alias, phone=storePatchDto.phone,
                                                            address=storePatchDto.address, open_hours=storePatchDto.open_hours,
                                                            is_deleted=storePatchDto.is_deleted)
    if store_model is None:
        raise HTTPException(status_code=404, detail="Store not found")
    response = StoreFullDto.from_model(store_model)
    return response
