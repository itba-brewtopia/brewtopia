from typing import Union
from pydantic import BaseModel

from api.dto.address_full_dto import AddressFullDto
from api.dto.open_hours_full_dto import OpenHoursFullDto


class StorePatchDto(BaseModel):
    address: Union[AddressFullDto, None] = None
    open_hours: Union[OpenHoursFullDto, None] = None
    alias: Union[str, None] = None
    phone: Union[str, None] = None
    is_deleted: Union[bool, None] = None
