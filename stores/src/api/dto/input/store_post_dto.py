from pydantic import BaseModel

from api.dto.address_full_dto import AddressFullDto
from api.dto.open_hours_full_dto import OpenHoursFullDto


class StorePostDto(BaseModel):
    address: AddressFullDto
    open_hours: OpenHoursFullDto
    alias: str
    phone: str
