from pydantic import BaseModel

from models.address import Address


class AddressFullDto(BaseModel):
    city: str
    number: str
    postal_code: str
    province: str
    street: str

    @staticmethod
    def from_model(address: Address):
        return AddressFullDto(city=address.city, number=address.number,
                              postal_code=address.postal_code, province=address.province,
                              street=address.street)
