from pydantic import BaseModel

from api.dto.address_full_dto import AddressFullDto
from api.dto.open_hours_full_dto import OpenHoursFullDto
from models.store import Store


class StoreFullDto(BaseModel):
    id: str
    address: AddressFullDto
    alias: str
    created_at: str
    is_deleted: bool
    modified_at: str
    open_hours: OpenHoursFullDto
    phone: str

    def from_model(store: Store):
        return StoreFullDto(id=str(store.id),
                            address=AddressFullDto.from_model(
                                store.address),
                            alias=store.alias,
                            is_deleted=store.is_deleted,
                            created_at=str(store.created_at),
                            modified_at=str(store.modified_at),
                            open_hours=OpenHoursFullDto.from_model(
                                store.open_hours),
                            phone=store.phone)
