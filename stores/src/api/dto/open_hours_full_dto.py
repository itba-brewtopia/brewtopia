from typing import List
from pydantic import BaseModel

from models.open_hours import OpenHours


class OpenHoursFullDto(BaseModel):
    friday: List[str]
    monday: List[str]
    saturday: List[str]
    sunday: List[str]
    thursday: List[str]
    tuesday: List[str]
    wednesday: List[str]

    @staticmethod
    def from_model(open_hours: OpenHours):
        return OpenHoursFullDto(friday=open_hours.friday,
                                monday=open_hours.monday,
                                saturday=open_hours.saturday,
                                sunday=open_hours.sunday,
                                thursday=open_hours.thursday,
                                tuesday=open_hours.tuesday,
                                wednesday=open_hours.wednesday)
