from datetime import datetime
import json
from fastapi.testclient import TestClient
from api.stores_api import app
from api.dto.input.store_post_dto import StorePostDto
from api.dto.input.store_patch_dto import StorePatchDto
from unittest.mock import MagicMock, patch
from models.store import Store

from api.dto.store_full_dto import StoreFullDto
from api.dto.open_hours_full_dto import OpenHoursFullDto
from api.dto.address_full_dto import AddressFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts
from tests.mocks.db_mocks import mock_db_open_hours, mock_db_address, mock_db_store, mock_db_stores


client = TestClient(app)


@patch('services.stores_service.StoresService.get_instance')
def test_health_check(mock_get_instance):
    mock_get_instance.return_value.health_check.return_value = "OK"
    response = client.get("/stores/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


@patch('services.stores_service.StoresService.get_instance')
def test_get_stores(mock_get_instance):
    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service
    mock_service.get_stores.return_value = mock_db_stores

    response = client.get("/stores")

    expected_response = [store.to_dict() for store in mock_db_stores]

    assert response.status_code == 200
    assert_equal_list_of_dicts(response.json(), expected_response)


@patch('services.stores_service.StoresService.get_instance')
def test_get_store_by_id(mock_get_instance):
    store1 = mock_db_store

    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.get_store_by_id.return_value = store1

    response = client.get(f"/stores/{store1.id}")

    expected_response = store1.to_dict()

    assert response.status_code == 200
    assert response.json() == expected_response


@patch('services.stores_service.StoresService.get_instance')
def test_create_store(mock_get_instance):
    store1 = mock_db_store

    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.create_store.return_value = store1

    response = client.post("/stores", json=StorePostDto(alias='Test Store', phone='123456789', address=AddressFullDto.from_model(mock_db_address),
                                                        open_hours=OpenHoursFullDto.from_model(mock_db_open_hours)).dict())

    expected_response = store1.to_dict()

    assert response.status_code == 201
    assert response.json() == expected_response


@patch('services.stores_service.StoresService.get_instance')
def test_update_store(mock_get_instance):
    store1 = mock_db_store

    mock_service = MagicMock()
    mock_get_instance.return_value = mock_service

    mock_service.update_store.return_value = store1

    response = client.patch(f"/stores/{store1.id}", json=StorePatchDto(alias='Test Store', phone='123456789', address=AddressFullDto.from_model(mock_db_address),
                                                                       open_hours=OpenHoursFullDto.from_model(mock_db_open_hours)).dict())

    expected_response = store1.to_dict()

    assert response.status_code == 200
    assert response.json() == expected_response
