import unittest
from unittest.mock import MagicMock, patch
from persistence.stores_persistence import StoresPersistence
from models.address import Address
from models.open_hours import OpenHours
from models.store import Store

mock_open_hours = OpenHours(
    monday=['10:00', '20:00'],
    tuesday=['10:00', '20:00'],
    wednesday=['10:00', '20:00'],
    thursday=['10:00', '20:00'],
    friday=['10:00', '20:00'],
    saturday=['10:00', '20:00'],
    sunday=['10:00', '20:00']
)

mock_address = Address(
    city='City 1',
    number='Number 1',
    postal_code='Postal Code 1',
    street='Street 1',
    province='Province 1'
)


@patch('mongoengine.connect')
@patch.object(StoresPersistence, '_init_db')
class TestStoresPersistence(unittest.TestCase):

    @patch.object(Store, 'objects')
    def test_get_stores(self, mock_objects, mock_init_db, mock_connection):
        mock_objects.return_value = [
            MagicMock(alias='Store 1'), MagicMock(alias='Store 2')]
        persistence = StoresPersistence()
        stores = persistence.get_stores()

        self.assertEqual(len(stores), 2, 'Should return 2 stores')
        self.assertEqual(stores[0].alias, 'Store 1')
        self.assertEqual(stores[1].alias, 'Store 2')

    @patch.object(Store, 'objects')
    def test_get_store_by_id(self, mock_objects, mock_init_db, mock_connection):
        # ID MUST be 24 characters long for test to work, since ObjectId expects this
        expected = MagicMock(id='5f4f3f2f1e0d0c0b0a090807')
        mock_objects.return_value.first.return_value = expected
        persistence = StoresPersistence()
        store = persistence.get_store_by_id(id=expected.id)

        self.assertEqual(store.id, expected.id)

    @patch.object(Store, 'save')
    def test_create_store(self, mock_save, mock_init_db, mock_connection):
        params = {'alias': 'Store 1', 'address': mock_address,
                  'phone': 'Phone 1', 'open_hours': mock_open_hours}
        expected = MagicMock(**params, id='123')
        mock_save.return_value = expected

        persistence = StoresPersistence()
        new_store = persistence.create_store(**params)

        self.assertEqual(new_store.alias, expected.alias)
        self.assertEqual(new_store.address.city, expected.address.city)
        self.assertEqual(new_store.phone, expected.phone)
        self.assertEqual(new_store.id, expected.id)

    @patch.object(Store, 'objects')
    def test_update_store(self, mock_objects, mock_init_db, mock_connection):
        params = {'id': '5f4f3f2f1e0d0c0b0a090807', 'alias': 'Store 1', 'address': mock_address,
                  'phone': 'Phone 1', 'open_hours': mock_open_hours}

        expected = MagicMock(**params)

        # Create the mock that will be returned by Store.objects.first()
        mock_store = MagicMock()
        mock_store.save.return_value = expected

        # Setup the mock object chain
        mock_objects.return_value.first.return_value = mock_store

        persistence = StoresPersistence()
        new_store = persistence.update_store(**params)

        self.assertEqual(new_store.alias, expected.alias)
        self.assertEqual(new_store.address.city, expected.address.city)
        self.assertEqual(new_store.phone, expected.phone)
        self.assertEqual(new_store.id, expected.id)
