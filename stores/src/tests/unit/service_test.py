import unittest
from unittest.mock import patch, MagicMock, create_autospec
from persistence.stores_persistence import StoresPersistence
from api.dto.open_hours_full_dto import OpenHoursFullDto
from api.dto.address_full_dto import AddressFullDto
from services.stores_service import StoresService

mock_open_hours = OpenHoursFullDto(
    monday = ['10:00','20:00'],
    tuesday = ['10:00','20:00'],
    wednesday = ['10:00','20:00'],
    thursday = ['10:00','20:00'],
    friday = ['10:00','20:00'],
    saturday = ['10:00','20:00'],
    sunday = ['10:00','20:00']
)

mock_address = AddressFullDto(
    city = 'City 1',
    number = 'Number 1',
    postal_code='Postal Code 1',
    street = 'Street 1',
    province='Province 1'
)

class TestStoresService(unittest.TestCase):  

    @patch('persistence.stores_persistence.StoresPersistence.get_instance', return_value=MagicMock())
    def test_get_stores(self, mock_get_instance):
        mock_get_instance.return_value.get_stores.return_value = [MagicMock(alias='Store 1'), MagicMock(alias='Store 2')]

        stores_service = StoresService()
        stores = stores_service.get_stores()

        self.assertEqual(len(stores), 2)
        self.assertEqual(stores[0].alias, 'Store 1')
        self.assertEqual(stores[1].alias, 'Store 2')

    @patch('persistence.stores_persistence.StoresPersistence.get_instance', return_value=MagicMock())
    def test_get_store_by_id(self, mock_get_instance):
        mock_get_instance.return_value.get_store_by_id.return_value = MagicMock(alias='Store 1')

        stores_service = StoresService()
        store = stores_service.get_store_by_id('123')
        self.assertEqual(store.alias, 'Store 1')
    
    @patch('persistence.stores_persistence.StoresPersistence.get_instance', return_value=MagicMock())
    def test_create_store(self, mock_get_instance):
        
        params = {'alias': 'Store 1', 'address': mock_address, 'phone': 'Phone 1', 'open_hours': mock_open_hours}
        mock_get_instance.return_value.create_store.return_value = MagicMock(id='123', **params)
        stores_service = StoresService()
        store = stores_service.create_store(**params)

        self.assertEqual(store.id, '123')
        self.assertEqual(store.alias, 'Store 1')
        self.assertEqual(store.address.city, 'City 1')
        self.assertEqual(store.phone, 'Phone 1')

    @patch('persistence.stores_persistence.StoresPersistence.get_instance', return_value=MagicMock())
    def test_update_store(self, mock_get_instance):
        params = {'id': '123', 'alias': 'Store 1', 'address': mock_address, 'phone': 'Phone 1', 'open_hours': mock_open_hours}

        mock_get_instance.return_value.update_store.return_value = MagicMock(**params)
        store_service = StoresService()
        store = store_service.update_store(**params)

        self.assertEqual(store.alias, 'Store 1')
        self.assertEqual(store.address.city, 'City 1')
        self.assertEqual(store.phone, 'Phone 1')

        
