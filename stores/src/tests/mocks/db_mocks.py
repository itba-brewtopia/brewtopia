from models.address import Address
from models.open_hours import OpenHours
import datetime
from models.store import Store
from bson.objectid import ObjectId

mock_db_open_hours = OpenHours(
    monday=['10:00', '20:00'],
    tuesday=['10:00', '20:00'],
    wednesday=['10:00', '20:00'],
    thursday=['10:00', '20:00'],
    friday=['10:00', '20:00'],
    saturday=['10:00', '20:00'],
    sunday=['10:00', '20:00']
)

mock_db_address = Address(
    city='City 1',
    number='Number 1',
    postal_code='Postal Code 1',
    street='Street 1',
    province='Province 1'
)

mock_db_store = Store(id=ObjectId("5f4f3f2f1e0d0c0b0a090807"), alias="store0",
                      phone="123456789", address=mock_db_address, open_hours=mock_db_open_hours,
                      created_at=datetime.datetime.utcnow(), modified_at=datetime.datetime.utcnow()
                      )

mock_db_stores = [
    Store(id=ObjectId("5f4f3f2f1e0d0c0b0a090802"), alias='CENTRAL', phone='123456789',
          address=Address(city='La boca', number='805', postal_code='C1161',
                          province='Capital federal', street='Brandsen'),
          open_hours=OpenHours(monday=['09:00-18:00'], tuesday=['09:00-18:00'], wednesday=['09:00-18:00'],
                               thursday=['09:00-18:00'], friday=['09:00-18:00'], saturday=['09:00-18:00']
                               )),
    Store(id=ObjectId("5f4f3f2f1e0d0c0b0a090807"), alias="store1",
          phone="123456789", address=mock_db_address, open_hours=mock_db_open_hours,
          created_at=datetime.datetime.utcnow(), modified_at=datetime.datetime.utcnow()
          ),
    Store(id=ObjectId("5f4f3f2f1e0d0c0b0a090801"), alias="store2",
          phone="123456789", address=mock_db_address, open_hours=mock_db_open_hours,
          created_at=datetime.datetime.utcnow(), modified_at=datetime.datetime.utcnow()
          )
]
