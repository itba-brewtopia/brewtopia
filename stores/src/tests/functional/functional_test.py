from fastapi.testclient import TestClient
import pytest
from api.stores_api import app
from api.dto.input.store_post_dto import StorePostDto
from api.dto.input.store_patch_dto import StorePatchDto
import datetime

from persistence.stores_persistence import StoresPersistence
from services.stores_service import StoresService

from api.dto.open_hours_full_dto import OpenHoursFullDto
from api.dto.address_full_dto import AddressFullDto
from tests.utils.assert_utils import assert_equal_list_of_dicts, assert_equal_dicts
from tests.mocks.db_mocks import mock_db_open_hours, mock_db_address, mock_db_stores
from tests.utils.db_utils import setup_db, seed_db, drop_db

client = TestClient(app)


@pytest.fixture(scope="module", autouse=True)
def setup_and_teardown_module():
    setup_db()

    yield

    drop_db()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown():
    StoresPersistence._instance = None
    StoresService._instance = None

    yield

    drop_db()


def test_health_check():
    response = client.get("/stores/health")
    assert response.status_code == 200
    assert response.json() == {"status": "OK"}


def test_get_stores():
    seed_db()
    response = client.get("/stores")

    expected_response = [store.to_dict() for store in mock_db_stores]

    assert response.status_code == 200
    assert_equal_list_of_dicts(expected_response, response.json())


def test_get_store_by_id():
    seed_db()

    store1 = mock_db_stores[0]

    response = client.get(f"/stores/{store1.id}")

    expected_response = store1.to_dict()

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)


def test_create_store():
    store1 = mock_db_stores[1]

    response = client.post("/stores", json=StorePostDto(alias=store1.alias, phone=store1.phone, address=AddressFullDto.from_model(store1.address),
                                                        open_hours=OpenHoursFullDto.from_model(store1.open_hours)).dict())

    expected_response = store1.to_dict()
    expected_response["id"] = response.json()["id"]
    assert response.status_code == 201
    assert_equal_dicts(response.json(), expected_response)


def test_update_store():
    seed_db()

    store1 = mock_db_stores[1]

    response = client.patch(f"/stores/{store1.id}", json=StorePatchDto(
        alias=store1.alias, phone=store1.phone, address=AddressFullDto.from_model(
            mock_db_address),
        open_hours=OpenHoursFullDto.from_model(mock_db_open_hours)).dict())

    expected_response = store1.to_dict()

    assert response.status_code == 200
    assert_equal_dicts(response.json(), expected_response)
