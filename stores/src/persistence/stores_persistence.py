import multiprocessing
import time
from typing import Optional
from bson import ObjectId
from func_timeout import FunctionTimedOut, func_timeout
# imported as mongoengine on purpose for tests to properly patch mongoengine.connect
import mongoengine
import os
import datetime

from models.store import Store
from models.address import Address
from models.open_hours import OpenHours


class StoresPersistence:

    _instance = None

    def __init__(self):
        database_name = os.environ.get('MONGO_INITDB_DATABASE')
        database_user = os.environ.get('MONGO_INITDB_ROOT_USERNAME')
        database_password = os.environ.get('MONGO_INITDB_ROOT_PASSWORD')
        mongoengine.connect(db=database_name, username=database_user,
                            password=database_password, host='stores-mongodb')
        self._init_db()

    def _init_db(self):
        if not Store.objects(alias__iexact='CENTRAL').first():
            print("Creating central store")
            self.create_store(alias='CENTRAL', phone='123456789',
                              address=Address(city='La boca', number='805', postal_code='C1161',
                                              province='Capital federal', street='Brandsen'),
                              open_hours=OpenHours(monday=['09:00-18:00'], tuesday=['09:00-18:00'], wednesday=['09:00-18:00'],
                                                   thursday=['09:00-18:00'], friday=['09:00-18:00'], saturday=['09:00-18:00'],))

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        try:
            func_timeout(5, lambda: Store.objects().first())
        except FunctionTimedOut:
            return "Connection to database error"
        except Exception:
            return "Healthcheck persistence error"

        return "OK"

    def get_stores(self, alias: str = '', city: str = ''):
        return Store.objects(alias__icontains=alias, address__city__icontains=city, is_deleted=False)

    def get_store_by_id(self, id: str):
        if not ObjectId.is_valid(id):
            return None
        return Store.objects(id=ObjectId(id)).first()

    def create_store(self, alias: str,
                     phone: str,
                     address: Address,
                     open_hours: OpenHours):
        store = Store(alias=alias, phone=phone,
                      address=address, open_hours=open_hours,
                      created_at=datetime.datetime.utcnow(),
                      modified_at=datetime.datetime.utcnow())
        return store.save()

    def update_store(self, id: str, alias: Optional[str] = None,
                     phone: Optional[str] = None,
                     address: Optional[Address] = None,
                     open_hours: Optional[OpenHours] = None,
                     is_deleted: Optional[bool] = None):
        if not ObjectId.is_valid(id):
            return None
        store = Store.objects(id=ObjectId(id)).first()
        if alias is not None:
            store.alias = alias
        if phone is not None:
            store.phone = phone
        if address is not None:
            store.address = address
        if open_hours is not None:
            store.open_hours = open_hours
        if is_deleted is not None:
            store.is_deleted = is_deleted

        store.modified_at = datetime.datetime.utcnow()
        return store.save()
