from typing import List, Optional
from persistence.stores_persistence import StoresPersistence

from models.store import Store
from models.address import Address
from models.open_hours import OpenHours
from api.dto.open_hours_full_dto import OpenHoursFullDto
from api.dto.address_full_dto import AddressFullDto


class StoresService:

    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def health_check(self):
        return StoresPersistence.get_instance().health_check()

    def get_stores(self, alias: str = '', city: str = ''):
        return StoresPersistence.get_instance().get_stores(alias=alias, city=city)

    def get_store_by_id(self, id: str):
        return StoresPersistence.get_instance().get_store_by_id(id)

    def create_store(self, alias: str, phone: str,
                     address: AddressFullDto,
                     open_hours: OpenHoursFullDto,
                     ):
        return StoresPersistence.get_instance()\
            .create_store(alias=alias,
                          phone=phone,
                          address=Address(
                              city=address.city, number=address.number, postal_code=address.postal_code, province=address.province, street=address.street),
                          open_hours=OpenHours(monday=open_hours.monday, tuesday=open_hours.tuesday, wednesday=open_hours.wednesday,
                                               thursday=open_hours.thursday, friday=open_hours.friday, saturday=open_hours.saturday,
                                               sunday=open_hours.sunday)
                          )

    def update_store(self, id: str, alias: Optional[str] = None, phone: Optional[str] = None,
                     address: Optional[AddressFullDto] = None,
                     open_hours: Optional[OpenHoursFullDto] = None,
                     is_deleted: Optional[bool] = None
                     ):
        db_address = None
        if address is not None:
            db_address = Address(city=address.city, number=address.number,
                                 postal_code=address.postal_code, province=address.province, street=address.street)

        db_open_hours = None
        if open_hours is not None:
            db_open_hours = OpenHours(monday=open_hours.monday, tuesday=open_hours.tuesday, wednesday=open_hours.wednesday,
                                      thursday=open_hours.thursday, friday=open_hours.friday, saturday=open_hours.saturday,
                                      sunday=open_hours.sunday)
        return StoresPersistence.get_instance()\
            .update_store(id=id, alias=alias, phone=phone, address=db_address, open_hours=db_open_hours, is_deleted=is_deleted)
