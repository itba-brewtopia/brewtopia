from mongoengine import EmbeddedDocument, ListField, StringField


class OpenHours(EmbeddedDocument):
    monday = ListField(StringField(max_length=20))
    tuesday = ListField(StringField(max_length=20))
    wednesday = ListField(StringField(max_length=20))
    thursday = ListField(StringField(max_length=20))
    friday = ListField(StringField(max_length=20))
    saturday = ListField(StringField(max_length=20))
    sunday = ListField(StringField(max_length=20))

    def to_dict(self):
        return {
            "monday": self.monday,
            "tuesday": self.tuesday,
            "wednesday": self.wednesday,
            "thursday": self.thursday,
            "friday": self.friday,
            "saturday": self.saturday,
            "sunday": self.sunday
        }
