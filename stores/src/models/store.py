from mongoengine import Document, EmbeddedDocumentField, StringField, BooleanField, DateTimeField
import datetime
from .address import Address
from .open_hours import OpenHours


class Store(Document):
    alias = StringField(max_length=50, unique=True)
    address = EmbeddedDocumentField(Address)
    open_hours = EmbeddedDocumentField(OpenHours)
    phone = StringField(max_length=20)
    is_deleted = BooleanField(default=False)
    created_at = DateTimeField()
    modified_at = DateTimeField()

    def to_dict(self):
        return {
            "id": str(self.id),
            "alias": self.alias,
            "address": self.address.to_dict() if self.address else None,
            "open_hours": self.open_hours.to_dict() if self.open_hours else None,
            "phone": self.phone,
            "is_deleted": self.is_deleted,
            "created_at": self.created_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.created_at else None,
            "modified_at": self.modified_at.strftime("%Y-%m-%d %H:%M:%S.%f") if self.modified_at else None,
        }
