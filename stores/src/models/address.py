from mongoengine import EmbeddedDocument, StringField


class Address(EmbeddedDocument):
    street = StringField(required=True, max_length=20)
    number = StringField(required=True, max_length=20)
    postal_code = StringField(required=True, max_length=20)
    city = StringField(required=True, max_length=20)
    province = StringField(required=True, max_length=20)

    def to_dict(self):
        return {
            "street": self.street,
            "number": self.number,
            "postal_code": self.postal_code,
            "city": self.city,
            "province": self.province
        }
