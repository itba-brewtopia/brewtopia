import requests


def _test_health_endpoint(service_name, path, port=8080):
    response = requests.get(
        f'http://{service_name}:{port}/{path}/health', timeout=10)
    assert response.status_code == 200
    assert response.json()['status'] == 'OK'


def test_customer_service_up():
    _test_health_endpoint('customers-service', 'customers')
    _test_health_endpoint('api-gateway', 'customers',
                          80)  # test from api gateway


def test_products_service_up():
    _test_health_endpoint('products-service', 'products')
    _test_health_endpoint('products-service', 'categories')
    # test from api gateway
    _test_health_endpoint('api-gateway', 'products', 80)
    _test_health_endpoint('api-gateway', 'categories', 80)


def test_carts_service_up():
    _test_health_endpoint('carts-service', 'carts')
    _test_health_endpoint('api-gateway', 'carts', 80)


def test_orders_service_up():
    _test_health_endpoint('orders-service', 'orders')
    _test_health_endpoint('api-gateway', 'orders', 80)


def test_stock_service_up():
    _test_health_endpoint('stock-service', 'stock')
    _test_health_endpoint('api-gateway', 'stock', 80)


def test_shipments_service_up():
    _test_health_endpoint('shipments-service', 'store-shipments')
    _test_health_endpoint('api-gateway', 'store-shipments', 80)


def test_stores_service_up():
    _test_health_endpoint('stores-service', 'stores')
    _test_health_endpoint('api-gateway', 'stores', 80)
