#!/bin/bash -e 

echo "Running integration tests"
python -m pytest "integration_test.py" --junitxml=integration_report.xml

echo "Running interface tests"
export TIMESTAMP=$(date +%Y%m%d%H%M%S)
python -m pytest "interface_tests" --junitxml=interface_report.xml
